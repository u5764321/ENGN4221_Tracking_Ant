# Ant Tracking Project Landing Page

**This project focuses on:**

**1. Developing head and body orientation tracking algorithms.**

**2. Upgrading the trackball device with new electronics (PCBs) and new components.**

**_An [Introduction Video](4_Miscellaneous/1_Ant_Experiment_Video/ant_experiment_final.mp4) to ANU Zeil Lab's Ant Experiment._**

## Handover Document and Poster

[Tracking Ant Team Handover Document](2_System_Design_and_Analysis/6_Handover_Document/handover_document_final_version_1.pdf)

[Tracking Ant Team Showcase Poster](2_System_Design_and_Analysis/7_Showcase_Poster/showcase_poster.pdf)

## Decision Log

[Decision Log](1_Project_Administration/4_Project_Management/5_Decision_Log/decision_log.pdf)

## Public Feedback
Please comment on this project and provide any feedback through the [Google Forms](https://goo.gl/forms/kiDe1UmwffYJyXR23) survey.

## Weekly Updates

| Week                                  | Contents                                          |
| :---:                                 | ---                                               |
| Week 1, 23 Jul - 29 Jul, 2018         | Team Formnation on 26 Jul                         |
| Week 2, 30 Jul - 05 Aug, 2018         | Client Meeting on 31 Jul, 02 Aug, topic finalised |
|                                       | Concept of Operations draft uploaded on 04 Aug    |
| Week 3, 06 Aug - 12 Aug, 2018         | Audit 1 on 06 Aug                                 |
|                                       | Client Meeting on 07 Aug, TDU requires new scope  |
|                                       | OT received ant walking images and videos         |
|                                       | A detail [WBS](1_Project_Administration/4_Project_Management/1_Work_Breakdown_Structure/work_breakdown_structure_v3_with_descriptions.pdf) is created by Tienan on 12 Aug               |
| Week 4, 13 Aug - 19 Aug, 2018         | A detail [Gantt Chart old_version](https://www.icloud.com/numbers/0pSnDPiuXL57h4IsBH_A6qjuA#tracking%5Fant%5Fgantt%5Fchart) is created by Tienan on 13 Aug    |
|                                       | Measurements on current trackball device          |
|                                       | Electronics selection and PCB design started      |
|                                       | Major components for new trackball device finalised |
|                                       | Initial PCB package dimension design completed |
|                                       | Computer Vision development stops, Machine Learning (Deep Learning) development starts|
| Week 5, 20 Aug - 26 Aug, 2018         | A [feedback](https://goo.gl/forms/kiDe1UmwffYJyXR23) channel is made available for the public |
|                                       | Revised version of [Gantt Chart](https://www.icloud.com/numbers/0lv1DTDurrmdIGanVhc3dUL1A#tracking%5Fant%5Fgantt%5Fchart%5FV2) with delayed TDU programming   |
|                                       | PCB schematics design conducted |
| Week 6, 27 Aug - 02 Sep, 2018         | Audit 2 on 27 Aug                 |
|                                       | PCB schematics and layout design completed, design documents and Bill of Material exported    |
|                                       | Repository Re-structured, old repository can be found [here](0_Old_Repository_Structure)      |
| Break 1, 03 Sep - 09 Sep, 2018        | Communicating with agent for manufacturer selection
|                                       | OT Algorithm - continue development for covering all angles   |
| Break 2, 10 Sep - 16 Sep, 2018        | OT Algorithm - start optimisation for different lightining conditions |
|                                       | TDU - prepare for optical PCB design  |
| Week 7, 17 Sep - 23 Sep, 2018         | Team review on project (see [Group Meeting Minutes 06](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_06.pdf))   |
|                                       | Decision on GUI development, introductory video and optical sensor PCB design |
|                                       | Head orientation tracking algorithm completion, further debugging are necessary   |
| Week 8, 24 Sep - 30 Sep, 2018         | Start body orientation tracking coding  |
|                                       | Handover document and poster planning    |
|                                       | Optical sensor PCB design sent out for manufacturing  |
| Week 9, 01 Oct - 07 Oct, 2018         | GUI initial program completion, training and testing functions realised, require further development to include new OT algorithm features |
|                                       | Body orientation tracking implemented |
|                                       | Program input and output discussion, initial stage of encapsulation completed    |
|                                       | Handover document and poster draft complete   |

## Task Allocation

Tasks are currently allocated to team members through Trello.

[![trello_page](4_Miscellaneous/trello_pic.png)](https://trello.com/b/IUsD1JJc)

## Team Member Contact

| Name                  | Email                     | Topic     | Administration Roles                                                  |
| ---                   | :---:                     | :---:     | ---                                                                   |
| Tienan Xu             | u5829270@anu.edu.au       | TDU       | Point of Contact; Trello, Landing Page & Repository Maintainer        |
| Yile Liu              | u5694993@anu.edu.au       | TDU       | TDU Leader                                                            |
| Fangxiao Dong         | u5698699@anu.edu.au       | TDU       | -                                                                     |
| Zhenzhen Liu          | u5625456@anu.edu.au       | TDU, OT   | -                                                                     |
| Shu Liu               | u5764321@anu.edu.au       | TDU, OT   | -                                                                     |
| Guofeng Xu            | u5491523@anu.edu.au       | OT        | OT Leader                                                             |

## Table Of Contents
1. [Welcome](#1-welcome)
2. [Project Description](#2-project-description)
3. [Concept of Operations](#3-concept-of-operations)
4. [Meetings](#4-meetings)
5. [Audit](#5-audit)
6. [Trackball Device Upgrade](#6-trackball-device-upgrade)
7. [Orientation Tracking](#7-orientation-tracking)

## 1. Welcome
Welcome to the Ant Tracking project. This project is conducting under the ANU Engineering Program ENGN4221 Systems Engineering Project.

In this page, you will be able to find files and resources published by the project team.


## 2. Project Description
The project aims to develop algorithms and equipments for the ANU Zeil Lab for improving current research on ant navigations, this includes:
1. Algorithms for tracking the ant’s head and body orientation through recorded videos of the ant walking on the trackball device.
2. New trackball device with improved electronics, wireless data transmission and real-time device orientation tracking.

## 3. Concept of Operations
Concept of Operations had been developed. It is expected to be signed-off in near future.

[Concept of Operations](1_Project_Administration/1_Concept_of_Operations/Concept_of_Operations_V2.md)

## 4. Meetings
### Client Meetings
| Client Meeting                                                            | Date                                              | Comment                           |
| :---:                                                                     | ---                                               | ---                               |
| [Client Meeting 01](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_01.pdf)     | Thu, 26/07/2018                     | Team Formation Night              |
| [Client Meeting 02](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_02.pdf)     | Tue, 31/07/2018                     | First visit to lab                |
| [Client Meeting 03](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_03.pdf)     | Thu, 02/08/2018                     | Decide topics                     |
| [Client Meeting 04](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_04.pdf)     | Tue, 07/08/2018                     | TDU scope change                  |
| [Client Meeting 05](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_05.pdf)     | Wed, 22/08/2018                     | Progress update                   |
| [Client Meeting 06](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_06.pdf)     | Mon, 27/08/2018                     | Audit 2 practice                  |
| [Client Meeting 07](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_07.pdf)     | Mon, 27/08/2018                     | PCB manufacturing discussion      |
| [Client Meeting 08](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_08.pdf)     | Wed, 12/09/2018                     | Manufacturing detail dicussion and OT model progress    |
| [Client Meeting 09](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_09.pdf)     | Thu, 13/09/2018                     | Ant experiment session  |
| [Client Meeting 10](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_10.pdf)     | Tue, 25/09/2018                     | Progress update, handover document task |
| [Client Meeting 11](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_11.pdf)     | Wed, 03/10/2018                     | OT demonstration, PCB manufacturing payment discussion |
| [Client Meeting 12](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_12.pdf)     | Mon, 08/10/2018                     | Audit 3 practice |


### Group Meetings
| Group Meeting                                                                             | Date                                                | Comment                             |
| :---:                                                                               | ---                                                 | ---                                 |
| [Group Meeting 01](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_01.pdf)      | Wed, 01/08/2018, RSCS & MSI Building                | Topic discussion, role assignment   |
| [Group Meeting 02](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_02.pdf)      | Sat, 04/08/2018, RSCS & MSI Building                | ConOps night                        |
| [Group Meeting 03](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_03.pdf)      | Wed, 08/08/2018, RSCS & MSI Building                | Timeline and plan proposal, TDU scope determined |
| [Group Meeting 04](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_04.pdf)      | Sun, 12/08/2018, Lena Karmel Lodge                  | Project Audit 1 Feedback discussion |
| [Group Meeting 05](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_05.pdf)      | Sun, 19/08/2018, Lena Karmel Lodge                  | Mainly technical discussion           |
| [Group Meeting 06](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_06.pdf)      | Sun, 23/09/2018, Lena Karmel Lodge                  | Progress review, discussion on next step |
| [Group Meeting 07](1_Project_Administration/2_Meeting_Minutes/1_Group_Meeting_Minutes/group_meeting_minute_07.pdf)      | Sun, 01/10/2018, Lena Karmel Lodge                  | Handover document and poster discussion, technical discussion |

### Tutorial Meetings
| Tutorial Meeting                                                                                       | Date                                              | Comment                       |
| :---:                                                                                         | ---                                               | ---                           |
| [Tutorial Meeting Week 2](1_Project_Administration/2_Meeting_Minutes/3_Tutorial_Meeting_Minutes/tutorial_meeting_minute_week_2.pdf)   | Mon, 30/07/2018, Ian Ross Design Studio           | Discussion on repository      |
| [Tutorial Meeting Week 4](1_Project_Administration/2_Meeting_Minutes/3_Tutorial_Meeting_Minutes/tutorial_meeting_minute_week_4.pdf)   | Mon, 13/08/2018, Ian Ross Design Studio           | Feedback on Audit 1, progress discussion  |
| [Tutorial Meeting Week 5](1_Project_Administration/2_Meeting_Minutes/3_Tutorial_Meeting_Minutes/tutorial_meeting_minute_week_5.pdf)   | Mon, 20/08/2018, Ian Ross Design Studio           | Technical work update  |
| [Tutorial Meeting Week 7](1_Project_Administration/2_Meeting_Minutes/3_Tutorial_Meeting_Minutes/tutotial_meeting_minute_week_7.pdf)   | Mon, 17/09/2018, Ian Ross Design Studio           | Feedback on Audit 2, progress discussion  |
| [Tutorial Meeting Week 8](1_Project_Administration/2_Meeting_Minutes/3_Tutorial_Meeting_Minutes/tutotial_meeting_minute_week_8.pdf)   | Mon, 24/10/2018, Ian Ross Design Studio           | Technical work update |

## 5. Audit
| Audit                                                                         | Date                                              |
| :---:                                                                         | ---                                               |
| [Audit 1](1_Project_Administration/3_Project_Audit/1_PA_1)                    | Mon, 06/08/2018, Ian Ross Design Studio           |
| [Audit 2](1_Project_Administration/3_Project_Audit/2_PA_2)                    | Mon, 27/08/2018, Ian Ross Design Studio           |
| [Audit 3](1_Project_Administration/3_Project_Audit/3_PA_3)                    | Mon, 08/10/2018, Ian Ross Design Studio           |

## 6. Trackball Device Upgrade
[Work repository](3_Technical_Design/2_Trackball_Device_Upgrade)

| Date                  | Update                                                        | Comment       |
| :---:                 | ---                                                           | ---           |
| Tue, 07/08/2018       | Major scope change is required, refer to [Client Meeting 04](1_Project_Administration/2_Meeting_Minutes/2_Client_Meeting_Minutes/client_meeting_minute_04.pdf)            | Scope not yet developed                   |
| Thu, 09/08/2018       | Timeline updated, refer to [Trackball Device Upgrade Timeline](1_Project_Administration/4_Project_Management/2_Timeline/timeline_TDU_v2.pdf)     | A Gantt Chart will be soon developed      |
| Mon, 13/08/2018       | [WBS](1_Project_Administration/4_Project_Management/1_Work_Breakdown_Structure/work_breakdown_structure_v3_with_descriptions.pdf) and [Gantt Chart](https://www.icloud.com/numbers/0lv1DTDurrmdIGanVhc3dUL1A#tracking%5Fant%5Fgantt%5Fchart%5FV2) created |   |
| Tue, 14/08/2018       | The team visited Zoltan to acquire the PCB design software - Eagle and conducted measurement on current trackball device | Measuring work to be continued tomorrow |
| Wed, 15/08/2018       | The team continued measuring dimensional and geometrical data of current trackball device |   A report will be created and uploaded   |
| Sat, 18/08/2018       | [Report](2_System_Design_and_Analysis/2_Analysis/Major_Components_and_Functions.pdf) on current device measurements uploaded                ||
| Sun, 19/08/2018       | Initial [PCB package dimension design](2_System_Design_and_Analysis/3_Design/Printed_Circuit_Board_Design.pdf) completed                                  | Uploaded on Wed, 22/08/2018 |
| Wed, 22/08/2018       | A [subsystem interface](2_System_Design_and_Analysis/4_Architecture/New_Trackball_Device_Subsystem_Interface.pdf) is created for the new trackball device    | A report will be created and uploaded |
| Thu, 23/08/2018       | [Report](2_System_Design_and_Analysis/2_Analysis/Electronic_Components.pdf) on current device electronics uploaded ||
|                       | Conduct main PCB schematics and layout design in Campus Field Station |    |
| Fri, 24/08/2018       | Continue on main PCB schematics and layout design |    |
| Mon, 27/08/2018       | [Report](2_System_Design_and_Analysis/4_Architecture/Explanation_on_New_Trackball_Device_Subsystem_Interface.pdf) on the Subsystem Interface uploaded    ||
| Tue, 28/08/2018       | Continue on mian PCB schematics and layout design      | [Main PCB Design](3_Technical_Design/2_Trackball_Device_Upgrade/3_Schematic_and_Layout/main_pcb_design)   |
| Wed, 29/08/2018       | Main PCB schematics and layout design completed      | Work documents will be uploaded after group review   |
| Mon, 03/09/2018       | Established contact with an agent, who is resourceful in negotiating with manufacturers in Shenzhen, China |  |
| Thu, 06/09/2018       | Initial qoute for PCB manufacturing received, agent advised the team that even better options are possible |    Reported to Trevor via Slack    |
| Tue, 11/09/2018       | Final quote received, order placed | Meeting with Trevor on 12/09/2018 to discuss about the quote |
| Mon, 17/09/2018       | Manufacturer advised the team that production formally commenced, see see [Manufacturer Contract](1_Project_Administration/4_Project_Management/4_Finance/main_pcb_manufacturer_contract_translated.pdf) | 
| Wed, 26/09/2018       | Optical sensor PCB designed, seeking expert opinions and feedbacks    | [Optical PCB Design](3_Technical_Design/2_Trackball_Device_Upgrade/3_Schematic_and_Layout/optical_pcb_design), once certified by the expert, send to manufacturer for production     |
| Thu, 27/09/2018       | Main PCB Manufacturing completed | Shipping will be arranged on the next work day    |
| Fri, 28/09/2018       | Main PCB shipped out by manufacturer |     Expected deivery time 2 days - 5 days, [DHL Tracking](http://www.dhl.com.au/en/express/tracking.html?AWB=3315186696&brand=DHL)   |
| Sat, 29/09/2018       | Optical sensor PCB design sent to manufacturer |  Quote was discussed with client and approved |
| Wed, 03/10/2018       | Other components of the trackball device - [disk](3_Technical_Design/2_Trackball_Device_Upgrade/2_Technical_Drawing/disk_holder_stl.stl) and [sphere](3_Technical_Design/2_Trackball_Device_Upgrade/2_Technical_Drawing/sphere_holder_stl.stl) holders, sent to Trevor for review |   A meet with the mechanical workshop to be scheduled in future |
| Thu, 04/10/2018       | Main PCB delivered |

## 7. Orientation Tracking
[Work repository](3_Technical_Design/1_Orientation_Tracking)

| Date                  | Update                                                        | Comment       |
| :---:                 | ---                                                           | ---           |
| Wed, 08/08/2018       | Timeline updated, refer to [Orientation Tracking Timeline](1_Project_Administration/4_Project_Management/2_Timeline/timeline_OT_v2.pdf)    | A Gantt Chart will be soon developed          |
| Thu, 09/08/2018       | Received 1000 annotaed ant orientation images from client     | Hard drive picked up from Client's office |
| Mon, 13/08/2018       | [WBS](1_Project_Administration/4_Project_Management/1_Work_Breakdown_Structure/work_breakdown_structure_v3_with_descriptions.pdf) and [Gantt Chart](https://www.icloud.com/numbers/0lv1DTDurrmdIGanVhc3dUL1A#tracking%5Fant%5Fgantt%5Fchart%5FV2) created |   |
| Sun, 19/08/2018       | Machine Learning (Deep Learning) [research and algorithm](2_System_Design_and_Analysis/4_Architecture/Deep_Learning_Network_Architecture_V2.pdf) explained ||
|                       | An [analysis](2_System_Design_and_Analysis/2_Analysis/Analysis_on_Computer_Vision_Approach.pdf) on Computer Vision approach is done   ||
| Wed, 22/08/2018       | Deep Learning model building test on GPU successed ||
| Thu, 23/08/2018       | The basic model is constructed  | Version 1 |
| Sun, 09/09/2018       | All researches have been completed  | the supporting functions are shown in [Modules Update](3_Technical_Design/1_Orientation_Tracking/1_Python_Codes/modules) 
| Thu, 25/08/2018       | The main function are updated in  [Version 1 Update](3_Technical_Design/1_Orientation_Tracking/1_Python_Codes/main_program/vgg16_sunshine_1.py)  | Versions updating details are shown in [Code Updates](3_Technical_Design/1_Orientation_Tracking/1_Python_Codes/main_program) |
| Wed, 03/10/2018       | GUI for version 5 is completed   [GUI Update](3_Technical_Design/1_Orientation_Tracking/1_Python_Codes/main_program/vgg16_sunshine_5_gui.py)  | This GUI is based on version 5 and does not contain the function from later version  |
| Sun, 07/10/2018       | The final version has been finished   [Final Version Update](3_Technical_Design/1_Orientation_Tracking/1_Python_Codes/main_program/vgg16_sunshine_9.py)  | |



