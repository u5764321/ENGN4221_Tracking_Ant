# -*- coding: utf-8 -*-
"""
Created on Thu Aug 23 17:08:09 2018

@author: AW
"""
from keras.models import Sequential
from keras.layers.convolutional import Conv3D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
import numpy as np
import pylab as plt
from keras import backend as K
import cv2
from keras.layers import Flatten, Dropout, Dense
from keras.applications.vgg16 import VGG16
from keras.models import Model

def construct_label_dict(dir_path, label_file, testing = False):
    
    def get_direction(x1,y1,x2,y2):
        # 1 is left eye, 2 is right eye
        diff_x = (x2-x1)
        diff_y = -(y2-y1)
        
        if diff_y >=0:
            if diff_x>0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            elif diff_x==0:
                eye_angle = 90
            else:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 180 - eye_angle
        else:
            if diff_x>0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 360 - eye_angle
            elif diff_x==0:
                eye_angle = 270
            else:   
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = eye_angle + 180
        eye_angle =  eye_angle+90
        if eye_angle>360:
            eye_angle = eye_angle-360
        return eye_angle

    f = open(dir_path+label_file)
    n = 0
    content = []
    for line in f.readlines():
        
        name = line[0:5].replace(' ','')
        labels = line[16:-1]
        labels = labels.split('    ')
        content.append([name+'.png', labels])
        n +=1
    
    content = content[3:]
    new_content = []
    for elems in content:
    
        error_data = 0
        new_elems = []
        new_elems.append(elems[0])
        new_labels = []
        num_var = 4
        for i in elems[1]:
            new_labels.append(int(i))
            num_var = num_var-1
            if num_var <= 0:
                break
        if testing:
            new_elems.append(np.mean(new_labels))
        else:
            
            x1 = new_labels[0]
            y1 = new_labels[1]
            x2 = new_labels[2]
            y2 = new_labels[3]
            if (x1!=0)&(x2 !=0):
                new_elems.append(get_direction(x1,y1,x2,y2))
            else:
                error_data = 1
                new_elems = 1
        if error_data!=1:
            new_content.append(new_elems)
    return new_content


def data_generator(data_path, label_dict, batch_size, time_step, min_angel, max_angel, size_x=None, size_y=None, training=True):
    num_dataset = len(label_dict)
    print('time_step ',time_step )
    print('batch_size ',batch_size )
    length_batch = time_step + batch_size -1
    steps_each_epoch = int(np.floor(num_dataset/ length_batch))
    print('num_dataset,',num_dataset)
    print('length_batch,',length_batch)
    print('start')
    # Training generator
    if training:
        print('good day')
        while 1:  
            #num of batch -- for each batched sample
            for i in range(steps_each_epoch):
                mini_batch = label_dict[i * length_batch: (i + 1) * length_batch]
                batched_X = []
                batched_Y = []
                print('find a batched sample, which size is:',np.shape(mini_batch))
    
                for start_frame_index in range(batch_size):
                    mini_frames = mini_batch[start_frame_index: start_frame_index+time_step]
                    time_X = []
                    time_Y = []
                    print('find frame, swhich size is:',np.shape(mini_frames))
                    #for each time frame -- a single img
                    for image_pair in mini_frames:
                        print(image_pair[0])
                        #image_pair[0]: name 
                        #image_pair[1]: label
                        image_path = data_path+'//'+image_pair[0]
                        image = cv2.imread(image_path)
                        image = image[240:440,550:750,:]
                        #### keep image normalization please
                        image = np.divide(image, 255.0)
                        image = np.subtract(image, 0.5)
                        image = np.multiply(image, 2.0)
                        if (size_x!=None) & (size_y!=None):
                            image = cv2.resize(image, (size_x, size_y))
                        #x,y,z = np.shape(image)
                        #image = np.reshape(image,(x,y,1))
                        #### data normalization please
                        label = image_pair[1]
                        label = (label - min_angel)/ (max_angel-min_angel)    
                        time_X.append(image)
                        time_Y.append(label)
                    batched_X.append(time_X)
                    batched_Y.append(time_Y)
                print(np.shape(batched_X))
                batched_X = np.array(batched_X)
                batched_Y = np.array(batched_Y)
                yield batched_X, batched_Y 
    # Test generator
    else:
        #num of batch -- for each batched sample
        for i in range(steps_each_epoch):
            mini_batch = label_dict[i * length_batch: (i + 1) * length_batch]
            batched_X = []
            batched_Y = []
            print('find a batched sample, which size is:',np.shape(mini_batch))

            for start_frame_index in range(batch_size):
                mini_frames = mini_batch[start_frame_index: start_frame_index+time_step]
                time_X = []
                time_Y = []
                print('find frame, swhich size is:',np.shape(mini_frames))
                #for each time frame -- a single img
                for image_pair in mini_frames:
                    print(image_pair[0])
                    #image_pair[0]: name 
                    #image_pair[1]: label
                    image_path = data_path+'//'+image_pair[0]
                    image = cv2.imread(image_path)
                    image = image[240:440,550:750,:]
                    #### keep image normalization please
                    image = np.divide(image, 255.0)
                    image = np.subtract(image, 0.5)
                    image = np.multiply(image, 2.0)
                    if (size_x!=None) & (size_y!=None):
                        image = cv2.resize(image, (size_x, size_y))
                    #x,y,z = np.shape(image)
                    #image = np.reshape(image,(x,y,1))
                    #### data normalization please
                    label = image_pair[1]
                    label = (label - min_angel)/ (max_angel-min_angel)    
                    time_X.append(image)
                    #error_solver 
                    label = np.array([label])
                    time_Y.append(label)
                batched_X.append(time_X)
                batched_Y.append(time_Y)
            print(np.shape(batched_X))
            batched_X = np.array(batched_X)
            batched_Y = np.array(batched_Y)
            yield batched_X, batched_Y 


def find_normalise_parameters(dataset):

    angel = []
    for d in dataset:
        subset_angel = []
        subset_angel = [d[i][1] for i in range(len(d))]
        angel += subset_angel
    max_angel = np.sort(np.array(angel))[-1] + 0.01
    min_angel = np.sort(np.array(angel))[0]
    return min_angel, max_angel

if __name__ =="__main__":
    # parameter
    
    epoch = 1
    data_path = r'.\test_data'
    label_file = r'/test.pos'
    # construct train and valid set
    label_dict = construct_label_dict(data_path, label_file)

    num_data = len(label_dict)
    num_train = int(np.floor(num_data*0.8))
    num_valid = int(np.floor(num_data*0.1))
    num_test = int(np.floor(num_data*0.1))
    
    #randomly shuffle the entire set
    #random.shuffle(label_dict)
    
    train_dict = label_dict[0:num_train]
    valid_dict = label_dict[num_train:num_train+num_valid]
    test_dict = label_dict[num_train+num_valid:]

    #### get min and max angel as normalise parameters instead of 360
    dicts = [train_dict,valid_dict, test_dict]
    min_angel, max_angel = find_normalise_parameters(dicts)
    
    batch_size = 1
    time_step = 3
    #### note: data normalization
    train_generator = data_generator(data_path, train_dict, batch_size, time_step , min_angel, max_angel, 224, 224, False)
    valid_generator = data_generator(data_path, valid_dict, batch_size, time_step ,  min_angel, max_angel, 224, 224)
    test_generator = data_generator(data_path, test_dict, batch_size, time_step , min_angel, max_angel, 224, 224, False)
    
    a = []
    b = []
    for i in train_generator:
        a.append(i[0])
        b.append(i[1])
    a = np.array(a)
    b = np.array(b)    