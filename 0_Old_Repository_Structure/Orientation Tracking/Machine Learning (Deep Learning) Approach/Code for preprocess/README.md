generator.py:
    
    Create an arbitrary data generator for Keras frame.
    include functions:
        
        construct_label_dict(dir_path, label_file, testing = False)
        data_generator(data_path, label_dict, batch_size, time_step, min_angel, max_angel, size_x=None, size_y=None, training=True)
        find_normalise_parameters(dataset)
        
        
image_showing.py:
    
    Display a image with path. It also includes the function of cropping, resizing and angle detection
    This file is not encapsulated.
    
    
img_pre.py
    
    Preprocess steps and file convtering steps to be used in this task. 
    include funtion:
        
        construct_label_dict(dir_path, label_file, testing = False)
        img_display(dic, path)
        update_name(path)
        image_cut_off(path)
        add_a_in_name(path)
        change_img_name(path)
        get_direction(x1,y1,x2,y2)
        