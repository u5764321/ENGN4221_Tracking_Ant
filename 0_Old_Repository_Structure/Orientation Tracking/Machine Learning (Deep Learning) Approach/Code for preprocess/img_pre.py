# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 01:03:02 2018

@author: AW
"""

import numpy as np 
import cv2


path = r'C:\Users\zhenz_000\Desktop\Ant_tracking\test_data'
label_file = '\\test.pos'

def construct_label_dict(dir_path, label_file, testing = False):
    
    def get_direction(x1,y1,x2,y2):
        k = -(y1-y2)/(x1-x2)
        angle = np.arctan(k)
        angle = angle/(2*np.pi)*360
        return angle

    f = open(dir_path+label_file)
    n = 0
    content = []
    for line in f.readlines():
        
        name = line[0:5].replace(' ','')
        labels = line[16:-1]
        labels = labels.split('    ')
        content.append([name+'.png', labels])
        n += 1
    
    content = content[3:]
    new_content = []
    for elems in content:
        new_elems = []
        new_elems.append(elems[0])
        new_labels = []
        num_var = 4
        for i in elems[1]:
            new_labels.append(int(i))
            num_var = num_var-1
            if num_var <= 0:
                break
        if testing:
            new_elems.append(new_labels)
        else:
            
            x1 = new_labels[0]
            y1 = new_labels[1]
            x2 = new_labels[2]
            y2 = new_labels[3]
            new_elems.append(get_direction(x1,y1,x2,y2))
        new_content.append(new_elems)
    return new_content

path = r'C:\Users\zhenz_000\Desktop\Ant_tracking\testing\testing\test_data'
#dict_a = construct_label_dict(path, label_file)
def img_display(dic, path):
    for i in dic:
        print(path+'//'+i[0])
        img = cv2.imread(path+'//'+i[0])
        
        label = i[1]
        point1 = label[:2]
        point2 = label[2:]
        size = 2
        img[point1[1]-size:point1[1]+size, point1[0]-size:point1[0]+size, :] = 0
        img[point2[1]-size:point2[1]+size, point2[0]-size:point2[0]+size, :] = 0
        img[point1[1]-size:point1[1]+size, point1[0]-size:point1[0]+size, 1] = 255
        img[point2[1]-size:point2[1]+size, point2[0]-size:point2[0]+size, 1] = 255
        cv2.imshow("Image", img) 
        cv2.waitKey (0)
        cv2.destroyAllWindows()


label_dict = construct_label_dict(path, label_file, True)
img_display(label_dict, path)

def update_name(path):
    import os
    list_dir = os.listdir(path)
    n = 0;
    for file in list_dir:
        if file.endswith('.png'):
            print(file)
            new_name = file[-9:]
            img_name = int(new_name[:-4])
            img_name = str(img_name)+'.png'
            print(img_name)
            os.rename(path+'\\'+file, path+'\\'+img_name)


def image_cut_off(path):
    import os
    import cv2
    list_dir = os.listdir(path)
    n = 0;
    for file in list_dir:
        image = cv2.imread(path+'\\'+file)
        
        size = 5
        x = 550
        y = 240
        image[y-size:y+size,x-size:x+size,:] = 255
        x = 750
        y = 450
        image[y-size:y+size,x-size:x+size,:] = 255
        '''f
        cv2.imshow("Image", image) 
        cv2.waitKey (0)
        cv2.destroyAllWindows()
        '''
        new_image = image[240:440,550:750,:]
        cv2.imshow("Image", new_image) 
        cv2.waitKey (0)
        cv2.destroyAllWindows()
        
    
path = '.\\test_data'
     
image_cut_off(path)

def add_a_in_name(path):
    import os
    file_list = os.listdir(path)
    for file in file_list:
        if file.endswith('png'):
            name = file[:-4]
            name = 'a'+name
            os.rename(path+'//'+file, path+'//'+name)

def change_img_name(path):
    import os
    file_list = os.listdir(path)
    for file in file_list:
        if not file.endswith('pos'):
            name = file[1:]
            name = int(name)
            name = name-1
            name = str(name)+'.png'
            os.rename(path+'//'+file, path+'//'+name)
    


def get_direction(x1,y1,x2,y2):
    # 1 is left eye, 2 is right eye
    diff_x = (x2-x1)
    diff_y = -(y2-y1)
    if diff_y >=0:
        if diff_x>=0:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
        else:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 180 - eye_angle
    else:
        if diff_x>=0:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 360 - eye_angle
        else:   
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = eye_angle + 180
    eye_angle =  eye_angle+90
    if eye_angle>360:
        eye_angle = eye_angle-360
    return eye_angle