
from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.optimizers import Adam
import numpy as np
from keras import backend as K
import cv2
from keras.layers import Flatten, Dropout, Dense
from keras.applications.vgg16 import VGG16
from keras.models import Model, load_model

# We create a layer which take as input movies of shape
# (n_frames, width, height, channels) and returns a movie
# of identical shape.
K.set_image_dim_ordering('tf')


def data_generator(data_path, label_dict, batch_size, min_angel, max_angel, size_x=None, size_y=None, training=True):

    num_dataset = len(label_dict)
    steps_each_epoch = int(num_dataset / batch_size)
    # Training generator
    if training:
        while 1:  
            for i in range(steps_each_epoch):
                #start load data in batch 
                mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
                X = []
                Y = []
                for image_pair in mini_batch:
                    #image_pair[0]: name 
                    #image_pair[1]: label
                    image_path = data_path+'//'+image_pair[0]
                    image = cv2.imread(image_path)
                    image = image[240:440,550:750,:]
                    #### keep image normalization please
                    image = np.divide(image, 255.0)
                    image = np.subtract(image, 0.5)
                    image = np.multiply(image, 2.0)
                    if (size_x!=None) & (size_y!=None):
                        image = cv2.resize(image, (size_x, size_y))
                    #x,y,z = np.shape(image)
                    #image = np.reshape(image,(x,y,1))
                    #### data normalization please
                    label = image_pair[1]
                    label = (label - min_angel)/ (max_angel-min_angel)
                    X.append(image)
                    Y.append(label)
                    X = np.array(X)
                    Y = np.array(Y)
                yield (X,Y)
    # Test generator
    else:
        for i in range(steps_each_epoch):
            #start load data in batch 
            mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
            X = []
            Y = []
            for image_pair in mini_batch:
                #image_pair[0]: name 
                #image_pair[1]: label
                image_path = data_path+'//'+image_pair[0]
                image = cv2.imread(image_path)
                image = image[240:440,550:750,:]
                #### data normalization please
                image = np.divide(image, 255.0)
                image = np.subtract(image, 0.5)
                image = np.multiply(image, 2.0)
                if (size_x!=None) & (size_y!=None):
                    image = cv2.resize(image, (size_x, size_y))
                #x,y,z = np.shape(image)
                #image = np.reshape(image,(x,y,1))
                label = image_pair[1]
                X.append(image)
                Y.append(label)
                X = np.array(X)
                Y = np.array(Y)
            yield (X,Y)

def build_model(input_shape,regr_num=1):

    # 初始化全连接层的resnet网络
    baseModel = VGG16(include_top=False, weights=None, input_shape=input_shape)
    # 导入权重
    baseModel.load_weights('./vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',by_name=True)
    # 导出resnet最后一层输出，接上全连接层
    x = baseModel.output
    x = Flatten()(x)
    # 注意：在回归函数中，（vv）选择sigmoid作为激活函数是为了将输出归一化到0-1之间，使得loss稳定 容易收敛，相对的输入也必须归一化到0-1之间。
    # 可根据实际情况，选择激活函数，也可以恒等映射（不建议）
    regr_predictions = Dense(regr_num, kernel_initializer='normal', activation='sigmoid', name='fc_regr')(x)

    # 初始化以baseModel的input（输入数据）为input，全连接层输出的预测回归结果为output的模型
    regr_model = Model(inputs=baseModel.input, outputs=regr_predictions)
    
    return regr_model


def train_model(model, epoch,
                train_generator, train_steps, valid_generator, valid_steps, trained_model_path = False):
    
    '''
    if trained_model:
        print('not complated')
    else:
    '''
    model.compile(optimizer=Adam(lr=0.0001, beta_1=0.9, beta_2=0.999, epsilon=1e-08),
            loss='mse', metrics=['mse'])
    
    if trained_model_path:
        checkpointer = ModelCheckpoint(filepath='./'+ trained_model_path,
                        monitor='mean_squared_error',verbose=1, save_best_only=True, mode='min')    
        model.fit_generator(generator=train_generator,
                            epochs=epoch,
                            steps_per_epoch=train_steps,
                            verbose=1,
                            validation_data=valid_generator,
                            validation_steps=valid_steps,
                            callbacks=[checkpointer])
    else:
        model.fit_generator(generator=train_generator,
                    epochs=epoch,
                    steps_per_epoch=train_steps,
                    verbose=1,
                    validation_data=valid_generator,
                    validation_steps=valid_steps)

def predict_model(test_generator, batch_size=1):

    model = load_model('./vgg_sunshine.h5')
    ground = []
    predicted = []
    for elem in test_generator:
        result_elem = model.predict(elem[0], batch_size)
        # ground truth / evaluated result
        ground.append(elem[1])
        predicted.append(result_elem)
    
    ground = np.array(ground)
    predicted = np.array(predicted)
    ground = np.reshape(ground, -1)
    predicted = np.reshape(predicted, -1)
    # diff = (ground-predicted)/ground
    # output_list = [ground, predicted, diff]
    return ground, predicted

def construct_label_dict(dir_path, label_file, testing = False):
    
    def get_direction(x1,y1,x2,y2):
        # 1 is left eye, 2 is right eye
        diff_x = (x2-x1)
        diff_y = -(y2-y1)
        if diff_y >=0:
            if diff_x>=0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            else:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 180 - eye_angle
        else:
            if diff_x>=0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 360 - eye_angle
            else:   
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = eye_angle + 180
        eye_angle =  eye_angle+90
        if eye_angle>360:
            eye_angle = eye_angle-360
        return eye_angle

    f = open(dir_path+label_file)
    n = 0
    content = []
    for line in f.readlines():
        
        name = line[0:5].replace(' ','')
        labels = line[16:-1]
        labels = labels.split('    ')
        content.append([name+'.png', labels])
        n +=1
    
    content = content[3:]
    new_content = []
    for elems in content:
        new_elems = []
        new_elems.append(elems[0])
        new_labels = []
        num_var = 4
        for i in elems[1]:
            new_labels.append(int(i))
            num_var = num_var-1
            if num_var <= 0:
                break
        if testing:
            new_elems.append(np.mean(new_labels))
        else:
            
            x1 = new_labels[0]
            y1 = new_labels[1]
            x2 = new_labels[2]
            y2 = new_labels[3]
            new_elems.append(get_direction(x1,y1,x2,y2))
        new_content.append(new_elems)
    return new_content

def find_normalise_parameters(dataset):

    angel = []
    for d in dataset:
        subset_angel = []
        subset_angel = [d[i][1] for i in range(len(d))]
        angel += subset_angel
    max_angel = np.sort(np.array(angel))[-1] + 0.01
    min_angel = np.sort(np.array(angel))[0]
    return min_angel, max_angel

def restore_prediction(prediction, min_angel, max_angel):

    return [pred*(max_angel-min_angel) + min_angel for pred in prediction]

def cal_mse(ground_truth, prediction):
    return ground_truth- prediction

def save_model(model, name = 'new_vgg_model.h5'):
    model.save(name)

if __name__ =="__main__":
    # parameter
    batch_size = 1
    epoch = 1
    data_path = './test_data'
    label_file = '/test.pos'
    # construct train and valid set
    label_dict = construct_label_dict(data_path, label_file)

    num_data = len(label_dict)
    num_train = int(np.floor(num_data*0.8))
    num_valid = int(np.floor(num_data*0.1))
    num_test = int(np.floor(num_data*0.1))

    train_dict = label_dict[0:num_train]
    valid_dict = label_dict[num_train:num_train+num_valid]
    test_dict = label_dict[num_train+num_valid:]

    #### get min and max angel as normalise parameters instead of 360
    #### Attentation: you should remove test_dict!!!
    dicts = [train_dict,valid_dict, test_dict]
    min_angel, max_angel = find_normalise_parameters(dicts)

    steps_each_epoch = int(num_data / batch_size)
    #### please keep image size as imagenet dataset
    model = build_model([224, 224,3])

    #### note: data normalization
    train_generator = data_generator(data_path, train_dict, batch_size, min_angel, max_angel, 224, 224)
    valid_generator = data_generator(data_path, valid_dict, batch_size, min_angel, max_angel, 224, 224)
    test_generator = data_generator(data_path, test_dict, batch_size, min_angel, max_angel, 224, 224, False)

    train_model(model, epoch, train_generator, steps_each_epoch, valid_generator, steps_each_epoch, trained_model_path = 'vgg_sunshine.h5')
    #### note changes
    ground, predicted = predict_model(test_generator, batch_size)
    #### restore model's output
    pred_angel = restore_prediction(predicted ,min_angel, max_angel)
    #### calculate mse  for single sample, you can sum
    mse_error = cal_mse(ground, pred_angel)
    print('ground_truth:{0}\n, predicted_angel:{1}\n, mse_error:{2}\n'.format(ground, pred_angel, mse_error))
    save_model(model)