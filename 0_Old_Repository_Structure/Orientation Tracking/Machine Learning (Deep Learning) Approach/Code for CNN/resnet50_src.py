# -*- coding: utf-8 -*-
import os
import cv2
import numpy as np
import ntpath
import tensorflow as tf
from keras.applications.resnet50 import ResNet50
from keras.layers import Flatten, Dropout, Dense
from keras.optimizers import Adam
from keras.models import Model, load_model
from keras.callbacks import ModelCheckpoint, TensorBoard
from data_generator import data_generator, load_testData, get_steps_per_epoch
from keras import backend as K
K.set_image_dim_ordering('tf')

#gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.5)
#sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

def build_model(img_rows, img_cols):
    if K.image_dim_ordering() == 'th':
        print('image_dim_ordering = th')
    baseModel = ResNet50(include_top=False, weights=None, input_shape=(img_rows, img_cols,3))
    baseModel.load_weights('/disk/sas/sdg/chenban/keras_weights/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5')

    x = baseModel.layers[172].output # activation layer
    x = Flatten()(x)
    x = Dropout(0.3)(x)
    predictions = Dense(1, kernel_initializer='normal', activation='sigmoid', name='fc1')(x)
    model = Model(inputs=baseModel.input, outputs=predictions)

    LAYERS_TO_FREEZE = 163
    for layer in model.layers[:LAYERS_TO_FREEZE]:
        layer.trainable = False
    for layer in model.layers[LAYERS_TO_FREEZE:]:
        layer.trainable = True

    return model

def get_predict_label(predict_Y, thres): # 获取模型预测后的两类标签
    predict_label = []
    num = np.shape(predict_Y)[0]
    for i in range(num):
        if predict_Y[i, 0] > thres:
            predict_label.append(1.)
        else:
            predict_label.append(0.)
    predict_label = np.array(predict_label)
    return predict_label

def train(trainingData, validationData, trained_model, imgRows, imgCols, batchSize, epoch,class_f):
    model = build_model(imgRows, imgCols,)

    training_folder = 'filter_data'
    # Please implement your own load_data() module for your own dataset
    train_path = trainingData + training_folder + '/'
    valid_path = validationData + training_folder + '/'
    train_steps = get_steps_per_epoch(train_path, class_f, batchSize)
    valid_steps = get_steps_per_epoch(valid_path, class_f, batchSize)
    train_generator = data_generator(train_path, class_f,imgRows, imgCols, batchSize)
    valid_generator = data_generator(valid_path, class_f,imgRows, imgCols, batchSize)

    checkpointer = ModelCheckpoint(filepath=os.path.join(trained_model_path, 'best_v1.h5'),
                                   monitor='val_acc',
                                   verbose=1,
                                   save_best_only=True,
                                   mode='max')

    tensorboard = TensorBoard(  log_dir='/disk/sas/sdg/chenban/models/cnn_finetune_master_25/logs/resnet50_v1arg_logs',
                                histogram_freq=0,
                                batch_size=batchSize,
                                write_images=True,
                                write_grads=False)

    model.compile(optimizer=Adam(lr=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-08),
                  loss='binary_crossentropy', metrics=["accuracy"])

    model.fit_generator(generator=train_generator,
                        epochs=epoch,
                        steps_per_epoch=train_steps,
                        callbacks=[checkpointer, tensorboard],
                        verbose=1,
                        validation_data=valid_generator,
                        validation_steps=valid_steps)

    model.save_weights(trained_model + "/final_weights_v1.h5")
    model.save(trained_model + "/final_v1.h5")
    del model


def predict_best(trained_model_path, validationData, imgRows, imgCols, batchSize):
    best_model = load_model(os.path.join(trained_model_path, 'best_v1.h5'))
    test_x, test_y, test_files = load_testData(validationData + 'filter_data/', imgRows, imgCols, class_f)
    predictY_best = best_model.predict(test_x, batch_size=batchSize, verbose=1)
    predict_label = get_predict_label(predictY_best, 0.5)
    leakageRate = get_leakageRate(test_y, predict_label)
    noiseFactor = get_noiseFactor(test_y, predict_label)
    accuracy = get_accuracy(test_y, predict_label)
    print('resnet50')
    print("leakageRate in best: ", leakageRate)
    print("noiseFactor in best: ", noiseFactor)
    print("accuracy in best: ", accuracy)


def predict_final(trained_model_path, validationData, imgRows, imgCols, batchSize):
    final_model = load_model(os.path.join(trained_model_path, 'final_v1.h5'))
    test_x, test_y, test_files = load_testData(validationData + 'filter_data/', imgRows, imgCols, class_f)
    predictY_best = final_model.predict(test_x, batch_size=batchSize, verbose=1)
    predict_label = get_predict_label(predictY_best, 0.5)
    leakageRate = get_leakageRate(test_y, predict_label)
    noiseFactor = get_noiseFactor(test_y, predict_label)
    accuracy = get_accuracy(test_y, predict_label)
    print('resnet50')
    print("leakageRate in final: ", leakageRate)
    print("noiseFactor in final: ", noiseFactor)
    print("accuracy in final: ", accuracy)

def get_leakageRate(init_label, pre_label): # 计算漏检率
    '''
        全部阳性样本中，未被预测为阳性的概率
        :param init_label:
        :param pre_label:
        :return:
    '''
    sum1 = 0
    sum2 = 0
    for i in range(len(pre_label)):
        if init_label[i] == 1:  # 所有阳性样本
            sum1 += 1
            if pre_label[i] == 1:  # 实际为阳性，预测为阳性
                sum2 += 1
    sensitivity = float(sum2) / float(sum1) # 阳性样本召回率
    sensitivity = (1.0 - sensitivity)
    return sensitivity

def get_noiseFactor(init_label, pre_label): # 计算误检率
    '''
    全部阴性样本中，判断为阳性的比例
    :param init_label:
    :param pre_label:
    :return:
    '''
    sum1 = 0
    sum2 = 0
    for i in range(len(pre_label)):
        if init_label[i] == 0:  # 全部阴性样本
            sum1 += 1
            if pre_label[i] == 0:  # 阴性样本预测为阴性
                sum2 += 1
    specificity = float(sum2) / float(sum1) # 阴性样本召回率
    specificity = (1.0 - specificity)
    return specificity


def get_accuracy(init_label, pre_label): # 计算准确率
    assert len(init_label) == len(pre_label)
    sum_label = len(init_label)
    sum2 = 0
    for i in range(len(pre_label)):
        if init_label[i] == 0:  # 全部阴性样本
            if pre_label[i] == 0:  # 阴性样本预测为阴性
                sum2 += 1
        elif init_label[i] == 1:  # 全部阳性样本
            if pre_label[i] == 1:  # 阳性样本预测为阳性
                sum2 += 1
    accuracy = float(sum2) / float(sum_label)
    return accuracy


def save_best_result(trained_model_path, validationData, imgRows, imgCols, batchSize):
    '''

    :param testData:
    :param imgRows:
    :param imgCols:
    :param modelPath:
    :param batchSize:
    :return:
    '''
    best_model = load_model(os.path.join(trained_model_path, 'best_v1.h5'))
    test_x, test_y, test_files = load_testData(validationData + 'filter_data/', imgRows, imgCols, class_f)
    predictY_best = best_model.predict(test_x, batch_size=batchSize, verbose=1)
    predict_label = get_predict_label(predictY_best, 0.5)

    save_path = './result_image/'
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    f1_path = (save_path + "f1/")
    if not os.path.exists(f1_path):
        os.mkdir(f1_path)
    f0_path = (save_path + "f0/")
    if not os.path.exists(f0_path):
        os.mkdir(f0_path)

    for i in range(len(predict_label)):
        if test_y[i] == 1:  # 所有阳性样本
            if predict_label[i] == 0:  # 实际为阳性，预测为阴性
                image = cv2.imread(test_files[i])
                image_name = ntpath.basename(test_files[i])
                cv2.imwrite(f1_path + image_name, image)
        elif test_y[i] == 0:
            if predict_label[i] == 1:
                image = cv2.imread(test_files[i])
                image_name = ntpath.basename(test_files[i])
                cv2.imwrite(f0_path + image_name, image)


if __name__ == '__main__':
    batchSize = 32
    epoch = 100
    class_f = ['f0', 'f1']

    weight_path = '/disk/sas/sdg/chenban/keras_weights/'
    imgRows, imgCols = 224, 224  # input shapetest
    trained_model_path = '/disk/sas/sdg/chenban/trained_models/resnet50/'
    training_data_path = '/disk/sas/sdg/chenban/Data/IOT/Trainarg_v1_24/'
    #training_data_path = '/disk/sas/sdg/chenban/Data/IOT/Train_set_v2/'
    validation_data_path = '/disk/sas/sdg/chenban/Data/IOT/Test_set_v1_24/'

    # model = build_model(imgRows, imgCols)
    train(training_data_path, validation_data_path, trained_model_path, imgRows, imgCols, batchSize, epoch,class_f)
    predict_best(trained_model_path, validation_data_path, imgRows, imgCols, batchSize)
    predict_final(trained_model_path, validation_data_path, imgRows, imgCols, batchSize)
    #save_best_result(trained_model_path, validation_data_path, imgRows, imgCols, batchSize)



