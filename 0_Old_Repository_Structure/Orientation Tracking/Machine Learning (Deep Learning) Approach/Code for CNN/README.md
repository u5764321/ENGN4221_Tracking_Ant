resnet50_src.py

    The source file of ResNet50 in keras frame.
    
    
test_lstm.py

    A test file for CNN-LSTM model, to be tested with larger dataset.
    
    
test_resnet.py

    A test file for ResNet50 model, to be tested with larger dataset.
    
    
test_vgg16.py

    A test file for VGG16 model, it is tested.
    

vgg16_sunshine.py

    The current version of VGG16 model, to be improved with larger dateset.