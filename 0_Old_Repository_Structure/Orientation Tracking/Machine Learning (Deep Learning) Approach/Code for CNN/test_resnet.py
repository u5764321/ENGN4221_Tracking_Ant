from keras.models import Sequential
from keras.layers.convolutional import Conv3D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
import numpy as np
import pylab as plt
from keras import backend as K
import cv2
from keras.layers import Flatten, Dropout, Dense
from keras.applications.vgg16 import VGG16
from keras.models import Model

# We create a layer which take as input movies of shape
# (n_frames, width, height, channels) and returns a movie
# of identical shape.
K.set_image_dim_ordering('tf')


def data_generator(data_path, label_dict, batch_size = 1, size_x = 224, size_y = 400):
    num_dataset = len(label_dict)
    steps_each_epoch = int(num_dataset / batch_size)
    
    while 1:
    
        for i in range(steps_each_epoch):
            #start load data in batch 
            mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
            X = []
            Y = []
            for image_pair in mini_batch:
                #image_pair[0]: name 
                #image_pair[1]: label
                image_path = data_path+'//'+image_pair[0]
                image = cv2.imread(image_path)
                if (size_x!=None) & (size_y!=None):
                    image = cv2.resize(image, (size_x, size_y))
                #x,y,z = np.shape(image)
                #image = np.reshape(image,(x,y,1))
                label = image_pair[1]
                X.append(image)
                Y.append(label)
                X = np.array(X)
                Y = np.array(Y)
                
            yield (X,Y)
            

def build_model(input_shape,regr_num=1):

    # 初始化全连接层的resnet网络
    baseModel = VGG16(include_top=False, input_shape=input_shape)
    # 导入权重
    #baseModel.load_weights('./imagenet_models/resnet50/resnet50_weights_tf_dim_ordering_tf_kernels.h5',by_name=True)
    # 导出resnet最后一层输出，接上全连接层
    x = baseModel.output
    x = Flatten()(x)
    # 注意：在回归函数中，（vv）选择sigmoid作为激活函数是为了将输出归一化到0-1之间，使得loss稳定 容易收敛，相对的输入也必须归一化到0-1之间。
    # 可根据实际情况，选择激活函数，也可以恒等映射（不建议）
    regr_predictions = Dense(regr_num, kernel_initializer='normal', activation='sigmoid', name='fc_regr')(x)

    # 初始化以baseModel的input（输入数据）为input，全连接层输出的预测回归结果为output的模型
    regr_model = Model(inputs=baseModel.input, outputs=regr_predictions)
    return regr_model


def train_model(model, epoch,
                train_generator, train_steps, valid_generator, valid_steps, trained_model = False):
    '''
    if trained_model:
        print('not complated')
    else:
    '''
    model.compile(optimizer=Adam(lr=0.00001, beta_1=0.9, beta_2=0.999, epsilon=1e-08),
            loss='mse', metrics=['mse'])
    model.fit_generator(generator=train_generator,
                    epochs=epoch,
                    steps_per_epoch=train_steps,
                    verbose=1,
                    validation_data=valid_generator,
                    validation_steps=valid_steps)



def construct_label_dict(dir_path, label_file, testing = False):
    f = open(dir_path+label_file)
    n = 0
    content = []
    for line in f.readlines():
        
        name = line[0:5].replace(' ','')
        labels = line[16:-1]
        labels = labels.split('    ')
        content.append([name+'.png', labels])
        n +=1
    
    content = content[3:]
    new_content = []
    for elems in content:
        new_elems = []
        new_elems.append(elems[0])
        new_labels = []
        num_var = 4
        for i in elems[1]:
            new_labels.append(int(i))
            num_var = num_var-1
            if num_var <= 0:
                break
        if testing:
            new_elems.append(np.mean(new_labels))
        else:
            new_elems.append(new_labels)
        new_content.append(new_elems)
    return new_content
        

if __name__ =="__main__":
    #parameter
    batch_size = 1
    data_path = '.\\test_data'
    label_file = '\\test.pos'
    #construct train and valid set
    label_dict = construct_label_dict(data_path, label_file, True)

    num_data = len(label_dict)
    num_train = int(np.floor(num_data*0.8))
    num_valid = int(np.floor(num_data*0.2))
    train_dict = label_dict[0:num_train]
    valid_dict = label_dict[num_train:]
    
    steps_each_epoch = int(num_data / batch_size)
    model = build_model([224, 400,3])
    train_generator = data_generator(data_path, train_dict, batch_size)
    valid_generator = data_generator(data_path, valid_dict, batch_size)
    train_model(model, 1, train_generator, steps_each_epoch, valid_generator, steps_each_epoch)