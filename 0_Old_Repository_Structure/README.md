# Ant Tracking Project Landing Page

## Public Feedback
Please comment on this project and provide any feedback through the [Google Forms](https://goo.gl/forms/kiDe1UmwffYJyXR23) survey.

## Weekly Updates

| Week                                  | Contents                                          |
| :---:                                 | ---                                               |
| Week 1, 23 Jul - 29 Jul, 2018         | Team Formnation on 26 Jul                         |
| Week 2, 30 Jul - 05 Aug, 2018         | Client Meeting on 31 Jul, 02 Aug, topic finalised |
|                                       | Concept of Operations draft uploaded on 04 Aug    |
| Week 3, 06 Aug - 12 Aug, 2018         | Audit 1 on 06 Aug                                 |
|                                       | Client Meeting on 07 Aug, TDU requires new scope  |
|                                       | OT received ant walking images and videos         |
|                                       | A detail [WBS](Project Documents/0_work_breakdown_structure_v3_with_descriptions.pdf) is created by Tienan on 12 Aug               |
| Week 4, 13 Aug - 19 Aug, 2018         | A detail [Gantt Chart old_version](https://www.icloud.com/numbers/0pSnDPiuXL57h4IsBH_A6qjuA#tracking%5Fant%5Fgantt%5Fchart) is created by Tienan on 13 Aug    |
|                                       | Measurements on current trackball device          |
|                                       | Electronics selection and PCB design started      |
|                                       | Major components for new trackball device finalised |
|                                       | Initial PCB package dimension design completed |
|                                       | Computer Vision development stops, Machine Learning (Deep Learning) development starts|
| Week 5, 20 Aug - 26 Aug, 2018         | A [feedback](https://goo.gl/forms/kiDe1UmwffYJyXR23) channel is made available for the public |
|                                       | Revised version of [Gantt Chart](https://www.icloud.com/numbers/0lv1DTDurrmdIGanVhc3dUL1A#tracking%5Fant%5Fgantt%5Fchart%5FV2) with delayed TDU programming   |
|                                       | PCB schematics design conducted |

## Task Allocation

Tasks are currently allocated to team members through Trello.

[![trello_page](Pictures/document_pics/trivial_pics/trello_pic.png)](https://trello.com/b/IUsD1JJc)

## Team Member Contact

| Name                  | Email                     | Topic     | Administration Roles                                                  |
| ---                   | :---:                     | :---:     | ---                                                                   |
| Tienan Xu             | u5829270@anu.edu.au       | TDU       | Point of Contact; Trello, Landing Page & Repository Maintainer        |
| Yile Liu              | u5694993@anu.edu.au       | TDU       | TDU Leader                                                            |
| Fangxiao Dong         | u5698699@anu.edu.au       | TDU       | -                                                                     |
| Zhenzhen Liu          | u5625456@anu.edu.au       | TDU, OT   | -                                                                     |
| Shu Liu               | u5764321@anu.edu.au       | TDU, OT   | -                                                                     |
| Guofeng Xu            | u5491523@anu.edu.au       | OT        | OT Leader                                                             |

## Table Of Contents
1. [Welcome](#1-welcome)
2. [Project Description](#2-project-description)
3. [Concept of Operations](#3-concept-of-operations)
4. [Meetings](#4-meetings)
5. [Audit](#5-audit)
6. [Trackball Device Upgrade](#6-trackball-device-upgrade)
7. [Orientation Tracking](#7-orientation-tracking)

## 1. Welcome
Welcome to the Ant Tracking project. This project is conducting under the ANU Engineering Program ENGN4221 Systems Engineering Project.

In this page, you will be able to find files and resources published by the project team.


## 2. Project Description
The project aims to develop algorithms and equipments for the ANU Zeil Lab for improving current research on ant navigations, this includes:
1. Algorithms for tracking the ant’s head and body orientation through recorded videos of the ant walking on the trackball device.
2. New trackball device with improved electronics, wireless data transmission and real-time device orientation tracking.

## 3. Concept of Operations
Concept of Operations had been developed. It is expected to be signed-off in near future.

[Concept of Operations](Project Documents/Concept_of_Operations.md)

## 4. Meetings
### Client Meetings
| Client Meeting                                                                               | Date                                              | Comment                           |
| :---:                                                                                 | ---                                               | ---                               |
| Client Meeting 01                                                                     | Thu, 26/07/2018, Ian Ross Design Studio           | Team Formation Night              |
| [Client Meeting 02](Meeting Minutes/Client Meetings/client_meeting_minute_02.pdf)     | Tue, 31/07/2018, ANU Zeil Lab                     | First visit to lab                |
| [Client Meeting 03](Meeting Minutes/Client Meetings/client_meeting_minute_03.pdf)     | Thu, 02/08/2018, ANU Zeil Lab                     | Decide topics                     |
| [Client Meeting 04](Meeting Minutes/Client Meetings/client_meeting_minute_04.pdf)     | Tue, 07/08/2018, ANU Zeil Lab                     | TDU scope change                  |
| [Client Meeting 05](Meeting Minutes/Client Meetings/client_meeting_minute_05.pdf)     | Wed, 22/08/2018, ANU Zeil Lab                     | Progress update                  |

### Group Meetings
| Group Meeting                                                                             | Date                                                | Comment                             |
| :---:                                                                               | ---                                                 | ---                                 |
| [Group Meeting 01](Meeting Minutes/Group Meetings/group_meeting_minute_01.pdf)      | Wed, 01/08/2018, RSCS & MSI Building                | Topic discussion, role assignment   |
| [Group Meeting 02](Meeting Minutes/Group Meetings/group_meeting_minute_02.pdf)      | Sat, 04/08/2018, RSCS & MSI Building                | ConOps night                        |
| [Group Meeting 03](Meeting Minutes/Group Meetings/group_meeting_minute_03.pdf)      | Wed, 08/08/2018, RSCS & MSI Building                | Timeline and plan proposal, TDU scope determined |
| [Group Meeting 04](Meeting Minutes/Group Meetings/group_meeting_minute_04.pdf)      | Sun, 12/08/2018, Lena Karmel Lodge                  | Project Audit 1 Feedback discussion |
| [Group Meeting 05](Meeting Minutes/Group Meetings/group_meeting_minute_05.pdf)      | Sun, 19/08/2018, Lena Karmel Lodge                  | Mainly technique discussion           |

### Tutorial Meetings
| Tutorial Meeting                                                                                       | Date                                              | Comment                       |
| :---:                                                                                         | ---                                               | ---                           |
| [Tutorial Meeting Week 2](Meeting Minutes/Tutorial Meetings/tutorial_meeting_minute_week_2.pdf)   | Mon, 30/07/2018, Ian Ross Design Studio           | Discussion on repository      |
| [Tutorial Meeting Week 4](Meeting Minutes/Tutorial Meetings/tutorial_meeting_minute_week_4.pdf)   | Mon, 13/08/2018, Ian Ross Design Studio           | Feedback on Audit 1, progress discussion  |
| [Tutorial Meeting Week 5](Meeting Minutes/Tutorial Meetings/tutorial_meeting_minute_week_5.pdf)   | Mon, 13/08/2018, Ian Ross Design Studio           | Technical work update  |

## 5. Audit
| Audit                                                                                 | Date                                              |
| :---:                                                                                 | ---                                               |
| [Audit 1](Project Documents/Audit 1/audit_1_ant_tracking_team.pdf)                    | Mon, 06/08/2018, Ian Ross Design Studio           |
| [Audit 2](Project Documents/Audit 2/)                                                 | Mon, 27/08/2018, Ian Ross Design Studio           |
| [Audit 3]                                                                             |                                                   |

## 6. Trackball Device Upgrade
[Work repository](Trackball Device Upgrade)

| Date                  | Update                                                        | Comment       |
| :---:                 | ---                                                           | ---           |
| Tue, 07/08/2018       | Major scope change is required, refer to [Client Meeting 04](Meeting Minutes/Client Meetings/client_meeting_minute_04.pdf)            | Scope not yet developed                   |
| Thu, 09/08/2018       | Timeline updated, refer to [Trackball Device Upgrade Timeline](Trackball Device Upgrade/timeline_trackball_device_upgrade_v2.pdf)     | A Gantt Chart will be soon developed      |
| Mon, 13/08/2018       | [WBS](Project Documents/0_work_breakdown_structure_v3_with_descriptions.pdf) and [Gantt Chart](https://www.icloud.com/numbers/0lv1DTDurrmdIGanVhc3dUL1A#tracking%5Fant%5Fgantt%5Fchart%5FV2) created |   |
| Tue, 14/08/2018       | The team visited Zoltan to acquire the PCB design software - Eagle and conducted measurement on current trackball device | Measuring work to be continued tomorrow |
| Wed, 15/08/2018       | The team continued measuring dimensional and geometrical data of current trackball device |   A report will be created and uploaded   |
| Sat, 18/08/2018       | [Report](Trackball Device Upgrade/Current Trackball Device Analysis/Major_Components_and_Functions.pdf) on current device measurements uploaded                ||
| Sun, 19/08/2018       | Initial [PCB package dimension design](Trackball Device Upgrade/New Trackball Device Designs/Printed_Circuit_Board_Design.pdf) completed                                  | Uploaded on Wed, 22/08/2018 |
| Wed, 22/08/2018       | A [subsystem interface](Trackball Device Upgrade/new_trackball_device_subsystem_interface.pdf) is created for the new trackball device    | A report will be created and uploaded |
| Thu, 23/08/2018       | [Report](Trackball Device Upgrade/Current Trackball Device Analysis/Electronic_Components.pdf) on current device electronics uploaded ||
| Mon, 27/08/2018       | [Report](Trackball Device Upgrade/Explanation_on_new_trackball_device_subsystem_interface.pdf) on the Subsystem Interface uploaded    ||

## 7. Orientation Tracking
[Work repository](Orientation Tracking)

| Date                  | Update                                                        | Comment       |
| :---:                 | ---                                                           | ---           |
| Wed, 08/08/2018       | Timeline updated, refer to [Orientation Tracking Timeline](Orientation Tracking/timeline_orientation_tracking_v2.pdf)    | A Gantt Chart will be soon developed          |
| Thu, 09/08/2018       | Received 1000 annotaed ant orientation images from client     | Hard drive picked up from Client's office |
| Mon, 13/08/2018       | [WBS](Project Documents/0_work_breakdown_structure_v3_with_descriptions.pdf) and [Gantt Chart](https://www.icloud.com/numbers/0lv1DTDurrmdIGanVhc3dUL1A#tracking%5Fant%5Fgantt%5Fchart%5FV2) created |   |
| Sun, 19/08/2018       | Machine Learning (Deep Learning) [research and algorithm](Orientation Tracking/Machine Learning (Deep Learning) Approach/Deep_Learning_Network_Architecture_V2.pdf) explained ||
|                       | An [analysis](Orientation Tracking/Computer Vision Approach/Analysis_on_Computer_Vision_Approach.pdf) on Computer Vision approach is done   ||
| Wed, 22/08/2018       | Deep Learning model building test on GPU successed ||
