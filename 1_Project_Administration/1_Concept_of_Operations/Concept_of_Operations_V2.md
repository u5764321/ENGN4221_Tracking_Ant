# Concept of Operation
## Project Goal
**_This project will develop a new ant trackball device and ant orientation tracking algorithms for the ANU Zeil Lab._**

## 1. Project Background

The ANU Zeil Lab is conducting research on exploring ants’ vision and navigation. The lab’s research focuses on investigating ants’  ability of using vision to navigate to their feeding tree, then back to nests. Zeil Lab has designed a series of experiments that involves:
1.	reconstruction of an ant’s living environment’s surrounding landscape into a 3-D digital model from pre-captured videos and images,
2.	a trackball device that encodes the movements of the ant walking on top of the device into displacements and paths,
3.	a software interface that maps the ant’s path data to locations in the digital model, and
4.	a 360-degree LED-matrix cinema that projects the surrounding landscape viewed from the ant's virtual location, which is updated while she walks on the trackball device.

It is notable that location mapping and landscape projection are simultaneous events. This means that the ant receives real-time feedback from the surrounding view for deciding which direction should be heading next. This simulates the scenario that the ant navigates through her natural environment.

## 2. Project Description

The project aims to address enquiries made by Zeil Lab regarding to the current experimental setups. Currently, Zeil Lab is able to perform experiments to generate paths that the ant takes for navigating to regions near her target destination (e.g., the nest), from a starting point specified in the digital model. However, the lab would like to have some improvements on the data aspects of the experiments, including:

[1] construction of new 3-D digital landscape models with UV colour channel and at a greater scale, and

[2] automated tracking of the ant’s head and body orientation through recorded videos of the ant in the experiment, this would enable the lab to investigate which parts of the 3-D landscape the ant is viewing while she navigates and helps understanding the path she takes.

In addition, there are two improvements around the trackball device that the lab would like to have:

[3] enabling wireless data transmission between the trackball device and lab computers, currently, the device transfers the ant’s movement data through a USB cable to lab computers, which limits the portability of the device, and

[4] adding a device that allows accurate tracking of the trackball device’s real-world position and orientation, this would help the lab incorporating trackball device’s position in the ant’s navigation data for both indoor and future outdoor experiments. Currently, it is concerned that the trackball device causes magnetic interference that stops common devices (such as magnetic compass) to track the orientation accurately.

## 3. Project Objective

The primary objective of the project is to deliver most value to Zeil Lab. This can be achieved by delivering the most needed improvements for the lab. Through meetings and discussions, it was determined that Improvement No. 2, 3 and 4 shall be investigated and developed for the lab. This was concluded from various considerations:

1.	the project will be a continuing in the future, handover is necessary,
2.	project team’s skills and capabilities, and
3.	Improvement No. 1 is at a lowest priority – existing digital models are satisfactory.

The project objective and goal are to deliver Improvement No. 2, 3 and 4 to Zeil lab, namely the Orientation Tracking and Trackball Device Upgrade objectives.

## 4. Project Scope

To address Improvement No. 2, the project aims to develop an orientation tracking algorithm. The algorithm shall be capable of identifying the ant’s head orientation from videos recorded under specified conditions, include but not limited to:

1.	ant is recorded from a top down view,
2.	camera is fixed in position throughout the video, and
3.	indoor and outdoor environments.

The algorithm shall be developed to be compatible with MATLAB or Python. 

To address Improvement No. 3, the project aims to develop a new Printed Circuit Board (PCB) that is able to replace the current board on the trackball device. The new board shall inherit existing functions from the current board, with the capability of wireless data transmission with lab computers. It is notable that simulation and prototyping may be conducted for verifying the feasibility.

To address Improvement No. 4, the project aims to develop a method that measures accurate orientation of the trackball device through interferences. Testings shall be done to examine the strength of magnetic interference caused by the trackball device. A suitable hardware shall be implemented to realise this function.

In addition, all final outputs are expected to be open source.

**The overall scope will be:**

**1. Developing head and body orientation tracking algorithms,**

**2. Upgrading the trackball device with new electronics (PCBs) and new components.**

## 5. Stakeholders
##### a. Stakeholder and responsibilities
| <p align="middle">Stakeholder  |  <p align="middle">Role     |  <p align="middle">Responsibility |
| :-----------                                  | :------: | ------------: |
| ANU Zeil Lab                                  | Project Client   | <p align="left">  Evaluate Project Team’s work progress and quality  <br /> Provides theoretical knowledge and recourse supports |
| Ant Tracking Team                              | Project Team   |  <p align="left"> 	Delivers project objectives   <br /> 	Act and reflect on feedbacks by other stakeholders    <br /> 	Act as Shadow Team for Shadow team stakeholders | 
| DER Team  <br /> SpaceShark Team <br /> YerraLoon Team | Shadow Team | <p align="left"> 	Evaluate Project Team’s work progress and quality <br />	Provide critical review from alternative perspectives <br />	Demonstrate own work and progress to Project Team |
| Don & Matt                                    | Tutor   | <p align="left">   	Evaluate Project Team’s work progress and quality <br />Conduct tutorial activities for Action-Learning Cycle |
| Chris                                          | Course Convener  | <p align="left"> 	Evaluate Project Team’s work progress and quality <br />Manage the course structure and overall progress  | 
| Manufacturer                                  | PCB manufacturing| <p align="left"> 	Manufacture PCBs according to the team's design <br />Providing expert opinions on if the designs are able to be manufactured  | 
| Agent                                          | Informant, negotiator  |<p align="left"> 	Establish easy communication between the team and the manufacturer <br /> Arranging and processing payments and ordering directly with the manufacturer  | 

##### b. Stakeholder interactions
![alt text](1_Project_Administration/1_Concept_of_Operations/stakeholder_analysis_V2.png)

##### c. The Project Team
|  <p align="middle">Members  |  <p align="middle">Affiliation     | <p align="middle"> Roles |
| :-----------                                  | :------: | ------------: |
|Guofeng Xu                             | Orientation Tracking   | <p align="left"> Computer vision specialist<br /> Affiliation POC|
|Shu Liu                      | Orientation Tracking |  <p align="left"> Computer vision & machine learning specialist<br /> | 
| Tienan Xu | Trackball Device Upgrade | <p align="left">Prototyping & testing specialist<br /> Documenter<br /> Project Team POC |
| Yile Liu                                    | Trackball Device Upgrade   | <p align="left">  Lead Electronics specialist<br /> Affiliation POC |
|Zhenzhen Liu                                        | Trackball Device Upgrade  | <p align="left"> Electronics specialist<br />  | 
|Fangxiao Dong                                    | Trackball Device Upgrade  |  <p align="left"> Electronics specialist<br /> | 

## 6. Resources
#### a. Orientation Tracking
##### i. Cost 
Since the orientation tracking objective is mainly a computer vision task, there is no obvious cost or budget. However, potential cost may include upgrading recording device for better video quality.
##### ii.	List of materials
List of materials describes the resources required for completing the objective.

| <p align="middle">Components |  <p align="middle">Quantity | <p align="middle">Comment | 
| :----------- | :------: | ------------: | 
|Videos of the ant    | (to be done)  | <p align="left">Not yet provided<br />The primary input for the tracking algorithm     | 
| Photos of the ant    | 1174   | <p align="left">This set of photos would be used as the training set for testing the tracing algorithm         | 
|Photos of the ant with manually labelled orientation  | (to be done)  |<p align="left"> Not yet provided<br />These set of photos would be used as a validation set for testing our algorithm| 
| Literatures and papers  | 1   |<p align="left">   Published in 1995, mainly about template matching and regression   | 
##### b. Trackball Device Upgrade
##### i. Cost 

| <p align="middle">Components |  <p align="middle">Cost |<p align="middle">Supplier | 
| :----------- | :------: | ------------: | 
| PCB manufacturing, including components costs    | $1000  | Shenzhen Qixin Electronics      | 
| Other components manufactruing    | -  |  University workshops
| USB RF receiver | - | Off-the-shelf suppliers (Digikey)

##### ii. Funding
Financial aspects of the project will be cover by the client.

However, methods for overseas payments and transfer should be discussed and arranged.

## 7. Technical Constraint
For Orientation Tracking, the main constraint is caused by the low quality of photo frames. Due to the exposure of the photos, some detailed features of the ant are not clear. Besides, since the ant on photos is fixed with a pole, that pole becomes an obstacle which blocks some parts of the ant’s body. These two constraints make the task complicated for developing a generalised solution.

For Trackball Device Upgrade, the main constraint is the need to use third-party manufacturers for manufacturing PCBs, as the Project Team self could not handle this task. This induces uncertainties such as device certifications, shipping, package handling and lead time. A possible contingency plan is to use off-the-shelf products to achieve the objective.

## 8. Deliverables and Milestones
Deliverables can be refered to the timelines for both Orientation Tracking and Trackball Device Upgrade, which also indicate project milestones scheduled by weeks. Updated on Saturday, 12 August 2018 (Week 3).

[Orientation Tracking](1_Project_Administration/4_Project_Management/2_Timeline/timeline_OT_v2.pdf)

[Trackball Device Upgrade](1_Project_Administration/4_Project_Management/2_Timeline/timeline_TDU_v2.pdf)

## 9. Audit Deliverables
|  | Activities| 
| :----------- | ------ | 
| Audit 1     | Project introduction, repository demonstration |
| Audit 2     | Orientation Tracking: demonstration of algorithm codes |
|             | Trackball Device Upgrade: demonstration of PCB designs |
| Audit 3     | Orientation Tracking: demonstration of orientation tracking results; demonstration of a User Interface   |
|             | Trackball Device Upgrade: demonstration of new trackball device |

## 10. Work Breakdown Structure
The project's Work Breakdown Structure can be viewed below. Updated on Saturday, 12 August 2018 (Week 3).

[Work Breakdown Structure](1_Project_Administration/4_Project_Management/1_Work_Breakdown_Structure/work_breakdown_structure_v3_with_descriptions.pdf)

## 11. Gantt Chart
A gantt Chart is developed for this project, it can be viewed dynamically on iCloud Numbers. Updated on Monday, 13 August 2018 (Week 4).

[Gantt Chart](https://www.icloud.com/numbers/0pSnDPiuXL57h4IsBH_A6qjuA#tracking%5Fant%5Fgantt%5Fchart)

## 12. Risk Analysis

Interactive version of the [Risk Analysis](1_Project_Administration/4_Project_Management/3_Risk_Analysis/risk_analysis_interactive.xlsx).

![Risk analysis](1_Project_Administration/1_Concept_of_Operations/risk_analysis.png)

## 13. Project Ethics
It is a common act to conduct ethics considerations on researches and experiments involving using of live animals. However, experiments that use invertebrates, such as ants, do not require ethics evaluation or approval from the University or research ethics committees. Despite this fact, the ANU Zeil Lab conducts their own ethical methodology. According to the client, this includes:
1. Ensure that ants are able to return to the nest after their day’s experiments.
2. Not conduct experiments if weather conditions are not favourable.
3. Cease experiments and return ants to the nest If ants become stressed during the experiments.
4. Offer ants that are involved in experiments food such as sugar and honey water solution.
5. Keep the number and duration of experiments and manipulations on ants to minimum.
6. Not conduct experiments on ants with too many failed manipulations.

The project team shall obey these rules, if at any stage the project requires conducting ant experiments.

## 14. Change Log:

| Concept_of_Operations.md      |   Original signed-off version |
| Concept_of_Operations_V2.md   |   Overall updated version     |
