<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="9" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Courtyard" color="13" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="near-ir">
<description>5mm x 4mm, 16 + 1 pin DFN package</description>
<packages>
<package name="LQFP48">
<description>LQFP48, specified by ST Microelectronics</description>
<smd name="6" x="-0.25" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="5" x="-0.75" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="4" x="-1.25" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="3" x="-1.75" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="2" x="-2.25" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="1" x="-2.75" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="7" x="0.25" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="8" x="0.75" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="9" x="1.25" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="10" x="1.75" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="11" x="2.25" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="12" x="2.75" y="-4.25" dx="0.3" dy="1.2" layer="1"/>
<smd name="18" x="4.25" y="-0.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="17" x="4.25" y="-0.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="16" x="4.25" y="-1.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="15" x="4.25" y="-1.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="14" x="4.25" y="-2.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="13" x="4.25" y="-2.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="19" x="4.25" y="0.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="20" x="4.25" y="0.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="21" x="4.25" y="1.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="22" x="4.25" y="1.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="23" x="4.25" y="2.25" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="24" x="4.25" y="2.75" dx="0.3" dy="1.2" layer="1" rot="R90"/>
<smd name="25" x="2.75" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="26" x="2.25" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="27" x="1.75" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="28" x="1.25" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="29" x="0.75" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="30" x="0.25" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="31" x="-0.25" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="32" x="-0.75" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="33" x="-1.25" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="34" x="-1.75" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="35" x="-2.25" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="36" x="-2.75" y="4.25" dx="0.3" dy="1.2" layer="1" rot="R180"/>
<smd name="37" x="-4.25" y="2.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="38" x="-4.25" y="2.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="39" x="-4.25" y="1.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="40" x="-4.25" y="1.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="41" x="-4.25" y="0.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="42" x="-4.25" y="0.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="43" x="-4.25" y="-0.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="44" x="-4.25" y="-0.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="45" x="-4.25" y="-1.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="46" x="-4.25" y="-1.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="47" x="-4.25" y="-2.25" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<smd name="48" x="-4.25" y="-2.75" dx="0.3" dy="1.2" layer="1" rot="R270"/>
<wire x1="-3.25" y1="3.25" x2="-3.25" y2="-3.25" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.25" x2="3.25" y2="-3.25" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.25" x2="3.25" y2="3.25" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.25" x2="-3.25" y2="3.25" width="0.127" layer="21"/>
<circle x="-2.75" y="-2.75" radius="0.25" width="0.127" layer="21"/>
<text x="-2.75" y="5.5" size="0.8" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-2.75" y="-6.25" size="0.8" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="C-5">
<wire x1="0.508" y1="0" x2="1.651" y2="0" width="0.127" layer="21"/>
<wire x1="-0.381" y1="0" x2="-1.651" y2="0" width="0.127" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002"/>
<text x="-2.54" y="1.778" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.54" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="0.254" y1="-1.524" x2="0.762" y2="1.524" layer="21"/>
<rectangle x1="-0.762" y1="-1.524" x2="-0.254" y2="1.524" layer="21"/>
</package>
<package name="C5B2,5">
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.127" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.127" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.127" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.127" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.127" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.127" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.127" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002"/>
<text x="4.191" y="0" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="4.191" y="-1.524" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="0805">
<wire x1="-1.75" y1="1" x2="-1.75" y2="-1" width="0.05" layer="101"/>
<wire x1="-1.75" y1="-1" x2="1.75" y2="-1" width="0.05" layer="101"/>
<wire x1="1.75" y1="-1" x2="1.75" y2="1" width="0.05" layer="101"/>
<wire x1="1.75" y1="1" x2="-1.75" y2="1" width="0.05" layer="101"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.1" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="-1.016" y2="-0.635" width="0.1" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="-1.016" y2="0.635" width="0.1" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="1.016" y2="0.635" width="0.1" layer="51"/>
<wire x1="-1.6764" y1="0.889" x2="1.6764" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.6764" y1="0.889" x2="1.6764" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.6764" y1="-0.889" x2="-1.6764" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.6764" y1="-0.889" x2="-1.6764" y2="0.889" width="0.1524" layer="21"/>
<smd name="2" x="0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<smd name="1" x="-0.9398" y="0" dx="1.0922" dy="1.397" layer="1"/>
<text x="-2.004" y="-3.011" size="0.8" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<text x="-1.994" y="2.008" size="0.8" layer="25" font="vector" ratio="20">&gt;NAME</text>
<rectangle x1="-1" y1="-0.625" x2="-0.625" y2="0.625" layer="51"/>
<rectangle x1="0.625" y1="-0.625" x2="1" y2="0.625" layer="51"/>
</package>
<package name="C-2,5">
<pad name="1" x="-1.27" y="0" drill="0.7" diameter="1.4224"/>
<pad name="2" x="1.27" y="0" drill="0.7" diameter="1.4224"/>
<text x="-1.905" y="1.016" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.905" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="-0.381" y1="-0.762" x2="-0.127" y2="0.762" layer="21"/>
<rectangle x1="0.127" y1="-0.762" x2="0.381" y2="0.762" layer="21"/>
</package>
<package name="C7,5B4">
<wire x1="-0.3048" y1="0.835" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.835" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-2.694" y2="0" width="0.127" layer="21"/>
<wire x1="0.3302" y1="0.835" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.835" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="2.694" y2="0" width="0.127" layer="21"/>
<wire x1="-5" y1="-2" x2="-5" y2="2" width="0.127" layer="21"/>
<wire x1="-5" y1="2" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="5" y1="2" x2="5" y2="-2" width="0.127" layer="21"/>
<wire x1="5" y1="-2" x2="-5" y2="-2" width="0.127" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1" diameter="2"/>
<pad name="2" x="3.81" y="0" drill="1" diameter="2"/>
<text x="5.461" y="0" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="5.461" y="-1.524" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="C5B3,5">
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.127" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.127" layer="21"/>
<wire x1="-3.683" y1="1.516" x2="-3.683" y2="-1.516" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-1.77" x2="3.429" y2="-1.77" width="0.127" layer="21"/>
<wire x1="3.683" y1="-1.516" x2="3.683" y2="1.516" width="0.127" layer="21"/>
<wire x1="3.429" y1="1.77" x2="-3.429" y2="1.77" width="0.127" layer="21"/>
<wire x1="3.429" y1="1.77" x2="3.683" y2="1.516" width="0.127" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.77" x2="3.683" y2="-1.516" width="0.127" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.516" x2="-3.429" y2="-1.77" width="0.127" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.516" x2="-3.429" y2="1.77" width="0.127" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002"/>
<text x="4.191" y="0" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="4.191" y="-1.524" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="C5B5">
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.127" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.127" layer="21"/>
<wire x1="-3.683" y1="2.278" x2="-3.683" y2="-2.278" width="0.127" layer="21"/>
<wire x1="-3.429" y1="-2.532" x2="3.429" y2="-2.532" width="0.127" layer="21"/>
<wire x1="3.683" y1="-2.278" x2="3.683" y2="2.278" width="0.127" layer="21"/>
<wire x1="3.429" y1="2.532" x2="-3.429" y2="2.532" width="0.127" layer="21"/>
<wire x1="3.429" y1="2.532" x2="3.683" y2="2.278" width="0.127" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.532" x2="3.683" y2="-2.278" width="0.127" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.278" x2="-3.429" y2="-2.532" width="0.127" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.278" x2="-3.429" y2="2.532" width="0.127" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" diameter="1.6002"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.6002"/>
<text x="4.191" y="0" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<text x="4.191" y="-1.524" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-1.6" y1="0.8" x2="1.6" y2="0.8" width="0.05" layer="51"/>
<wire x1="1.6" y1="0.8" x2="1.6" y2="-0.8" width="0.05" layer="51"/>
<wire x1="1.6" y1="-0.8" x2="-1.6" y2="-0.8" width="0.05" layer="51"/>
<wire x1="-1.6" y1="-0.8" x2="-1.6" y2="0.8" width="0.05" layer="51"/>
<wire x1="-2" y1="1.1" x2="-2" y2="-1.1" width="0.05" layer="21"/>
<wire x1="-2" y1="-1.1" x2="2" y2="-1.1" width="0.05" layer="21"/>
<wire x1="2" y1="-1.1" x2="2" y2="1.1" width="0.05" layer="21"/>
<wire x1="2" y1="1.1" x2="-2" y2="1.1" width="0.05" layer="21"/>
<smd name="1" x="-1.35" y="0" dx="0.7" dy="1.6" layer="1"/>
<smd name="2" x="1.35" y="0" dx="0.7" dy="1.6" layer="1"/>
<text x="-2" y="2" size="1" layer="25">&gt;NAME</text>
<text x="-2" y="-3" size="1" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="TDK-5750">
<description>For non-standard TDK high value ceramic caps</description>
<smd name="1" x="-2.55" y="0" dx="1.4" dy="5.6" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.4" dy="5.6" layer="1"/>
<wire x1="-3.5" y1="3" x2="3.5" y2="3" width="0.2" layer="21"/>
<wire x1="3.5" y1="3" x2="3.5" y2="-3" width="0.2" layer="21"/>
<wire x1="3.5" y1="-3" x2="-3.5" y2="-3" width="0.2" layer="21"/>
<wire x1="-3.5" y1="-3" x2="-3.5" y2="3" width="0.2" layer="21"/>
<text x="-3.5" y="3.3" size="1" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.6" y="-4.3" size="1" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="CRYSTAL-1.5X3.2MM-2LEAD">
<wire x1="2.125" y1="1.25" x2="-2.125" y2="1.25" width="0.05" layer="101"/>
<wire x1="-2.125" y1="1.25" x2="-2.125" y2="-1.25" width="0.05" layer="101"/>
<wire x1="-2.125" y1="-1.25" x2="2.125" y2="-1.25" width="0.05" layer="101"/>
<wire x1="2.125" y1="-1.25" x2="2.125" y2="1.25" width="0.05" layer="101"/>
<wire x1="-0.2" y1="0.2" x2="-0.2" y2="-0.2" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="-0.2" x2="0.2" y2="-0.2" width="0.1524" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="0.2" y2="0.2" width="0.1524" layer="21"/>
<wire x1="0.2" y1="0.2" x2="-0.2" y2="0.2" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0.4" x2="-0.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0.4" x2="0.5" y2="0" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="1.05" x2="1.9" y2="1.05" width="0.1524" layer="21"/>
<wire x1="1.9" y1="1.05" x2="1.9" y2="-1.05" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="1.05" x2="-1.9" y2="-1.05" width="0.1524" layer="21"/>
<wire x1="-1.9" y1="-1.05" x2="1.9" y2="-1.05" width="0.1524" layer="21"/>
<smd name="2" x="1.25" y="0" dx="1" dy="1.8" layer="1"/>
<smd name="1" x="-1.25" y="0" dx="1" dy="1.8" layer="1"/>
<text x="-1.98" y="-3" size="0.8" layer="25" ratio="16">&gt;NAME</text>
<text x="-1.98" y="3" size="0.8" layer="27" ratio="16">&gt;VALUE</text>
</package>
<package name="CRYSTAL-5X7MM-4LEAD">
<wire x1="-4.625" y1="3" x2="-4.625" y2="-3" width="0.05" layer="101"/>
<wire x1="-4.625" y1="-3" x2="4.625" y2="-3" width="0.05" layer="101"/>
<wire x1="4.625" y1="-3" x2="4.625" y2="3" width="0.05" layer="101"/>
<wire x1="4.625" y1="3" x2="-4.625" y2="3" width="0.05" layer="101"/>
<wire x1="-0.2" y1="0.2" x2="-0.2" y2="-0.2" width="0.1524" layer="21"/>
<wire x1="-0.2" y1="-0.2" x2="0.2" y2="-0.2" width="0.1524" layer="21"/>
<wire x1="0.2" y1="-0.2" x2="0.2" y2="0.2" width="0.1524" layer="21"/>
<wire x1="0.2" y1="0.2" x2="-0.2" y2="0.2" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0.4" x2="-0.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0.4" x2="0.5" y2="0" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.4" width="0.1524" layer="21"/>
<wire x1="0.5" y1="0" x2="0.6" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.5" y1="0" x2="-0.6" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.6" y1="2.6" x2="3.6" y2="2.6" width="0.1524" layer="21"/>
<wire x1="-3.6" y1="-2.6" x2="3.6" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.6" y1="-2.3" x2="-3.6" y2="-2.6" width="0.1524" layer="21"/>
<wire x1="-3.6" y1="-0.3" x2="-3.6" y2="0.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-2.6" x2="3.6" y2="-2.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="-0.3" x2="3.6" y2="0.3" width="0.1524" layer="21"/>
<wire x1="3.6" y1="2.3" x2="3.6" y2="2.6" width="0.1524" layer="21"/>
<wire x1="-3.6" y1="2.6" x2="-3.6" y2="2.3" width="0.1524" layer="21"/>
<smd name="1" x="-3.15" y="-1.27" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="-3.15" y="1.27" dx="2.2" dy="1.4" layer="1"/>
<smd name="3" x="3.15" y="1.27" dx="2.2" dy="1.4" layer="1"/>
<smd name="4" x="3.05" y="-1.27" dx="2.4" dy="1.4" layer="1"/>
<text x="-5" y="3.75" size="0.8" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-5" y="-4.5" size="0.8" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="CRYSTAL-2.5X3.2MM-2LEAD">
<wire x1="-2" y1="1.6" x2="-2" y2="-1.6" width="0.2" layer="21"/>
<wire x1="-2" y1="-1.6" x2="2" y2="-1.6" width="0.2" layer="21"/>
<wire x1="2" y1="-1.6" x2="2" y2="1.6" width="0.2" layer="21"/>
<wire x1="2" y1="1.6" x2="-2" y2="1.6" width="0.2" layer="21"/>
<smd name="P$1" x="-1" y="0" dx="1.6" dy="2.6" layer="1"/>
<smd name="P$2" x="1" y="0" dx="1.6" dy="2.6" layer="1"/>
<text x="-2.2" y="2" size="0.85" layer="25" ratio="19">&gt;NAME</text>
<text x="-2.2" y="-2.8" size="0.85" layer="27" ratio="19">&gt;VALUE</text>
</package>
<package name="DEBUG">
<pad name="GND" x="0" y="6.35" drill="1" shape="square"/>
<pad name="VIN" x="0" y="3.81" drill="1"/>
<pad name="PRG" x="0" y="1.27" drill="1"/>
<pad name="RXD" x="0" y="-1.27" drill="1"/>
<pad name="TXD" x="0" y="-3.81" drill="1"/>
<pad name="RES" x="0" y="-6.35" drill="1"/>
<wire x1="-1.27" y1="7.62" x2="1.27" y2="7.62" width="0.2" layer="21"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.2" layer="21"/>
<wire x1="1.27" y1="-7.62" x2="-1.27" y2="-7.62" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-7.62" x2="-1.27" y2="7.62" width="0.2" layer="21"/>
<text x="-2.54" y="-6.35" size="1" layer="25" font="vector" ratio="18" rot="R90">&gt;NAME</text>
<text x="-2.54" y="1.27" size="1" layer="27" font="vector" ratio="18" rot="R90">&gt;VALUE</text>
<circle x="1.7" y="7.4" radius="0.111803125" width="0.2" layer="21"/>
</package>
<package name="2512">
<smd name="P$1" x="-3.175" y="0" dx="3.175" dy="1.27" layer="1" rot="R90"/>
<smd name="P$2" x="3.175" y="0" dx="3.175" dy="1.27" layer="1" rot="R90"/>
<wire x1="-4.064" y1="1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.778" x2="4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.778" x2="-4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="1.778" width="0.1524" layer="21"/>
<text x="-4.064" y="2.032" size="1" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-4.064" y="-3.048" size="1" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="1210">
<smd name="P$1" x="-1.651" y="0" dx="2.794" dy="1.651" layer="1" rot="R90"/>
<smd name="P$2" x="1.651" y="0" dx="2.794" dy="1.651" layer="1" rot="R90"/>
<wire x1="-2.667" y1="1.651" x2="2.667" y2="1.651" width="0.127" layer="21"/>
<wire x1="2.667" y1="1.651" x2="2.667" y2="-1.651" width="0.127" layer="21"/>
<wire x1="2.667" y1="-1.651" x2="-2.667" y2="-1.651" width="0.127" layer="21"/>
<wire x1="-2.667" y1="-1.651" x2="-2.667" y2="1.651" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="16">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="16">&gt;VALUE</text>
</package>
<package name="OPTICAL">
<pad name="GND" x="0" y="6.35" drill="1" shape="square"/>
<pad name="SCLK" x="0" y="3.81" drill="1"/>
<pad name="MISO" x="0" y="1.27" drill="1"/>
<pad name="MOSI" x="0" y="-1.27" drill="1"/>
<pad name="!CSEL" x="0" y="-3.81" drill="1"/>
<pad name="VDD" x="0" y="-6.35" drill="1"/>
<wire x1="-1.27" y1="7.62" x2="1.27" y2="7.62" width="0.2" layer="21"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.2" layer="21"/>
<wire x1="1.27" y1="-7.62" x2="-1.27" y2="-7.62" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-7.62" x2="-1.27" y2="7.62" width="0.2" layer="21"/>
<text x="-2.54" y="-6.35" size="1" layer="25" font="vector" ratio="18" rot="R90">&gt;NAME</text>
<text x="-2.54" y="1.27" size="1" layer="27" font="vector" ratio="18" rot="R90">&gt;VALUE</text>
<circle x="1.7" y="7.4" radius="0.111803125" width="0.2" layer="21"/>
</package>
<package name="MSE16(12)">
<description>16-pin MSE package with 4 pins removed, from LT</description>
<smd name="TAB" x="0" y="0" dx="2.9" dy="1.7" layer="1"/>
<smd name="12" x="0.25" y="2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="11" x="0.75" y="2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="10" x="1.25" y="2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="9" x="1.75" y="2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="14" x="-0.75" y="2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="16" x="-1.75" y="2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="5" x="0.25" y="-2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="6" x="0.75" y="-2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="7" x="1.25" y="-2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="8" x="1.75" y="-2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="3" x="-0.75" y="-2.5" dx="0.3" dy="0.9" layer="1"/>
<smd name="1" x="-1.75" y="-2.5" dx="0.3" dy="0.9" layer="1"/>
<wire x1="-2" y1="1.5" x2="2" y2="1.5" width="0.127" layer="21"/>
<wire x1="2" y1="1.5" x2="2" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2" y1="-1.5" x2="-2" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.5" x2="-2" y2="1.5" width="0.127" layer="21"/>
<circle x="-1.7" y="-1.2" radius="0.14141875" width="0.127" layer="21"/>
<text x="-2" y="4" size="1" layer="25" ratio="16">&gt;NAME</text>
<text x="-2.5" y="-4.5" size="1" layer="27" ratio="16">&gt;VALUE</text>
</package>
<package name="SRR1208">
<wire x1="-6.5" y1="6" x2="-6.5" y2="3.75" width="0.1524" layer="21"/>
<wire x1="-6" y1="6.5" x2="6" y2="6.5" width="0.1524" layer="21"/>
<wire x1="6.5" y1="6" x2="6.5" y2="3.75" width="0.1524" layer="21"/>
<wire x1="6.5" y1="-3.75" x2="6.5" y2="-6" width="0.1524" layer="21"/>
<wire x1="6" y1="-6.5" x2="-6" y2="-6.5" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="-6" x2="-6.5" y2="-3.75" width="0.1524" layer="21"/>
<wire x1="-6.5" y1="-6" x2="-6" y2="-6.5" width="0.1524" layer="21" curve="90"/>
<wire x1="6" y1="-6.5" x2="6.5" y2="-6" width="0.1524" layer="21" curve="90"/>
<wire x1="6" y1="6.5" x2="6.5" y2="6" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.5" y1="6" x2="-6" y2="6.5" width="0.1524" layer="21" curve="-90"/>
<wire x1="-7.5" y1="7.25" x2="7.5" y2="7.25" width="0.05" layer="101"/>
<wire x1="7.5" y1="7.25" x2="7.5" y2="-7.25" width="0.05" layer="101"/>
<wire x1="7.5" y1="-7.25" x2="-7.5" y2="-7.25" width="0.05" layer="101"/>
<wire x1="-7.5" y1="-7.25" x2="-7.5" y2="7.25" width="0.05" layer="101"/>
<smd name="P$1" x="-5" y="0" dx="7" dy="4" layer="1" rot="R90"/>
<smd name="P$2" x="5" y="0" dx="7" dy="4" layer="1" rot="R90"/>
<text x="-6" y="10" size="0.8" layer="25" ratio="16">&gt;NAME</text>
<text x="-6" y="8" size="0.8" layer="27" ratio="16">&gt;VALUE</text>
</package>
<package name="CDRH5D18BHPNP">
<wire x1="-3" y1="2.5" x2="-2.5" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="-2.5" y1="3" x2="-1.5" y2="3" width="0.127" layer="21"/>
<wire x1="1.5" y1="3" x2="2.5" y2="3" width="0.127" layer="21"/>
<wire x1="2.5" y1="3" x2="3" y2="2.5" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="2.5" x2="3" y2="-2.5" width="0.127" layer="21"/>
<wire x1="3" y1="-2.5" x2="2.5" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="2.5" y1="-3" x2="1.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-3" x2="-2.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-3" x2="-3" y2="-2.5" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="-2.5" x2="-3" y2="2.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="-3.5" width="0.127" layer="101"/>
<wire x1="-3.5" y1="-3.5" x2="3.5" y2="-3.5" width="0.127" layer="101"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="3.5" width="0.127" layer="101"/>
<wire x1="3.5" y1="3.5" x2="-3.5" y2="3.5" width="0.127" layer="101"/>
<smd name="P$1" x="0" y="2.45" dx="2.5" dy="1.7" layer="1"/>
<smd name="P$2" x="0" y="-2.45" dx="2.5" dy="1.7" layer="1"/>
<text x="-3" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-5" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="CDRH5D18NP">
<wire x1="-3" y1="-0.8" x2="-3" y2="0.8" width="0.127" layer="21"/>
<wire x1="3" y1="0.8" x2="3" y2="-0.8" width="0.127" layer="21"/>
<wire x1="-3" y1="3" x2="-3" y2="-3" width="0.127" layer="51"/>
<wire x1="-3" y1="-3" x2="3" y2="-3" width="0.127" layer="51"/>
<wire x1="3" y1="-3" x2="3" y2="3" width="0.127" layer="51"/>
<wire x1="3" y1="3" x2="-3" y2="3" width="0.127" layer="51"/>
<smd name="P$1" x="0" y="2.2" dx="6.3" dy="2.2" layer="1"/>
<smd name="P$2" x="0" y="-2.2" dx="6.3" dy="2.2" layer="1"/>
<text x="-4" y="4" size="0.8" layer="25" ratio="19">&gt;NAME</text>
<text x="-5" y="-5" size="0.8" layer="27" ratio="19">&gt;VALUE</text>
</package>
<package name="CDEP147">
<smd name="X" x="-6.5" y="0" dx="2.4" dy="2.8" layer="1" rot="R90"/>
<smd name="1" x="6.5" y="4.5" dx="3" dy="2.8" layer="1" rot="R90"/>
<smd name="2" x="6.5" y="-4.5" dx="3" dy="2.8" layer="1" rot="R90"/>
<wire x1="-8.5" y1="8.5" x2="-8.5" y2="-8.5" width="0.2" layer="21"/>
<wire x1="-8.5" y1="-8.5" x2="8.5" y2="-8.5" width="0.2" layer="21"/>
<wire x1="8.5" y1="-8.5" x2="8.5" y2="8.5" width="0.2" layer="21"/>
<wire x1="8.5" y1="8.5" x2="-8.5" y2="8.5" width="0.2" layer="21"/>
<text x="-8" y="9" size="1" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-8" y="-10" size="1" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="CDMC6D28">
<description>Sumida power inductor series</description>
<smd name="2" x="2.85" y="0" dx="1.3" dy="3.2" layer="1"/>
<smd name="1" x="-2.85" y="0" dx="1.3" dy="3.2" layer="1"/>
<wire x1="-3.5" y1="-2" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="-2" width="0.127" layer="21"/>
<wire x1="3.5" y1="2" x2="3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="-3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="2" width="0.127" layer="21"/>
<text x="-3.5" y="4" size="1" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-3.5" y="-5" size="1" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="DFEH12060D">
<description>Powdered iron inductors from Murata 1-22uH 19-5A 2.9-35mOhm</description>
<smd name="P$1" x="-5.5" y="0" dx="3" dy="5" layer="1"/>
<smd name="P$2" x="5.5" y="0" dx="3" dy="5" layer="1"/>
<wire x1="-6.5" y1="6.5" x2="-6.5" y2="3" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-3" x2="-6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-6.5" x2="6.5" y2="-6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="-6.5" x2="6.5" y2="-3" width="0.127" layer="21"/>
<wire x1="6.5" y1="3" x2="6.5" y2="6.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="6.5" x2="-6.5" y2="6.5" width="0.127" layer="21"/>
<text x="-6.5" y="8" size="0.8" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-6.5" y="7" size="0.8" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="WEPDA1210">
<description>Shielded inductors from Wurth</description>
<smd name="P$1" x="0" y="-4.9" dx="5.4" dy="3" layer="1"/>
<smd name="P$2" x="0" y="4.9" dx="5.4" dy="3" layer="1"/>
<wire x1="-3" y1="-6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-3" y2="6" width="0.127" layer="21"/>
<wire x1="3" y1="6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="3" y2="-6" width="0.127" layer="21"/>
<text x="-6" y="7" size="0.8" layer="25" font="vector" ratio="16">&gt;NAME</text>
<text x="-6" y="-8" size="0.8" layer="27" font="vector" ratio="16">&gt;VALUE</text>
</package>
<package name="MAG3110">
<description>Magnetic compass MAG3110</description>
<smd name="1" x="-0.9" y="0.8" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="2" x="-0.9" y="0.4" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="3" x="-0.9" y="0" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="4" x="-0.9" y="-0.4" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="5" x="-0.9" y="-0.8" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="6" x="0.9" y="-0.8" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="7" x="0.9" y="-0.4" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="8" x="0.9" y="0" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="9" x="0.9" y="0.4" dx="0.6" dy="0.225" layer="1" stop="no"/>
<smd name="10" x="0.9" y="0.8" dx="0.6" dy="0.225" layer="1" stop="no"/>
<wire x1="-1.5" y1="1.2" x2="-1.5" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.2" x2="1.5" y2="-1.2" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.2" x2="1.5" y2="1.2" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.2" x2="-1.5" y2="1.2" width="0.127" layer="21"/>
<circle x="-1.6" y="1.3" radius="0.1" width="0" layer="21"/>
<text x="-2.5" y="1.5" size="1" layer="25" ratio="16">&gt;NAME</text>
<text x="-2.5" y="-2.5" size="1" layer="27" ratio="16">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.1" x2="1.4" y2="1.1" layer="29"/>
</package>
<package name="SOT23-6">
<wire x1="-2.5" y1="2.5" x2="2.5" y2="2.5" width="0.05" layer="101"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="-2.5" width="0.05" layer="101"/>
<wire x1="2.5" y1="-2.5" x2="-2.5" y2="-2.5" width="0.05" layer="101"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="2.5" width="0.05" layer="101"/>
<wire x1="-2" y1="1" x2="-1.45" y2="1" width="0.1524" layer="21"/>
<wire x1="-2" y1="-1" x2="-1.45" y2="-1" width="0.1524" layer="21"/>
<wire x1="1.45" y1="1" x2="2" y2="1" width="0.1524" layer="21"/>
<wire x1="2" y1="-1" x2="1.45" y2="-1" width="0.1524" layer="21"/>
<wire x1="2" y1="1" x2="2" y2="-1" width="0.1524" layer="21"/>
<wire x1="-2" y1="1" x2="-2" y2="-1" width="0.1524" layer="21"/>
<circle x="-1.651" y="-0.635" radius="0.1135" width="0.127" layer="21"/>
<smd name="6" x="-0.95" y="1.306" dx="0.6" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.6" dy="1.2" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.6" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.6" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.6" dy="1.2" layer="1"/>
<smd name="5" x="0" y="1.306" dx="0.6" dy="1.2" layer="1"/>
<text x="-3.028" y="-2.028" size="0.8" layer="25" ratio="16" rot="R90">&gt;NAME</text>
<text x="4.048" y="-2.028" size="0.8" layer="27" ratio="16" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="0.7126" x2="0.2375" y2="1.5439" layer="51"/>
</package>
<package name="USB-B">
<wire x1="-6.1" y1="-10.3" x2="6.1" y2="-10.3" width="0.127" layer="51"/>
<wire x1="6.1" y1="-10.3" x2="6.1" y2="5.4" width="0.127" layer="51"/>
<wire x1="6.1" y1="5.4" x2="-6.1" y2="5.4" width="0.127" layer="51"/>
<wire x1="-6.1" y1="5.4" x2="-6.1" y2="-10.3" width="0.127" layer="51"/>
<wire x1="-6.5" y1="1.5" x2="-6.5" y2="6" width="0.127" layer="21"/>
<wire x1="-6.5" y1="6" x2="6.5" y2="6" width="0.127" layer="21"/>
<wire x1="6.5" y1="6" x2="6.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="6.5" y1="-1.5" x2="6.5" y2="-7" width="0.127" layer="21"/>
<wire x1="-6.5" y1="-1.5" x2="-6.5" y2="-7" width="0.127" layer="21"/>
<pad name="3" x="-1.25" y="2.71" drill="0.8"/>
<pad name="4" x="1.25" y="2.71" drill="0.8"/>
<pad name="1" x="1.25" y="4.71" drill="0.8"/>
<pad name="2" x="-1.25" y="4.71" drill="0.8"/>
<text x="-2.5" y="-3.5" size="1.27" layer="21">USB-B</text>
<text x="-6.5" y="6.5" size="1.27" layer="25">&gt;NAME</text>
<text x="0.5" y="6.5" size="1.27" layer="27">&gt;VALUE</text>
<hole x="-6.02" y="0" drill="2.3"/>
<hole x="6.02" y="0" drill="2.3"/>
</package>
<package name="A1101R09A00GR">
<smd name="P$1" x="-1" y="0" dx="1" dy="1" layer="1"/>
<smd name="P$2" x="1" y="0" dx="1" dy="1" layer="1"/>
<smd name="P$3" x="-3.9" y="3.25" dx="1" dy="1" layer="1"/>
<smd name="P$4" x="3.9" y="3.25" dx="1" dy="1" layer="1"/>
<smd name="P$5" x="-3.9" y="7.25" dx="1" dy="1" layer="1"/>
<smd name="P$6" x="3.9" y="7.25" dx="1" dy="1" layer="1"/>
<smd name="1" x="-4.1" y="1.25" dx="1.5" dy="0.61" layer="1"/>
<smd name="2" x="-4.1" y="0.25" dx="1.5" dy="0.61" layer="1"/>
<smd name="3" x="-4.1" y="-0.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="4" x="-4.1" y="-1.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="5" x="-4.1" y="-2.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="6" x="-4.1" y="-3.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="7" x="-4.1" y="-4.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="8" x="-4.1" y="-5.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="24" x="4.1" y="1.25" dx="1.5" dy="0.61" layer="1"/>
<smd name="23" x="4.1" y="0.25" dx="1.5" dy="0.61" layer="1"/>
<smd name="22" x="4.1" y="-0.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="21" x="4.1" y="-1.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="20" x="4.1" y="-2.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="19" x="4.1" y="-3.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="18" x="4.1" y="-4.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="17" x="4.1" y="-5.75" dx="1.5" dy="0.61" layer="1"/>
<smd name="12" x="-0.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="13" x="0.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="14" x="1.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="15" x="2.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="16" x="3.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="11" x="-1.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="10" x="-2.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<smd name="9" x="-3.5" y="-7.35" dx="1.5" dy="0.61" layer="1" rot="R90"/>
<wire x1="-3" y1="8" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="-3" y1="-6" x2="3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="3" y2="8" width="0.127" layer="21"/>
<wire x1="3" y1="8" x2="-3" y2="8" width="0.127" layer="21"/>
<text x="-3" y="9.5" size="1" layer="25" ratio="16">&gt;NAME</text>
<text x="2.5" y="9.5" size="1" layer="27" ratio="16">&gt;VALUE</text>
<wire x1="-4.7" y1="8.25" x2="-4.7" y2="-7.7" width="0.127" layer="51"/>
<wire x1="-4.7" y1="-7.7" x2="4.7" y2="-7.7" width="0.127" layer="51"/>
<wire x1="4.7" y1="-7.7" x2="4.7" y2="8.25" width="0.127" layer="51"/>
<wire x1="4.7" y1="8.25" x2="-4.7" y2="8.25" width="0.127" layer="51"/>
</package>
<package name="PLCC4">
<wire x1="-1.625" y1="2.75" x2="1.625" y2="2.75" width="0.1" layer="101"/>
<wire x1="1.625" y1="2.75" x2="1.625" y2="-2.75" width="0.1" layer="101"/>
<wire x1="1.625" y1="-2.75" x2="-1.625" y2="-2.75" width="0.1" layer="101"/>
<wire x1="-1.625" y1="-2.75" x2="-1.625" y2="2.75" width="0.1" layer="101"/>
<wire x1="-1.375" y1="1.5" x2="1.375" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.375" y1="1.5" x2="1.375" y2="-1.625" width="0.127" layer="51"/>
<wire x1="1.375" y1="-1.625" x2="-1.375" y2="-1.625" width="0.127" layer="51"/>
<wire x1="-1.375" y1="-1.625" x2="-1.375" y2="1.5" width="0.127" layer="51"/>
<wire x1="-1.35" y1="0.65" x2="-1.35" y2="-0.65" width="0.1524" layer="21"/>
<wire x1="-1.35" y1="-0.65" x2="1.35" y2="-0.65" width="0.1524" layer="21"/>
<wire x1="1.35" y1="-0.65" x2="1.35" y2="0.65" width="0.1524" layer="21"/>
<wire x1="1.35" y1="0.65" x2="-1.35" y2="0.65" width="0.1524" layer="21"/>
<circle x="0" y="-0.05" radius="1.2041" width="0.15" layer="51"/>
<circle x="0" y="0" radius="0.6519" width="0.1524" layer="21"/>
<smd name="1" x="-0.75" y="-1.55" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="2" x="0.75" y="-1.55" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="3" x="0.75" y="1.55" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<smd name="4" x="-0.75" y="1.55" dx="1.5" dy="0.9" layer="1" rot="R90"/>
<text x="-2" y="3" size="0.8" layer="25" ratio="16">&gt;NAME</text>
<text x="-2" y="-4" size="0.8" layer="27" ratio="16">&gt;VALUE</text>
<polygon width="0.127" layer="51">
<vertex x="-1.375" y="-1.625"/>
<vertex x="-1.375" y="-1"/>
<vertex x="-0.75" y="-1.625"/>
</polygon>
<polygon width="0.1524" layer="21">
<vertex x="-1.35" y="-0.6"/>
<vertex x="-1.35" y="0.2"/>
<vertex x="-0.55" y="-0.6"/>
</polygon>
</package>
<package name="SOLDERPOINT">
<pad name="P$1" x="0" y="0" drill="1" diameter="2"/>
</package>
</packages>
<symbols>
<symbol name="FRAME-A3-REF">
<wire x1="-381" y1="266.7" x2="0" y2="266.7" width="0.254" layer="94"/>
<wire x1="0" y1="266.7" x2="0" y2="11.43" width="0.254" layer="94"/>
<wire x1="0" y1="11.43" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-19.05" y2="0" width="0.254" layer="94"/>
<wire x1="-19.05" y1="0" x2="-34.29" y2="0" width="0.254" layer="94"/>
<wire x1="-34.29" y1="0" x2="-68.58" y2="0" width="0.254" layer="94"/>
<wire x1="-68.58" y1="0" x2="-106.68" y2="0" width="0.254" layer="94"/>
<wire x1="-106.68" y1="0" x2="-381" y2="0" width="0.254" layer="94"/>
<wire x1="-381" y1="0" x2="-381" y2="266.7" width="0.254" layer="94"/>
<wire x1="-106.172" y1="10.541" x2="-69.215" y2="10.541" width="0.127" layer="94"/>
<wire x1="-106.172" y1="10.033" x2="-69.215" y2="10.033" width="0.127" layer="94"/>
<wire x1="-106.172" y1="9.525" x2="-69.215" y2="9.525" width="0.127" layer="94"/>
<wire x1="-106.172" y1="9.017" x2="-69.215" y2="9.017" width="0.127" layer="94"/>
<wire x1="-106.172" y1="8.509" x2="-104.267" y2="8.509" width="0.127" layer="94"/>
<wire x1="-104.267" y1="8.509" x2="-103.886" y2="8.763" width="0.127" layer="94"/>
<wire x1="-103.886" y1="8.763" x2="-101.092" y2="8.763" width="0.127" layer="94"/>
<wire x1="-101.092" y1="8.763" x2="-100.711" y2="8.509" width="0.127" layer="94"/>
<wire x1="-100.711" y1="8.509" x2="-98.806" y2="8.509" width="0.127" layer="94"/>
<wire x1="-98.806" y1="8.509" x2="-98.425" y2="8.763" width="0.127" layer="94"/>
<wire x1="-98.425" y1="8.763" x2="-95.631" y2="8.763" width="0.127" layer="94"/>
<wire x1="-95.631" y1="8.763" x2="-95.25" y2="8.509" width="0.127" layer="94"/>
<wire x1="-95.25" y1="8.509" x2="-94.488" y2="8.509" width="0.127" layer="94"/>
<wire x1="-94.488" y1="8.509" x2="-94.107" y2="8.763" width="0.127" layer="94"/>
<wire x1="-94.107" y1="8.763" x2="-93.218" y2="8.763" width="0.127" layer="94"/>
<wire x1="-93.218" y1="8.763" x2="-92.837" y2="8.509" width="0.127" layer="94"/>
<wire x1="-92.837" y1="8.509" x2="-90.424" y2="8.509" width="0.127" layer="94"/>
<wire x1="-90.424" y1="8.509" x2="-90.043" y2="8.763" width="0.127" layer="94"/>
<wire x1="-90.043" y1="8.763" x2="-89.154" y2="8.763" width="0.127" layer="94"/>
<wire x1="-89.154" y1="8.763" x2="-88.773" y2="8.509" width="0.127" layer="94"/>
<wire x1="-88.773" y1="8.509" x2="-87.757" y2="8.509" width="0.127" layer="94"/>
<wire x1="-87.757" y1="8.509" x2="-87.376" y2="8.763" width="0.127" layer="94"/>
<wire x1="-87.376" y1="8.763" x2="-84.328" y2="8.763" width="0.127" layer="94"/>
<wire x1="-84.328" y1="8.763" x2="-83.947" y2="8.509" width="0.127" layer="94"/>
<wire x1="-83.947" y1="8.509" x2="-80.518" y2="8.509" width="0.127" layer="94"/>
<wire x1="-80.518" y1="8.509" x2="-80.137" y2="8.763" width="0.127" layer="94"/>
<wire x1="-80.137" y1="8.763" x2="-77.343" y2="8.763" width="0.127" layer="94"/>
<wire x1="-77.343" y1="8.763" x2="-76.962" y2="8.509" width="0.127" layer="94"/>
<wire x1="-76.962" y1="8.509" x2="-74.93" y2="8.509" width="0.127" layer="94"/>
<wire x1="-74.93" y1="8.509" x2="-74.549" y2="8.763" width="0.127" layer="94"/>
<wire x1="-74.549" y1="8.763" x2="-71.755" y2="8.763" width="0.127" layer="94"/>
<wire x1="-71.755" y1="8.763" x2="-71.374" y2="8.509" width="0.127" layer="94"/>
<wire x1="-71.374" y1="8.509" x2="-69.215" y2="8.509" width="0.127" layer="94"/>
<wire x1="-106.172" y1="8.001" x2="-104.394" y2="8.001" width="0.127" layer="94"/>
<wire x1="-104.394" y1="8.001" x2="-104.013" y2="8.255" width="0.127" layer="94"/>
<wire x1="-104.013" y1="8.255" x2="-100.584" y2="8.255" width="0.127" layer="94"/>
<wire x1="-100.584" y1="8.255" x2="-100.203" y2="8.001" width="0.127" layer="94"/>
<wire x1="-100.203" y1="8.001" x2="-99.06" y2="8.001" width="0.127" layer="94"/>
<wire x1="-99.06" y1="8.001" x2="-98.679" y2="8.255" width="0.127" layer="94"/>
<wire x1="-98.679" y1="8.255" x2="-95.504" y2="8.255" width="0.127" layer="94"/>
<wire x1="-95.504" y1="8.255" x2="-95.123" y2="8.001" width="0.127" layer="94"/>
<wire x1="-95.123" y1="8.001" x2="-94.488" y2="8.001" width="0.127" layer="94"/>
<wire x1="-94.488" y1="8.001" x2="-94.107" y2="8.255" width="0.127" layer="94"/>
<wire x1="-94.107" y1="8.255" x2="-92.837" y2="8.255" width="0.127" layer="94"/>
<wire x1="-92.837" y1="8.255" x2="-92.456" y2="8.001" width="0.127" layer="94"/>
<wire x1="-92.456" y1="8.001" x2="-90.424" y2="8.001" width="0.127" layer="94"/>
<wire x1="-90.424" y1="8.001" x2="-90.043" y2="8.255" width="0.127" layer="94"/>
<wire x1="-90.043" y1="8.255" x2="-89.027" y2="8.255" width="0.127" layer="94"/>
<wire x1="-89.027" y1="8.255" x2="-88.646" y2="8.001" width="0.127" layer="94"/>
<wire x1="-88.646" y1="8.001" x2="-87.884" y2="8.001" width="0.127" layer="94"/>
<wire x1="-87.884" y1="8.001" x2="-87.503" y2="8.255" width="0.127" layer="94"/>
<wire x1="-87.503" y1="8.255" x2="-83.566" y2="8.255" width="0.127" layer="94"/>
<wire x1="-83.566" y1="8.255" x2="-83.185" y2="8.001" width="0.127" layer="94"/>
<wire x1="-83.185" y1="8.001" x2="-81.28" y2="8.001" width="0.127" layer="94"/>
<wire x1="-81.28" y1="8.001" x2="-80.899" y2="8.255" width="0.127" layer="94"/>
<wire x1="-80.899" y1="8.255" x2="-76.708" y2="8.255" width="0.127" layer="94"/>
<wire x1="-76.708" y1="8.255" x2="-76.327" y2="8.001" width="0.127" layer="94"/>
<wire x1="-76.327" y1="8.001" x2="-75.057" y2="8.001" width="0.127" layer="94"/>
<wire x1="-75.057" y1="8.001" x2="-74.676" y2="8.255" width="0.127" layer="94"/>
<wire x1="-74.676" y1="8.255" x2="-70.993" y2="8.255" width="0.127" layer="94"/>
<wire x1="-70.993" y1="8.255" x2="-70.612" y2="8.001" width="0.127" layer="94"/>
<wire x1="-70.612" y1="8.001" x2="-69.215" y2="8.001" width="0.127" layer="94"/>
<wire x1="-106.172" y1="7.493" x2="-104.394" y2="7.493" width="0.127" layer="94"/>
<wire x1="-104.394" y1="7.493" x2="-104.013" y2="7.747" width="0.127" layer="94"/>
<wire x1="-104.013" y1="7.747" x2="-102.997" y2="7.747" width="0.127" layer="94"/>
<wire x1="-102.997" y1="7.747" x2="-102.616" y2="7.493" width="0.127" layer="94"/>
<wire x1="-102.616" y1="7.493" x2="-102.108" y2="7.493" width="0.127" layer="94"/>
<wire x1="-102.108" y1="7.493" x2="-101.727" y2="7.747" width="0.127" layer="94"/>
<wire x1="-101.727" y1="7.747" x2="-100.33" y2="7.747" width="0.127" layer="94"/>
<wire x1="-100.33" y1="7.747" x2="-99.949" y2="7.493" width="0.127" layer="94"/>
<wire x1="-99.949" y1="7.493" x2="-99.06" y2="7.493" width="0.127" layer="94"/>
<wire x1="-99.06" y1="7.493" x2="-98.679" y2="7.747" width="0.127" layer="94"/>
<wire x1="-98.679" y1="7.747" x2="-96.139" y2="7.747" width="0.127" layer="94"/>
<wire x1="-96.139" y1="7.747" x2="-95.758" y2="7.493" width="0.127" layer="94"/>
<wire x1="-95.758" y1="7.493" x2="-94.488" y2="7.493" width="0.127" layer="94"/>
<wire x1="-94.488" y1="7.493" x2="-94.107" y2="7.747" width="0.127" layer="94"/>
<wire x1="-94.107" y1="7.747" x2="-92.456" y2="7.747" width="0.127" layer="94"/>
<wire x1="-92.456" y1="7.747" x2="-92.075" y2="7.493" width="0.127" layer="94"/>
<wire x1="-92.075" y1="7.493" x2="-90.424" y2="7.493" width="0.127" layer="94"/>
<wire x1="-90.424" y1="7.493" x2="-90.043" y2="7.747" width="0.127" layer="94"/>
<wire x1="-90.043" y1="7.747" x2="-89.027" y2="7.747" width="0.127" layer="94"/>
<wire x1="-89.027" y1="7.747" x2="-88.646" y2="7.493" width="0.127" layer="94"/>
<wire x1="-88.646" y1="7.493" x2="-87.884" y2="7.493" width="0.127" layer="94"/>
<wire x1="-87.884" y1="7.493" x2="-87.503" y2="7.747" width="0.127" layer="94"/>
<wire x1="-87.503" y1="7.747" x2="-83.185" y2="7.747" width="0.127" layer="94"/>
<wire x1="-83.185" y1="7.747" x2="-82.804" y2="7.493" width="0.127" layer="94"/>
<wire x1="-82.804" y1="7.493" x2="-81.534" y2="7.493" width="0.127" layer="94"/>
<wire x1="-81.534" y1="7.493" x2="-81.153" y2="7.747" width="0.127" layer="94"/>
<wire x1="-81.153" y1="7.747" x2="-79.375" y2="7.747" width="0.127" layer="94"/>
<wire x1="-79.375" y1="7.747" x2="-78.994" y2="7.493" width="0.127" layer="94"/>
<wire x1="-78.994" y1="7.493" x2="-78.486" y2="7.493" width="0.127" layer="94"/>
<wire x1="-78.486" y1="7.493" x2="-78.105" y2="7.747" width="0.127" layer="94"/>
<wire x1="-78.105" y1="7.747" x2="-76.327" y2="7.747" width="0.127" layer="94"/>
<wire x1="-76.327" y1="7.747" x2="-75.946" y2="7.493" width="0.127" layer="94"/>
<wire x1="-75.946" y1="7.493" x2="-75.057" y2="7.493" width="0.127" layer="94"/>
<wire x1="-75.057" y1="7.493" x2="-74.676" y2="7.747" width="0.127" layer="94"/>
<wire x1="-74.676" y1="7.747" x2="-70.739" y2="7.747" width="0.127" layer="94"/>
<wire x1="-70.739" y1="7.747" x2="-70.358" y2="7.493" width="0.127" layer="94"/>
<wire x1="-70.358" y1="7.493" x2="-69.215" y2="7.493" width="0.127" layer="94"/>
<wire x1="-106.172" y1="6.985" x2="-104.394" y2="6.985" width="0.127" layer="94"/>
<wire x1="-104.394" y1="6.985" x2="-104.013" y2="7.239" width="0.127" layer="94"/>
<wire x1="-104.013" y1="7.239" x2="-102.997" y2="7.239" width="0.127" layer="94"/>
<wire x1="-102.997" y1="7.239" x2="-102.616" y2="6.985" width="0.127" layer="94"/>
<wire x1="-102.616" y1="6.985" x2="-101.727" y2="6.985" width="0.127" layer="94"/>
<wire x1="-101.727" y1="6.985" x2="-101.346" y2="7.239" width="0.127" layer="94"/>
<wire x1="-101.346" y1="7.239" x2="-100.33" y2="7.239" width="0.127" layer="94"/>
<wire x1="-100.33" y1="7.239" x2="-99.949" y2="6.985" width="0.127" layer="94"/>
<wire x1="-99.949" y1="6.985" x2="-99.06" y2="6.985" width="0.127" layer="94"/>
<wire x1="-99.06" y1="6.985" x2="-98.679" y2="7.239" width="0.127" layer="94"/>
<wire x1="-98.679" y1="7.239" x2="-97.663" y2="7.239" width="0.127" layer="94"/>
<wire x1="-97.663" y1="7.239" x2="-97.282" y2="6.985" width="0.127" layer="94"/>
<wire x1="-97.282" y1="6.985" x2="-94.488" y2="6.985" width="0.127" layer="94"/>
<wire x1="-94.488" y1="6.985" x2="-94.107" y2="7.239" width="0.127" layer="94"/>
<wire x1="-94.107" y1="7.239" x2="-91.948" y2="7.239" width="0.127" layer="94"/>
<wire x1="-91.948" y1="7.239" x2="-91.567" y2="6.985" width="0.127" layer="94"/>
<wire x1="-91.567" y1="6.985" x2="-90.424" y2="6.985" width="0.127" layer="94"/>
<wire x1="-90.424" y1="6.985" x2="-90.043" y2="7.239" width="0.127" layer="94"/>
<wire x1="-90.043" y1="7.239" x2="-89.027" y2="7.239" width="0.127" layer="94"/>
<wire x1="-89.027" y1="7.239" x2="-88.646" y2="6.985" width="0.127" layer="94"/>
<wire x1="-88.646" y1="6.985" x2="-87.884" y2="6.985" width="0.127" layer="94"/>
<wire x1="-87.884" y1="6.985" x2="-87.503" y2="7.239" width="0.127" layer="94"/>
<wire x1="-87.503" y1="7.239" x2="-86.487" y2="7.239" width="0.127" layer="94"/>
<wire x1="-86.487" y1="7.239" x2="-86.106" y2="6.985" width="0.127" layer="94"/>
<wire x1="-86.106" y1="6.985" x2="-84.582" y2="6.985" width="0.127" layer="94"/>
<wire x1="-84.582" y1="6.985" x2="-84.201" y2="7.239" width="0.127" layer="94"/>
<wire x1="-84.201" y1="7.239" x2="-82.931" y2="7.239" width="0.127" layer="94"/>
<wire x1="-82.931" y1="7.239" x2="-82.55" y2="6.985" width="0.127" layer="94"/>
<wire x1="-82.55" y1="6.985" x2="-81.915" y2="6.985" width="0.127" layer="94"/>
<wire x1="-81.915" y1="6.985" x2="-81.534" y2="7.239" width="0.127" layer="94"/>
<wire x1="-81.534" y1="7.239" x2="-80.137" y2="7.239" width="0.127" layer="94"/>
<wire x1="-80.137" y1="7.239" x2="-79.756" y2="6.985" width="0.127" layer="94"/>
<wire x1="-79.756" y1="6.985" x2="-77.851" y2="6.985" width="0.127" layer="94"/>
<wire x1="-77.851" y1="6.985" x2="-77.47" y2="7.239" width="0.127" layer="94"/>
<wire x1="-77.47" y1="7.239" x2="-76.073" y2="7.239" width="0.127" layer="94"/>
<wire x1="-76.073" y1="7.239" x2="-75.692" y2="6.985" width="0.127" layer="94"/>
<wire x1="-75.692" y1="6.985" x2="-75.057" y2="6.985" width="0.127" layer="94"/>
<wire x1="-75.057" y1="6.985" x2="-74.676" y2="7.239" width="0.127" layer="94"/>
<wire x1="-74.676" y1="7.239" x2="-73.66" y2="7.239" width="0.127" layer="94"/>
<wire x1="-73.66" y1="7.239" x2="-73.279" y2="6.985" width="0.127" layer="94"/>
<wire x1="-73.279" y1="6.985" x2="-72.263" y2="6.985" width="0.127" layer="94"/>
<wire x1="-72.263" y1="6.985" x2="-71.882" y2="7.239" width="0.127" layer="94"/>
<wire x1="-71.882" y1="7.239" x2="-70.612" y2="7.239" width="0.127" layer="94"/>
<wire x1="-70.612" y1="7.239" x2="-70.231" y2="6.985" width="0.127" layer="94"/>
<wire x1="-70.231" y1="6.985" x2="-69.215" y2="6.985" width="0.127" layer="94"/>
<wire x1="-106.172" y1="6.477" x2="-104.394" y2="6.477" width="0.127" layer="94"/>
<wire x1="-104.394" y1="6.477" x2="-104.013" y2="6.731" width="0.127" layer="94"/>
<wire x1="-104.013" y1="6.731" x2="-102.997" y2="6.731" width="0.127" layer="94"/>
<wire x1="-102.997" y1="6.731" x2="-102.616" y2="6.477" width="0.127" layer="94"/>
<wire x1="-102.616" y1="6.477" x2="-101.981" y2="6.477" width="0.127" layer="94"/>
<wire x1="-101.981" y1="6.477" x2="-101.6" y2="6.731" width="0.127" layer="94"/>
<wire x1="-101.6" y1="6.731" x2="-100.584" y2="6.731" width="0.127" layer="94"/>
<wire x1="-100.584" y1="6.731" x2="-100.203" y2="6.477" width="0.127" layer="94"/>
<wire x1="-100.203" y1="6.477" x2="-99.06" y2="6.477" width="0.127" layer="94"/>
<wire x1="-99.06" y1="6.477" x2="-98.679" y2="6.731" width="0.127" layer="94"/>
<wire x1="-98.679" y1="6.731" x2="-96.266" y2="6.731" width="0.127" layer="94"/>
<wire x1="-96.266" y1="6.731" x2="-95.885" y2="6.477" width="0.127" layer="94"/>
<wire x1="-95.885" y1="6.477" x2="-94.488" y2="6.477" width="0.127" layer="94"/>
<wire x1="-94.488" y1="6.477" x2="-94.107" y2="6.731" width="0.127" layer="94"/>
<wire x1="-94.107" y1="6.731" x2="-91.567" y2="6.731" width="0.127" layer="94"/>
<wire x1="-91.567" y1="6.731" x2="-91.186" y2="6.477" width="0.127" layer="94"/>
<wire x1="-91.186" y1="6.477" x2="-90.424" y2="6.477" width="0.127" layer="94"/>
<wire x1="-90.424" y1="6.477" x2="-90.043" y2="6.731" width="0.127" layer="94"/>
<wire x1="-90.043" y1="6.731" x2="-89.027" y2="6.731" width="0.127" layer="94"/>
<wire x1="-89.027" y1="6.731" x2="-88.646" y2="6.477" width="0.127" layer="94"/>
<wire x1="-88.646" y1="6.477" x2="-87.884" y2="6.477" width="0.127" layer="94"/>
<wire x1="-87.884" y1="6.477" x2="-87.503" y2="6.731" width="0.127" layer="94"/>
<wire x1="-87.503" y1="6.731" x2="-86.487" y2="6.731" width="0.127" layer="94"/>
<wire x1="-86.487" y1="6.731" x2="-86.106" y2="6.477" width="0.127" layer="94"/>
<wire x1="-86.106" y1="6.477" x2="-84.328" y2="6.477" width="0.127" layer="94"/>
<wire x1="-84.328" y1="6.477" x2="-83.947" y2="6.731" width="0.127" layer="94"/>
<wire x1="-83.947" y1="6.731" x2="-82.804" y2="6.731" width="0.127" layer="94"/>
<wire x1="-82.804" y1="6.731" x2="-82.423" y2="6.477" width="0.127" layer="94"/>
<wire x1="-82.423" y1="6.477" x2="-82.042" y2="6.477" width="0.127" layer="94"/>
<wire x1="-82.042" y1="6.477" x2="-81.661" y2="6.731" width="0.127" layer="94"/>
<wire x1="-81.661" y1="6.731" x2="-80.391" y2="6.731" width="0.127" layer="94"/>
<wire x1="-80.391" y1="6.731" x2="-80.01" y2="6.477" width="0.127" layer="94"/>
<wire x1="-80.01" y1="6.477" x2="-77.597" y2="6.477" width="0.127" layer="94"/>
<wire x1="-77.597" y1="6.477" x2="-77.216" y2="6.731" width="0.127" layer="94"/>
<wire x1="-77.216" y1="6.731" x2="-75.946" y2="6.731" width="0.127" layer="94"/>
<wire x1="-75.946" y1="6.731" x2="-75.565" y2="6.477" width="0.127" layer="94"/>
<wire x1="-75.565" y1="6.477" x2="-75.057" y2="6.477" width="0.127" layer="94"/>
<wire x1="-75.057" y1="6.477" x2="-74.676" y2="6.731" width="0.127" layer="94"/>
<wire x1="-74.676" y1="6.731" x2="-73.66" y2="6.731" width="0.127" layer="94"/>
<wire x1="-73.66" y1="6.731" x2="-73.279" y2="6.477" width="0.127" layer="94"/>
<wire x1="-73.279" y1="6.477" x2="-72.263" y2="6.477" width="0.127" layer="94"/>
<wire x1="-72.263" y1="6.477" x2="-71.882" y2="6.731" width="0.127" layer="94"/>
<wire x1="-71.882" y1="6.731" x2="-70.612" y2="6.731" width="0.127" layer="94"/>
<wire x1="-70.612" y1="6.731" x2="-70.231" y2="6.477" width="0.127" layer="94"/>
<wire x1="-70.231" y1="6.477" x2="-69.215" y2="6.477" width="0.127" layer="94"/>
<wire x1="-106.172" y1="5.969" x2="-104.394" y2="5.969" width="0.127" layer="94"/>
<wire x1="-104.394" y1="5.969" x2="-104.013" y2="6.223" width="0.127" layer="94"/>
<wire x1="-104.013" y1="6.223" x2="-101.092" y2="6.223" width="0.127" layer="94"/>
<wire x1="-101.092" y1="6.223" x2="-100.711" y2="5.969" width="0.127" layer="94"/>
<wire x1="-100.711" y1="5.969" x2="-99.06" y2="5.969" width="0.127" layer="94"/>
<wire x1="-99.06" y1="5.969" x2="-98.679" y2="6.223" width="0.127" layer="94"/>
<wire x1="-98.679" y1="6.223" x2="-95.631" y2="6.223" width="0.127" layer="94"/>
<wire x1="-95.631" y1="6.223" x2="-95.25" y2="5.969" width="0.127" layer="94"/>
<wire x1="-95.25" y1="5.969" x2="-94.488" y2="5.969" width="0.127" layer="94"/>
<wire x1="-94.488" y1="5.969" x2="-94.107" y2="6.223" width="0.127" layer="94"/>
<wire x1="-94.107" y1="6.223" x2="-93.091" y2="6.223" width="0.127" layer="94"/>
<wire x1="-93.091" y1="6.223" x2="-92.837" y2="6.096" width="0.127" layer="94"/>
<wire x1="-92.837" y1="6.096" x2="-92.583" y2="6.096" width="0.127" layer="94"/>
<wire x1="-92.583" y1="6.096" x2="-92.329" y2="6.223" width="0.127" layer="94"/>
<wire x1="-92.329" y1="6.223" x2="-91.186" y2="6.223" width="0.127" layer="94"/>
<wire x1="-91.186" y1="6.223" x2="-90.805" y2="5.969" width="0.127" layer="94"/>
<wire x1="-90.805" y1="5.969" x2="-90.424" y2="5.969" width="0.127" layer="94"/>
<wire x1="-90.424" y1="5.969" x2="-90.043" y2="6.223" width="0.127" layer="94"/>
<wire x1="-90.043" y1="6.223" x2="-89.027" y2="6.223" width="0.127" layer="94"/>
<wire x1="-89.027" y1="6.223" x2="-88.646" y2="5.969" width="0.127" layer="94"/>
<wire x1="-88.646" y1="5.969" x2="-87.884" y2="5.969" width="0.127" layer="94"/>
<wire x1="-87.884" y1="5.969" x2="-87.503" y2="6.223" width="0.127" layer="94"/>
<wire x1="-87.503" y1="6.223" x2="-86.487" y2="6.223" width="0.127" layer="94"/>
<wire x1="-86.487" y1="6.223" x2="-86.106" y2="5.969" width="0.127" layer="94"/>
<wire x1="-86.106" y1="5.969" x2="-84.201" y2="5.969" width="0.127" layer="94"/>
<wire x1="-84.201" y1="5.969" x2="-83.82" y2="6.223" width="0.127" layer="94"/>
<wire x1="-83.82" y1="6.223" x2="-82.677" y2="6.223" width="0.127" layer="94"/>
<wire x1="-82.677" y1="6.223" x2="-82.296" y2="5.969" width="0.127" layer="94"/>
<wire x1="-82.296" y1="5.969" x2="-82.042" y2="5.969" width="0.127" layer="94"/>
<wire x1="-82.042" y1="5.969" x2="-81.661" y2="6.223" width="0.127" layer="94"/>
<wire x1="-81.661" y1="6.223" x2="-80.518" y2="6.223" width="0.127" layer="94"/>
<wire x1="-80.518" y1="6.223" x2="-80.137" y2="5.969" width="0.127" layer="94"/>
<wire x1="-80.137" y1="5.969" x2="-77.47" y2="5.969" width="0.127" layer="94"/>
<wire x1="-77.47" y1="5.969" x2="-77.089" y2="6.223" width="0.127" layer="94"/>
<wire x1="-77.089" y1="6.223" x2="-75.819" y2="6.223" width="0.127" layer="94"/>
<wire x1="-75.819" y1="6.223" x2="-75.438" y2="5.969" width="0.127" layer="94"/>
<wire x1="-75.438" y1="5.969" x2="-75.057" y2="5.969" width="0.127" layer="94"/>
<wire x1="-75.057" y1="5.969" x2="-74.676" y2="6.223" width="0.127" layer="94"/>
<wire x1="-74.676" y1="6.223" x2="-73.66" y2="6.223" width="0.127" layer="94"/>
<wire x1="-73.66" y1="6.223" x2="-73.279" y2="5.969" width="0.127" layer="94"/>
<wire x1="-73.279" y1="5.969" x2="-73.025" y2="5.969" width="0.127" layer="94"/>
<wire x1="-73.025" y1="5.969" x2="-72.644" y2="6.223" width="0.127" layer="94"/>
<wire x1="-72.644" y1="6.223" x2="-70.866" y2="6.223" width="0.127" layer="94"/>
<wire x1="-70.866" y1="6.223" x2="-70.485" y2="5.969" width="0.127" layer="94"/>
<wire x1="-70.485" y1="5.969" x2="-69.215" y2="5.969" width="0.127" layer="94"/>
<wire x1="-106.172" y1="5.461" x2="-104.394" y2="5.461" width="0.127" layer="94"/>
<wire x1="-104.394" y1="5.461" x2="-104.013" y2="5.715" width="0.127" layer="94"/>
<wire x1="-104.013" y1="5.715" x2="-100.203" y2="5.715" width="0.127" layer="94"/>
<wire x1="-100.203" y1="5.715" x2="-99.822" y2="5.461" width="0.127" layer="94"/>
<wire x1="-99.822" y1="5.461" x2="-99.06" y2="5.461" width="0.127" layer="94"/>
<wire x1="-99.06" y1="5.461" x2="-98.679" y2="5.715" width="0.127" layer="94"/>
<wire x1="-98.679" y1="5.715" x2="-95.758" y2="5.715" width="0.127" layer="94"/>
<wire x1="-95.758" y1="5.715" x2="-95.377" y2="5.461" width="0.127" layer="94"/>
<wire x1="-95.377" y1="5.461" x2="-94.488" y2="5.461" width="0.127" layer="94"/>
<wire x1="-94.488" y1="5.461" x2="-94.107" y2="5.715" width="0.127" layer="94"/>
<wire x1="-94.107" y1="5.715" x2="-93.091" y2="5.715" width="0.127" layer="94"/>
<wire x1="-93.091" y1="5.715" x2="-92.71" y2="5.461" width="0.127" layer="94"/>
<wire x1="-92.71" y1="5.461" x2="-92.456" y2="5.461" width="0.127" layer="94"/>
<wire x1="-92.456" y1="5.461" x2="-92.075" y2="5.715" width="0.127" layer="94"/>
<wire x1="-92.075" y1="5.715" x2="-90.932" y2="5.715" width="0.127" layer="94"/>
<wire x1="-90.932" y1="5.715" x2="-90.551" y2="5.461" width="0.127" layer="94"/>
<wire x1="-90.551" y1="5.461" x2="-90.424" y2="5.461" width="0.127" layer="94"/>
<wire x1="-90.424" y1="5.461" x2="-90.043" y2="5.715" width="0.127" layer="94"/>
<wire x1="-90.043" y1="5.715" x2="-89.027" y2="5.715" width="0.127" layer="94"/>
<wire x1="-89.027" y1="5.715" x2="-88.646" y2="5.461" width="0.127" layer="94"/>
<wire x1="-88.646" y1="5.461" x2="-87.884" y2="5.461" width="0.127" layer="94"/>
<wire x1="-87.884" y1="5.461" x2="-87.503" y2="5.715" width="0.127" layer="94"/>
<wire x1="-87.503" y1="5.715" x2="-86.487" y2="5.715" width="0.127" layer="94"/>
<wire x1="-86.487" y1="5.715" x2="-86.106" y2="5.461" width="0.127" layer="94"/>
<wire x1="-86.106" y1="5.461" x2="-84.201" y2="5.461" width="0.127" layer="94"/>
<wire x1="-84.201" y1="5.461" x2="-83.82" y2="5.715" width="0.127" layer="94"/>
<wire x1="-83.82" y1="5.715" x2="-82.677" y2="5.715" width="0.127" layer="94"/>
<wire x1="-82.677" y1="5.715" x2="-82.296" y2="5.461" width="0.127" layer="94"/>
<wire x1="-82.296" y1="5.461" x2="-82.042" y2="5.461" width="0.127" layer="94"/>
<wire x1="-82.042" y1="5.461" x2="-81.661" y2="5.715" width="0.127" layer="94"/>
<wire x1="-81.661" y1="5.715" x2="-80.518" y2="5.715" width="0.127" layer="94"/>
<wire x1="-80.518" y1="5.715" x2="-80.137" y2="5.461" width="0.127" layer="94"/>
<wire x1="-80.137" y1="5.461" x2="-77.47" y2="5.461" width="0.127" layer="94"/>
<wire x1="-77.47" y1="5.461" x2="-77.089" y2="5.715" width="0.127" layer="94"/>
<wire x1="-77.089" y1="5.715" x2="-75.819" y2="5.715" width="0.127" layer="94"/>
<wire x1="-75.819" y1="5.715" x2="-75.438" y2="5.461" width="0.127" layer="94"/>
<wire x1="-75.438" y1="5.461" x2="-75.057" y2="5.461" width="0.127" layer="94"/>
<wire x1="-75.057" y1="5.461" x2="-74.676" y2="5.715" width="0.127" layer="94"/>
<wire x1="-74.676" y1="5.715" x2="-71.374" y2="5.715" width="0.127" layer="94"/>
<wire x1="-71.374" y1="5.715" x2="-70.993" y2="5.461" width="0.127" layer="94"/>
<wire x1="-70.993" y1="5.461" x2="-69.215" y2="5.461" width="0.127" layer="94"/>
<wire x1="-106.172" y1="4.953" x2="-104.394" y2="4.953" width="0.127" layer="94"/>
<wire x1="-104.394" y1="4.953" x2="-104.013" y2="5.207" width="0.127" layer="94"/>
<wire x1="-104.013" y1="5.207" x2="-102.997" y2="5.207" width="0.127" layer="94"/>
<wire x1="-102.997" y1="5.207" x2="-102.616" y2="4.953" width="0.127" layer="94"/>
<wire x1="-102.616" y1="4.953" x2="-101.6" y2="4.953" width="0.127" layer="94"/>
<wire x1="-101.6" y1="4.953" x2="-101.219" y2="5.207" width="0.127" layer="94"/>
<wire x1="-101.219" y1="5.207" x2="-99.949" y2="5.207" width="0.127" layer="94"/>
<wire x1="-99.949" y1="5.207" x2="-99.568" y2="4.953" width="0.127" layer="94"/>
<wire x1="-99.568" y1="4.953" x2="-99.06" y2="4.953" width="0.127" layer="94"/>
<wire x1="-99.06" y1="4.953" x2="-98.679" y2="5.207" width="0.127" layer="94"/>
<wire x1="-98.679" y1="5.207" x2="-97.663" y2="5.207" width="0.127" layer="94"/>
<wire x1="-97.663" y1="5.207" x2="-97.282" y2="4.953" width="0.127" layer="94"/>
<wire x1="-97.282" y1="4.953" x2="-94.488" y2="4.953" width="0.127" layer="94"/>
<wire x1="-94.488" y1="4.953" x2="-94.107" y2="5.207" width="0.127" layer="94"/>
<wire x1="-94.107" y1="5.207" x2="-93.091" y2="5.207" width="0.127" layer="94"/>
<wire x1="-93.091" y1="5.207" x2="-92.71" y2="4.953" width="0.127" layer="94"/>
<wire x1="-92.71" y1="4.953" x2="-91.948" y2="4.953" width="0.127" layer="94"/>
<wire x1="-91.948" y1="4.953" x2="-91.567" y2="5.207" width="0.127" layer="94"/>
<wire x1="-91.567" y1="5.207" x2="-89.027" y2="5.207" width="0.127" layer="94"/>
<wire x1="-89.027" y1="5.207" x2="-88.646" y2="4.953" width="0.127" layer="94"/>
<wire x1="-88.646" y1="4.953" x2="-87.884" y2="4.953" width="0.127" layer="94"/>
<wire x1="-87.884" y1="4.953" x2="-87.503" y2="5.207" width="0.127" layer="94"/>
<wire x1="-87.503" y1="5.207" x2="-86.487" y2="5.207" width="0.127" layer="94"/>
<wire x1="-86.487" y1="5.207" x2="-86.106" y2="4.953" width="0.127" layer="94"/>
<wire x1="-86.106" y1="4.953" x2="-84.328" y2="4.953" width="0.127" layer="94"/>
<wire x1="-84.328" y1="4.953" x2="-83.947" y2="5.207" width="0.127" layer="94"/>
<wire x1="-83.947" y1="5.207" x2="-82.804" y2="5.207" width="0.127" layer="94"/>
<wire x1="-82.804" y1="5.207" x2="-82.423" y2="4.953" width="0.127" layer="94"/>
<wire x1="-82.423" y1="4.953" x2="-82.042" y2="4.953" width="0.127" layer="94"/>
<wire x1="-82.042" y1="4.953" x2="-81.661" y2="5.207" width="0.127" layer="94"/>
<wire x1="-81.661" y1="5.207" x2="-80.391" y2="5.207" width="0.127" layer="94"/>
<wire x1="-80.391" y1="5.207" x2="-80.01" y2="4.953" width="0.127" layer="94"/>
<wire x1="-80.01" y1="4.953" x2="-77.47" y2="4.953" width="0.127" layer="94"/>
<wire x1="-77.47" y1="4.953" x2="-77.089" y2="5.207" width="0.127" layer="94"/>
<wire x1="-77.089" y1="5.207" x2="-75.946" y2="5.207" width="0.127" layer="94"/>
<wire x1="-75.946" y1="5.207" x2="-75.565" y2="4.953" width="0.127" layer="94"/>
<wire x1="-75.565" y1="4.953" x2="-75.057" y2="4.953" width="0.127" layer="94"/>
<wire x1="-75.057" y1="4.953" x2="-74.676" y2="5.207" width="0.127" layer="94"/>
<wire x1="-74.676" y1="5.207" x2="-71.882" y2="5.207" width="0.127" layer="94"/>
<wire x1="-71.882" y1="5.207" x2="-71.501" y2="4.953" width="0.127" layer="94"/>
<wire x1="-71.501" y1="4.953" x2="-69.215" y2="4.953" width="0.127" layer="94"/>
<wire x1="-106.172" y1="4.445" x2="-104.394" y2="4.445" width="0.127" layer="94"/>
<wire x1="-104.394" y1="4.445" x2="-104.013" y2="4.699" width="0.127" layer="94"/>
<wire x1="-104.013" y1="4.699" x2="-102.997" y2="4.699" width="0.127" layer="94"/>
<wire x1="-102.997" y1="4.699" x2="-102.616" y2="4.445" width="0.127" layer="94"/>
<wire x1="-102.616" y1="4.445" x2="-101.346" y2="4.445" width="0.127" layer="94"/>
<wire x1="-101.346" y1="4.445" x2="-100.965" y2="4.699" width="0.127" layer="94"/>
<wire x1="-100.965" y1="4.699" x2="-99.822" y2="4.699" width="0.127" layer="94"/>
<wire x1="-99.822" y1="4.699" x2="-99.441" y2="4.445" width="0.127" layer="94"/>
<wire x1="-99.441" y1="4.445" x2="-99.06" y2="4.445" width="0.127" layer="94"/>
<wire x1="-99.06" y1="4.445" x2="-98.679" y2="4.699" width="0.127" layer="94"/>
<wire x1="-98.679" y1="4.699" x2="-97.663" y2="4.699" width="0.127" layer="94"/>
<wire x1="-97.663" y1="4.699" x2="-97.282" y2="4.445" width="0.127" layer="94"/>
<wire x1="-97.282" y1="4.445" x2="-94.488" y2="4.445" width="0.127" layer="94"/>
<wire x1="-94.488" y1="4.445" x2="-94.107" y2="4.699" width="0.127" layer="94"/>
<wire x1="-94.107" y1="4.699" x2="-93.091" y2="4.699" width="0.127" layer="94"/>
<wire x1="-93.091" y1="4.699" x2="-92.71" y2="4.445" width="0.127" layer="94"/>
<wire x1="-92.71" y1="4.445" x2="-91.694" y2="4.445" width="0.127" layer="94"/>
<wire x1="-91.694" y1="4.445" x2="-91.313" y2="4.699" width="0.127" layer="94"/>
<wire x1="-91.313" y1="4.699" x2="-89.027" y2="4.699" width="0.127" layer="94"/>
<wire x1="-89.027" y1="4.699" x2="-88.646" y2="4.445" width="0.127" layer="94"/>
<wire x1="-88.646" y1="4.445" x2="-87.884" y2="4.445" width="0.127" layer="94"/>
<wire x1="-87.884" y1="4.445" x2="-87.503" y2="4.699" width="0.127" layer="94"/>
<wire x1="-87.503" y1="4.699" x2="-86.487" y2="4.699" width="0.127" layer="94"/>
<wire x1="-86.487" y1="4.699" x2="-86.106" y2="4.445" width="0.127" layer="94"/>
<wire x1="-86.106" y1="4.445" x2="-84.582" y2="4.445" width="0.127" layer="94"/>
<wire x1="-84.582" y1="4.445" x2="-84.201" y2="4.699" width="0.127" layer="94"/>
<wire x1="-84.201" y1="4.699" x2="-82.931" y2="4.699" width="0.127" layer="94"/>
<wire x1="-82.931" y1="4.699" x2="-82.55" y2="4.445" width="0.127" layer="94"/>
<wire x1="-82.55" y1="4.445" x2="-81.915" y2="4.445" width="0.127" layer="94"/>
<wire x1="-81.915" y1="4.445" x2="-81.534" y2="4.699" width="0.127" layer="94"/>
<wire x1="-81.534" y1="4.699" x2="-80.137" y2="4.699" width="0.127" layer="94"/>
<wire x1="-80.137" y1="4.699" x2="-79.756" y2="4.445" width="0.127" layer="94"/>
<wire x1="-79.756" y1="4.445" x2="-77.724" y2="4.445" width="0.127" layer="94"/>
<wire x1="-77.724" y1="4.445" x2="-77.343" y2="4.699" width="0.127" layer="94"/>
<wire x1="-77.343" y1="4.699" x2="-76.073" y2="4.699" width="0.127" layer="94"/>
<wire x1="-76.073" y1="4.699" x2="-75.692" y2="4.445" width="0.127" layer="94"/>
<wire x1="-75.692" y1="4.445" x2="-75.057" y2="4.445" width="0.127" layer="94"/>
<wire x1="-75.057" y1="4.445" x2="-74.676" y2="4.699" width="0.127" layer="94"/>
<wire x1="-74.676" y1="4.699" x2="-73.66" y2="4.699" width="0.127" layer="94"/>
<wire x1="-73.66" y1="4.699" x2="-73.279" y2="4.445" width="0.127" layer="94"/>
<wire x1="-73.279" y1="4.445" x2="-73.152" y2="4.445" width="0.127" layer="94"/>
<wire x1="-73.152" y1="4.445" x2="-72.771" y2="4.699" width="0.127" layer="94"/>
<wire x1="-72.771" y1="4.699" x2="-71.501" y2="4.699" width="0.127" layer="94"/>
<wire x1="-71.501" y1="4.699" x2="-71.12" y2="4.445" width="0.127" layer="94"/>
<wire x1="-71.12" y1="4.445" x2="-69.215" y2="4.445" width="0.127" layer="94"/>
<wire x1="-106.172" y1="3.937" x2="-104.394" y2="3.937" width="0.127" layer="94"/>
<wire x1="-104.394" y1="3.937" x2="-104.013" y2="4.191" width="0.127" layer="94"/>
<wire x1="-104.013" y1="4.191" x2="-102.997" y2="4.191" width="0.127" layer="94"/>
<wire x1="-102.997" y1="4.191" x2="-102.616" y2="3.937" width="0.127" layer="94"/>
<wire x1="-102.616" y1="3.937" x2="-101.727" y2="3.937" width="0.127" layer="94"/>
<wire x1="-101.727" y1="3.937" x2="-101.346" y2="4.191" width="0.127" layer="94"/>
<wire x1="-101.346" y1="4.191" x2="-99.949" y2="4.191" width="0.127" layer="94"/>
<wire x1="-99.949" y1="4.191" x2="-99.568" y2="3.937" width="0.127" layer="94"/>
<wire x1="-99.568" y1="3.937" x2="-99.06" y2="3.937" width="0.127" layer="94"/>
<wire x1="-99.06" y1="3.937" x2="-98.679" y2="4.191" width="0.127" layer="94"/>
<wire x1="-98.679" y1="4.191" x2="-97.663" y2="4.191" width="0.127" layer="94"/>
<wire x1="-97.663" y1="4.191" x2="-97.282" y2="3.937" width="0.127" layer="94"/>
<wire x1="-97.282" y1="3.937" x2="-94.488" y2="3.937" width="0.127" layer="94"/>
<wire x1="-94.488" y1="3.937" x2="-94.107" y2="4.191" width="0.127" layer="94"/>
<wire x1="-94.107" y1="4.191" x2="-93.091" y2="4.191" width="0.127" layer="94"/>
<wire x1="-93.091" y1="4.191" x2="-92.71" y2="3.937" width="0.127" layer="94"/>
<wire x1="-92.71" y1="3.937" x2="-91.313" y2="3.937" width="0.127" layer="94"/>
<wire x1="-91.313" y1="3.937" x2="-90.932" y2="4.191" width="0.127" layer="94"/>
<wire x1="-90.932" y1="4.191" x2="-89.027" y2="4.191" width="0.127" layer="94"/>
<wire x1="-89.027" y1="4.191" x2="-88.646" y2="3.937" width="0.127" layer="94"/>
<wire x1="-88.646" y1="3.937" x2="-87.884" y2="3.937" width="0.127" layer="94"/>
<wire x1="-87.884" y1="3.937" x2="-87.503" y2="4.191" width="0.127" layer="94"/>
<wire x1="-87.503" y1="4.191" x2="-86.487" y2="4.191" width="0.127" layer="94"/>
<wire x1="-86.487" y1="4.191" x2="-86.106" y2="3.937" width="0.127" layer="94"/>
<wire x1="-86.106" y1="3.937" x2="-85.344" y2="3.937" width="0.127" layer="94"/>
<wire x1="-85.344" y1="3.937" x2="-84.963" y2="4.191" width="0.127" layer="94"/>
<wire x1="-84.963" y1="4.191" x2="-83.058" y2="4.191" width="0.127" layer="94"/>
<wire x1="-83.058" y1="4.191" x2="-82.677" y2="3.937" width="0.127" layer="94"/>
<wire x1="-82.677" y1="3.937" x2="-81.661" y2="3.937" width="0.127" layer="94"/>
<wire x1="-81.661" y1="3.937" x2="-81.28" y2="4.191" width="0.127" layer="94"/>
<wire x1="-81.28" y1="4.191" x2="-79.629" y2="4.191" width="0.127" layer="94"/>
<wire x1="-79.629" y1="4.191" x2="-79.248" y2="3.937" width="0.127" layer="94"/>
<wire x1="-79.248" y1="3.937" x2="-78.232" y2="3.937" width="0.127" layer="94"/>
<wire x1="-78.232" y1="3.937" x2="-77.851" y2="4.191" width="0.127" layer="94"/>
<wire x1="-77.851" y1="4.191" x2="-76.2" y2="4.191" width="0.127" layer="94"/>
<wire x1="-76.2" y1="4.191" x2="-75.819" y2="3.937" width="0.127" layer="94"/>
<wire x1="-75.819" y1="3.937" x2="-75.057" y2="3.937" width="0.127" layer="94"/>
<wire x1="-75.057" y1="3.937" x2="-74.676" y2="4.191" width="0.127" layer="94"/>
<wire x1="-74.676" y1="4.191" x2="-73.66" y2="4.191" width="0.127" layer="94"/>
<wire x1="-73.66" y1="4.191" x2="-73.279" y2="3.937" width="0.127" layer="94"/>
<wire x1="-73.279" y1="3.937" x2="-72.771" y2="3.937" width="0.127" layer="94"/>
<wire x1="-72.771" y1="3.937" x2="-72.39" y2="4.191" width="0.127" layer="94"/>
<wire x1="-72.39" y1="4.191" x2="-70.993" y2="4.191" width="0.127" layer="94"/>
<wire x1="-70.993" y1="4.191" x2="-70.612" y2="3.937" width="0.127" layer="94"/>
<wire x1="-70.612" y1="3.937" x2="-69.215" y2="3.937" width="0.127" layer="94"/>
<wire x1="-106.172" y1="3.429" x2="-104.394" y2="3.429" width="0.127" layer="94"/>
<wire x1="-104.394" y1="3.429" x2="-104.013" y2="3.683" width="0.127" layer="94"/>
<wire x1="-104.013" y1="3.683" x2="-100.203" y2="3.683" width="0.127" layer="94"/>
<wire x1="-100.203" y1="3.683" x2="-99.822" y2="3.429" width="0.127" layer="94"/>
<wire x1="-99.822" y1="3.429" x2="-99.06" y2="3.429" width="0.127" layer="94"/>
<wire x1="-99.06" y1="3.429" x2="-98.679" y2="3.683" width="0.127" layer="94"/>
<wire x1="-98.679" y1="3.683" x2="-95.377" y2="3.683" width="0.127" layer="94"/>
<wire x1="-95.377" y1="3.683" x2="-94.996" y2="3.429" width="0.127" layer="94"/>
<wire x1="-94.996" y1="3.429" x2="-94.488" y2="3.429" width="0.127" layer="94"/>
<wire x1="-94.488" y1="3.429" x2="-94.107" y2="3.683" width="0.127" layer="94"/>
<wire x1="-94.107" y1="3.683" x2="-93.091" y2="3.683" width="0.127" layer="94"/>
<wire x1="-93.091" y1="3.683" x2="-92.71" y2="3.429" width="0.127" layer="94"/>
<wire x1="-92.71" y1="3.429" x2="-90.932" y2="3.429" width="0.127" layer="94"/>
<wire x1="-90.932" y1="3.429" x2="-90.551" y2="3.683" width="0.127" layer="94"/>
<wire x1="-90.551" y1="3.683" x2="-89.027" y2="3.683" width="0.127" layer="94"/>
<wire x1="-89.027" y1="3.683" x2="-88.646" y2="3.429" width="0.127" layer="94"/>
<wire x1="-88.646" y1="3.429" x2="-87.884" y2="3.429" width="0.127" layer="94"/>
<wire x1="-87.884" y1="3.429" x2="-87.503" y2="3.683" width="0.127" layer="94"/>
<wire x1="-87.503" y1="3.683" x2="-83.439" y2="3.683" width="0.127" layer="94"/>
<wire x1="-83.439" y1="3.683" x2="-83.058" y2="3.429" width="0.127" layer="94"/>
<wire x1="-83.058" y1="3.429" x2="-81.28" y2="3.429" width="0.127" layer="94"/>
<wire x1="-81.28" y1="3.429" x2="-80.899" y2="3.683" width="0.127" layer="94"/>
<wire x1="-80.899" y1="3.683" x2="-76.581" y2="3.683" width="0.127" layer="94"/>
<wire x1="-76.581" y1="3.683" x2="-76.2" y2="3.429" width="0.127" layer="94"/>
<wire x1="-76.2" y1="3.429" x2="-75.057" y2="3.429" width="0.127" layer="94"/>
<wire x1="-75.057" y1="3.429" x2="-74.676" y2="3.683" width="0.127" layer="94"/>
<wire x1="-74.676" y1="3.683" x2="-73.66" y2="3.683" width="0.127" layer="94"/>
<wire x1="-73.66" y1="3.683" x2="-73.279" y2="3.429" width="0.127" layer="94"/>
<wire x1="-73.279" y1="3.429" x2="-72.39" y2="3.429" width="0.127" layer="94"/>
<wire x1="-72.39" y1="3.429" x2="-72.009" y2="3.683" width="0.127" layer="94"/>
<wire x1="-72.009" y1="3.683" x2="-70.612" y2="3.683" width="0.127" layer="94"/>
<wire x1="-70.612" y1="3.683" x2="-70.231" y2="3.429" width="0.127" layer="94"/>
<wire x1="-70.231" y1="3.429" x2="-69.215" y2="3.429" width="0.127" layer="94"/>
<wire x1="-106.172" y1="2.921" x2="-104.394" y2="2.921" width="0.127" layer="94"/>
<wire x1="-104.394" y1="2.921" x2="-104.013" y2="3.175" width="0.127" layer="94"/>
<wire x1="-104.013" y1="3.175" x2="-100.711" y2="3.175" width="0.127" layer="94"/>
<wire x1="-100.711" y1="3.175" x2="-100.33" y2="2.921" width="0.127" layer="94"/>
<wire x1="-100.33" y1="2.921" x2="-98.933" y2="2.921" width="0.127" layer="94"/>
<wire x1="-98.933" y1="2.921" x2="-98.552" y2="3.175" width="0.127" layer="94"/>
<wire x1="-98.552" y1="3.175" x2="-95.504" y2="3.175" width="0.127" layer="94"/>
<wire x1="-95.504" y1="3.175" x2="-95.123" y2="2.921" width="0.127" layer="94"/>
<wire x1="-95.123" y1="2.921" x2="-94.488" y2="2.921" width="0.127" layer="94"/>
<wire x1="-94.488" y1="2.921" x2="-94.107" y2="3.175" width="0.127" layer="94"/>
<wire x1="-94.107" y1="3.175" x2="-93.218" y2="3.175" width="0.127" layer="94"/>
<wire x1="-93.218" y1="3.175" x2="-92.837" y2="2.921" width="0.127" layer="94"/>
<wire x1="-92.837" y1="2.921" x2="-90.424" y2="2.921" width="0.127" layer="94"/>
<wire x1="-90.424" y1="2.921" x2="-90.043" y2="3.175" width="0.127" layer="94"/>
<wire x1="-90.043" y1="3.175" x2="-89.154" y2="3.175" width="0.127" layer="94"/>
<wire x1="-89.154" y1="3.175" x2="-88.773" y2="2.921" width="0.127" layer="94"/>
<wire x1="-88.773" y1="2.921" x2="-87.884" y2="2.921" width="0.127" layer="94"/>
<wire x1="-87.884" y1="2.921" x2="-87.503" y2="3.175" width="0.127" layer="94"/>
<wire x1="-87.503" y1="3.175" x2="-84.201" y2="3.175" width="0.127" layer="94"/>
<wire x1="-84.201" y1="3.175" x2="-83.82" y2="2.921" width="0.127" layer="94"/>
<wire x1="-83.82" y1="2.921" x2="-80.772" y2="2.921" width="0.127" layer="94"/>
<wire x1="-80.772" y1="2.921" x2="-80.391" y2="3.175" width="0.127" layer="94"/>
<wire x1="-80.391" y1="3.175" x2="-77.216" y2="3.175" width="0.127" layer="94"/>
<wire x1="-77.216" y1="3.175" x2="-76.835" y2="2.921" width="0.127" layer="94"/>
<wire x1="-76.835" y1="2.921" x2="-75.057" y2="2.921" width="0.127" layer="94"/>
<wire x1="-75.057" y1="2.921" x2="-74.676" y2="3.175" width="0.127" layer="94"/>
<wire x1="-74.676" y1="3.175" x2="-73.787" y2="3.175" width="0.127" layer="94"/>
<wire x1="-73.787" y1="3.175" x2="-73.406" y2="2.921" width="0.127" layer="94"/>
<wire x1="-73.406" y1="2.921" x2="-72.009" y2="2.921" width="0.127" layer="94"/>
<wire x1="-72.009" y1="2.921" x2="-71.628" y2="3.175" width="0.127" layer="94"/>
<wire x1="-71.628" y1="3.175" x2="-70.612" y2="3.175" width="0.127" layer="94"/>
<wire x1="-70.612" y1="3.175" x2="-70.231" y2="2.921" width="0.127" layer="94"/>
<wire x1="-70.231" y1="2.921" x2="-69.215" y2="2.921" width="0.127" layer="94"/>
<wire x1="-106.172" y1="2.413" x2="-69.215" y2="2.413" width="0.127" layer="94"/>
<wire x1="-106.172" y1="1.905" x2="-69.215" y2="1.905" width="0.127" layer="94"/>
<wire x1="-106.172" y1="1.397" x2="-69.215" y2="1.397" width="0.127" layer="94"/>
<wire x1="-106.172" y1="0.889" x2="-69.215" y2="0.889" width="0.127" layer="94"/>
<wire x1="0" y1="11.43" x2="-68.58" y2="11.43" width="0.254" layer="94"/>
<wire x1="-68.58" y1="11.43" x2="-106.68" y2="11.43" width="0.254" layer="94"/>
<wire x1="-106.68" y1="0" x2="-106.68" y2="11.43" width="0.254" layer="94"/>
<wire x1="-68.58" y1="11.43" x2="-68.58" y2="7.62" width="0.254" layer="94"/>
<wire x1="-68.58" y1="7.62" x2="-68.58" y2="3.81" width="0.254" layer="94"/>
<wire x1="-68.58" y1="3.81" x2="-68.58" y2="0" width="0.254" layer="94"/>
<wire x1="-68.58" y1="3.81" x2="-19.05" y2="3.81" width="0.254" layer="94"/>
<wire x1="-19.05" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-68.58" y1="7.62" x2="-34.29" y2="7.62" width="0.254" layer="94"/>
<wire x1="-34.29" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="-34.29" y1="7.62" x2="-34.29" y2="0" width="0.254" layer="94"/>
<wire x1="-19.05" y1="3.81" x2="-19.05" y2="0" width="0.254" layer="94"/>
<text x="-33.02" y="5.08" size="1.778" layer="94">&gt;LAST_DATE_TIME</text>
<text x="-8.89" y="1.27" size="1.778" layer="94">&gt;SHEET</text>
<text x="-67.31" y="8.89" size="1.778" layer="94">Bendor Research Pty. Ltd.</text>
<text x="-67.31" y="5.08" size="1.778" layer="94">&gt;DRAWING_NAME</text>
<text x="-18.034" y="1.27" size="1.778" layer="94">Sheet:</text>
<text x="-33.02" y="1.27" size="1.778" layer="94">Rev:</text>
<text x="-19.05" y="8.89" size="1.27" layer="94">www.bendor.com.au</text>
<frame x1="-381" y1="0" x2="0" y2="266.7" columns="8" rows="5" layer="94" border-right="no" border-bottom="no"/>
<text x="-26.67" y="1.27" size="1.778" layer="94">&gt;REVISION</text>
</symbol>
<symbol name="STM32L15XCXXXA">
<description>STM32L151xxCxxx microcontroller in a 48-pin LQFP</description>
<pin name="VLCD" x="-22.86" y="25.4" length="middle"/>
<pin name="PC13/RTC/WKUP2" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PC14/OSC32_IN" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PC15/OSC32_OUT" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PH0/OSC_IN" x="-22.86" y="20.32" length="middle"/>
<pin name="PH1/OSC_OUT" x="-22.86" y="17.78" length="middle"/>
<pin name="!RESET!" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="VSSA" x="-22.86" y="-33.02" length="middle"/>
<pin name="VDDA" x="-22.86" y="33.02" length="middle"/>
<pin name="PA0/WKUP1" x="-22.86" y="12.7" length="middle"/>
<pin name="PA1" x="-22.86" y="10.16" length="middle"/>
<pin name="PA2" x="-22.86" y="7.62" length="middle"/>
<pin name="PA3" x="-22.86" y="5.08" length="middle"/>
<pin name="PA4" x="-22.86" y="2.54" length="middle"/>
<pin name="PA5" x="-22.86" y="0" length="middle"/>
<pin name="PA6" x="-22.86" y="-2.54" length="middle"/>
<pin name="PA7" x="-22.86" y="-5.08" length="middle"/>
<pin name="PB0" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="PB1" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="PB2/BOOT1" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="PB10" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PB11" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="VSS@1" x="-22.86" y="-35.56" length="middle"/>
<pin name="VDD@1" x="-22.86" y="35.56" length="middle"/>
<pin name="PB12" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PB13" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PB14" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PB15" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PA8" x="-22.86" y="-10.16" length="middle"/>
<pin name="PA9" x="-22.86" y="-12.7" length="middle"/>
<pin name="PA10" x="-22.86" y="-15.24" length="middle"/>
<pin name="PA11/USB-" x="-22.86" y="-17.78" length="middle"/>
<pin name="PA12/USB+" x="-22.86" y="-20.32" length="middle"/>
<pin name="PA13" x="-22.86" y="-22.86" length="middle"/>
<pin name="VSS@2" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="VDD@2" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="PA14" x="-22.86" y="-25.4" length="middle"/>
<pin name="PA15" x="-22.86" y="-27.94" length="middle"/>
<pin name="PB3" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PB4" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PB5" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PB6" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PB7" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="BOOT0" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PB8" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PB9" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="VSS@3" x="22.86" y="-35.56" length="middle" rot="R180"/>
<pin name="VDD@3" x="22.86" y="35.56" length="middle" rot="R180"/>
<wire x1="-17.78" y1="38.1" x2="-17.78" y2="-38.1" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-38.1" x2="17.78" y2="-38.1" width="0.254" layer="94"/>
<wire x1="17.78" y1="-38.1" x2="17.78" y2="38.1" width="0.254" layer="94"/>
<wire x1="17.78" y1="38.1" x2="-17.78" y2="38.1" width="0.254" layer="94"/>
<text x="-17.78" y="43.18" size="1.778" layer="95" ratio="16">&gt;NAME</text>
<text x="-17.78" y="40.64" size="1.778" layer="96" ratio="16">&gt;VALUE</text>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.6096" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.016" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="off" length="point" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3.3V">
<wire x1="-1.27" y1="2.54" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-2.54" y="5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="1.016" y="1.397" size="1.27" layer="95" font="vector">&gt;NAME</text>
<text x="1.016" y="-2.667" size="1.27" layer="96" font="vector">&gt;VALUE</text>
<rectangle x1="-1.524" y1="0.381" x2="1.524" y2="0.889" layer="94"/>
<rectangle x1="-1.524" y1="-0.889" x2="1.524" y2="-0.381" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="point" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="CRYSTAL">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.8128" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="-2.54" width="0.8128" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.508" y1="1.27" x2="1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="0.508" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0.508" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="-1.27" y2="-0.508" width="0" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
<pin name="P$1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="DEBUG">
<pin name="RXD" x="-5.08" y="0" length="middle"/>
<pin name="TXD" x="-5.08" y="-2.54" length="middle"/>
<pin name="VIN" x="-5.08" y="5.08" length="middle"/>
<pin name="GND" x="-5.08" y="7.62" length="middle"/>
<pin name="RES" x="-5.08" y="-5.08" length="middle"/>
<pin name="PRG" x="-5.08" y="2.54" length="middle"/>
<wire x1="0" y1="10.16" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94"/>
<text x="0" y="12.7" size="1.27" layer="95" ratio="16">&gt;NAME</text>
<text x="0" y="-10.16" size="1.27" layer="96" ratio="16">&gt;VALUE</text>
</symbol>
<symbol name="R">
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-3.175" x2="0" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="-3.175" x2="0.635" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0.635" y1="-3.175" x2="0.635" y2="0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="0" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="0.635" x2="-0.635" y2="0.635" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.175" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<text x="1.27" y="-0.635" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.175" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="point" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="point" rot="R270"/>
</symbol>
<symbol name="OPTICAL">
<pin name="VDD" x="-10.16" y="5.08" length="middle"/>
<pin name="SCLK" x="-10.16" y="2.54" length="middle"/>
<pin name="MOSI" x="-10.16" y="0" length="middle"/>
<pin name="MISO" x="-10.16" y="-2.54" length="middle"/>
<pin name="!CSEL" x="-10.16" y="-5.08" length="middle"/>
<pin name="GND" x="-10.16" y="-7.62" length="middle"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="-12.7" size="1.27" layer="96" ratio="16">&gt;VALUE</text>
<text x="-5.08" y="10.16" size="1.27" layer="95" ratio="16">&gt;NAME</text>
</symbol>
<symbol name="LTC3630A">
<pin name="VIN" x="-15.24" y="15.24" length="middle" direction="pas"/>
<pin name="RUN" x="-15.24" y="5.08" length="middle" direction="pas"/>
<pin name="FBO" x="-15.24" y="-10.16" length="middle" direction="pas"/>
<pin name="VPRG1" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="VPRG2" x="12.7" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="SS" x="12.7" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="FB" x="12.7" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="ISET" x="-15.24" y="-2.54" length="middle" direction="pas"/>
<pin name="SW" x="12.7" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="GND@2" x="-15.24" y="-15.24" length="middle" direction="pas"/>
<pin name="GND@1" x="-15.24" y="-17.78" length="middle" direction="pas"/>
<pin name="GND@3" x="12.7" y="-17.78" length="middle" direction="pas" rot="R180"/>
<pin name="GND@4" x="12.7" y="-15.24" length="middle" direction="pas" rot="R180"/>
<wire x1="-10.16" y1="17.78" x2="-10.16" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="7.62" y2="-20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="-20.32" x2="7.62" y2="17.78" width="0.254" layer="94"/>
<wire x1="7.62" y1="17.78" x2="-10.16" y2="17.78" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="1.27" layer="95" ratio="16">&gt;NAME</text>
<text x="-10.16" y="18.542" size="1.27" layer="96" ratio="16">&gt;VALUE</text>
</symbol>
<symbol name="L">
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.4064" layer="94" style="shortdash"/>
<text x="-5.08" y="3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="P$1" x="-7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="P$2" x="7.62" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MAG3110">
<description>MAG3110 Symbol</description>
<pin name="CAP-A" x="-15.24" y="5.08" length="middle" direction="pas"/>
<pin name="VDD" x="-15.24" y="2.54" length="middle" direction="pas"/>
<pin name="NC" x="-15.24" y="0" length="middle" direction="pas"/>
<pin name="CAP-R" x="-15.24" y="-2.54" length="middle" direction="pas"/>
<pin name="GND@1" x="-15.24" y="-5.08" length="middle" direction="pas"/>
<pin name="SDA" x="15.24" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="SCL" x="15.24" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="VDDIO" x="15.24" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="INT1" x="15.24" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GND@2" x="15.24" y="5.08" length="middle" direction="pas" rot="R180"/>
<wire x1="-10" y1="6.5" x2="-10" y2="-7" width="0.254" layer="94"/>
<wire x1="-10" y1="-7" x2="10" y2="-7" width="0.254" layer="94"/>
<wire x1="10" y1="-7" x2="10" y2="6.5" width="0.254" layer="94"/>
<wire x1="10" y1="6.5" x2="-10" y2="6.5" width="0.254" layer="94"/>
<text x="-9.66" y="-9.2" size="1.778" layer="96">&gt;VALUE</text>
<text x="-9.66" y="7.16" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="STF202">
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="10.16" y2="-2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="12.7" size="1.27" layer="96">&gt;VALUE</text>
<pin name="D+BUS" x="-12.7" y="2.54" length="middle" direction="pas"/>
<pin name="D-BUS" x="-12.7" y="5.08" length="middle" direction="pas"/>
<pin name="D+" x="15.24" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="D-" x="15.24" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="-12.7" y="0" length="middle" direction="pas"/>
<pin name="VBUS" x="-12.7" y="7.62" length="middle" direction="pas"/>
</symbol>
<symbol name="USB-A">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="-5.08" y="10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="7.62" size="1.27" layer="96">&gt;VALUE</text>
<pin name="VBUS" x="10.16" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="D-" x="10.16" y="0" length="middle" direction="pas" rot="R180"/>
<pin name="D+" x="10.16" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="10.16" y="-5.08" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="A1101R09A00GR">
<pin name="GND@1" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="GND@2" x="12.7" y="-7.62" length="middle" direction="pas" rot="R180"/>
<pin name="VCP1" x="12.7" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="VDD" x="12.7" y="7.62" length="middle" direction="pas" rot="R180"/>
<pin name="SCLK" x="-15.24" y="7.62" length="middle" direction="pas"/>
<pin name="MOSI" x="-15.24" y="5.08" length="middle" direction="pas"/>
<pin name="MISO" x="-15.24" y="2.54" length="middle" direction="pas"/>
<pin name="!CSEL" x="-15.24" y="0" length="middle" direction="pas"/>
<pin name="VCP2" x="12.7" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="GDO0" x="-15.24" y="-7.62" length="middle" direction="pas"/>
<pin name="GDO2" x="-15.24" y="-10.16" length="middle" direction="pas"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="7.62" y2="-12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="10.922" size="1.778" layer="96" ratio="16">&gt;VALUE</text>
<text x="-10.16" y="13.462" size="1.778" layer="95" ratio="16">&gt;NAME</text>
</symbol>
<symbol name="LED-RGB">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-6.096" y1="-0.762" x2="-4.064" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="94"/>
<wire x1="-1.016" y1="-0.762" x2="1.016" y2="-0.762" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.127" layer="94"/>
<wire x1="4.064" y1="-0.762" x2="6.096" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.127" layer="94"/>
<circle x="0" y="2.54" radius="0.254" width="0.127" layer="94"/>
<text x="-3.81" y="-2.286" size="1.27" layer="94">R</text>
<text x="1.27" y="-2.286" size="1.27" layer="94">G</text>
<text x="6.35" y="-2.286" size="1.27" layer="94">B</text>
<text x="2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="5.08" size="1.27" layer="96">&gt;VALUE</text>
<pin name="A" x="0" y="5.08" visible="pad" length="short" direction="out" rot="R270"/>
<pin name="R" x="-5.08" y="-5.08" visible="pad" length="short" direction="out" rot="R90"/>
<pin name="G" x="0" y="-5.08" visible="pad" length="short" direction="out" rot="R90"/>
<pin name="B" x="5.08" y="-5.08" visible="pad" length="short" direction="out" rot="R90"/>
<polygon width="0.254" layer="94">
<vertex x="-5.08" y="-0.508"/>
<vertex x="-4.064" y="0.762"/>
<vertex x="-6.096" y="0.762"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="0" y="-0.508"/>
<vertex x="1.016" y="0.762"/>
<vertex x="-1.016" y="0.762"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="5.08" y="-0.508"/>
<vertex x="6.096" y="0.762"/>
<vertex x="4.064" y="0.762"/>
</polygon>
<polygon width="0.127" layer="94">
<vertex x="0" y="2.286"/>
<vertex x="-0.254" y="2.54"/>
<vertex x="0" y="2.794"/>
<vertex x="0.254" y="2.54"/>
</polygon>
</symbol>
<symbol name="SOLDERPOINT">
<pin name="P$1" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<circle x="0.508" y="0" radius="0.359209375" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A3-REF">
<description>A3 drawing frame with grid references. Uses Bendor logo.</description>
<gates>
<gate name="G$1" symbol="FRAME-A3-REF" x="-238.76" y="167.64"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STM32L15XCXXXA" prefix="U">
<description>STM32L151/152xCxxxA uC in 48-pin LQFP</description>
<gates>
<gate name="G$1" symbol="STM32L15XCXXXA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LQFP48">
<connects>
<connect gate="G$1" pin="!RESET!" pad="7"/>
<connect gate="G$1" pin="BOOT0" pad="44"/>
<connect gate="G$1" pin="PA0/WKUP1" pad="10"/>
<connect gate="G$1" pin="PA1" pad="11"/>
<connect gate="G$1" pin="PA10" pad="31"/>
<connect gate="G$1" pin="PA11/USB-" pad="32"/>
<connect gate="G$1" pin="PA12/USB+" pad="33"/>
<connect gate="G$1" pin="PA13" pad="34"/>
<connect gate="G$1" pin="PA14" pad="37"/>
<connect gate="G$1" pin="PA15" pad="38"/>
<connect gate="G$1" pin="PA2" pad="12"/>
<connect gate="G$1" pin="PA3" pad="13"/>
<connect gate="G$1" pin="PA4" pad="14"/>
<connect gate="G$1" pin="PA5" pad="15"/>
<connect gate="G$1" pin="PA6" pad="16"/>
<connect gate="G$1" pin="PA7" pad="17"/>
<connect gate="G$1" pin="PA8" pad="29"/>
<connect gate="G$1" pin="PA9" pad="30"/>
<connect gate="G$1" pin="PB0" pad="18"/>
<connect gate="G$1" pin="PB1" pad="19"/>
<connect gate="G$1" pin="PB10" pad="21"/>
<connect gate="G$1" pin="PB11" pad="22"/>
<connect gate="G$1" pin="PB12" pad="25"/>
<connect gate="G$1" pin="PB13" pad="26"/>
<connect gate="G$1" pin="PB14" pad="27"/>
<connect gate="G$1" pin="PB15" pad="28"/>
<connect gate="G$1" pin="PB2/BOOT1" pad="20"/>
<connect gate="G$1" pin="PB3" pad="39"/>
<connect gate="G$1" pin="PB4" pad="40"/>
<connect gate="G$1" pin="PB5" pad="41"/>
<connect gate="G$1" pin="PB6" pad="42"/>
<connect gate="G$1" pin="PB7" pad="43"/>
<connect gate="G$1" pin="PB8" pad="45"/>
<connect gate="G$1" pin="PB9" pad="46"/>
<connect gate="G$1" pin="PC13/RTC/WKUP2" pad="2"/>
<connect gate="G$1" pin="PC14/OSC32_IN" pad="3"/>
<connect gate="G$1" pin="PC15/OSC32_OUT" pad="4"/>
<connect gate="G$1" pin="PH0/OSC_IN" pad="5"/>
<connect gate="G$1" pin="PH1/OSC_OUT" pad="6"/>
<connect gate="G$1" pin="VDD@1" pad="24"/>
<connect gate="G$1" pin="VDD@2" pad="36"/>
<connect gate="G$1" pin="VDD@3" pad="48"/>
<connect gate="G$1" pin="VDDA" pad="9"/>
<connect gate="G$1" pin="VLCD" pad="1"/>
<connect gate="G$1" pin="VSS@1" pad="23"/>
<connect gate="G$1" pin="VSS@2" pad="35"/>
<connect gate="G$1" pin="VSS@3" pad="47"/>
<connect gate="G$1" pin="VSSA" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUP">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3.3V" prefix="SUP">
<gates>
<gate name="G$1" symbol="+3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="X" package="C-5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH5" package="C5B2,5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH2,5" package="C-2,5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH7,5" package="C7,5B4">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH5-3,5" package="C5B3,5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TH5-5" package="C5B5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CKG57" package="TDK-5750">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="TYPE" value="CKG57"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<gates>
<gate name="G$1" symbol="CRYSTAL" x="0" y="0"/>
</gates>
<devices>
<device name="SMD-1.5X3.2MM-2LEAD" package="CRYSTAL-1.5X3.2MM-2LEAD">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-5.0X7.0MM-4LEAD" package="CRYSTAL-5X7MM-4LEAD">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMD-2.5X3.2MM-2LEAD" package="CRYSTAL-2.5X3.2MM-2LEAD">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DEBUG" prefix="J">
<gates>
<gate name="G$1" symbol="DEBUG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DEBUG">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="PRG" pad="PRG"/>
<connect gate="G$1" pin="RES" pad="RES"/>
<connect gate="G$1" pin="RXD" pad="RXD"/>
<connect gate="G$1" pin="TXD" pad="TXD"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2512" package="2512">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OPTICAL" prefix="J">
<gates>
<gate name="G$1" symbol="OPTICAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OPTICAL">
<connects>
<connect gate="G$1" pin="!CSEL" pad="!CSEL"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="SCLK" pad="SCLK"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC3630A" prefix="U">
<gates>
<gate name="G$1" symbol="LTC3630A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSE16(12)">
<connects>
<connect gate="G$1" pin="FB" pad="9"/>
<connect gate="G$1" pin="FBO" pad="12"/>
<connect gate="G$1" pin="GND@1" pad="8"/>
<connect gate="G$1" pin="GND@2" pad="14"/>
<connect gate="G$1" pin="GND@3" pad="16"/>
<connect gate="G$1" pin="GND@4" pad="TAB"/>
<connect gate="G$1" pin="ISET" pad="11"/>
<connect gate="G$1" pin="RUN" pad="5"/>
<connect gate="G$1" pin="SS" pad="10"/>
<connect gate="G$1" pin="SW" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VPRG1" pad="7"/>
<connect gate="G$1" pin="VPRG2" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<description>Power inductor</description>
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="SRR1208" package="SRR1208">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CDRH5D18BHPNP" package="CDRH5D18BHPNP">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CDRH5D18NP" package="CDRH5D18NP">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CDEP147" package="CDEP147">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CDMC6D28" package="CDMC6D28">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DFEH12060D" package="DFEH12060D">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WEPDA1210" package="WEPDA1210">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MAG3110" prefix="U">
<description>Magnetic compass 3-axis I2C I/F</description>
<gates>
<gate name="G$1" symbol="MAG3110" x="-17.78" y="-2.54"/>
</gates>
<devices>
<device name="" package="MAG3110">
<connects>
<connect gate="G$1" pin="CAP-A" pad="1"/>
<connect gate="G$1" pin="CAP-R" pad="4"/>
<connect gate="G$1" pin="GND@1" pad="5"/>
<connect gate="G$1" pin="GND@2" pad="10"/>
<connect gate="G$1" pin="INT1" pad="9"/>
<connect gate="G$1" pin="NC" pad="3"/>
<connect gate="G$1" pin="SCL" pad="7"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="VDD" pad="2"/>
<connect gate="G$1" pin="VDDIO" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="STF202" prefix="U">
<description>USB filter</description>
<gates>
<gate name="G$1" symbol="STF202" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="SOT23-6">
<connects>
<connect gate="G$1" pin="D+" pad="4"/>
<connect gate="G$1" pin="D+BUS" pad="3"/>
<connect gate="G$1" pin="D-" pad="5"/>
<connect gate="G$1" pin="D-BUS" pad="2"/>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB-B" prefix="J">
<gates>
<gate name="G$1" symbol="USB-A" x="20.32" y="-10.16"/>
</gates>
<devices>
<device name="" package="USB-B">
<connects>
<connect gate="G$1" pin="D+" pad="3"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="VBUS" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="A1101R09A00GR" prefix="U">
<description>A CC1101 based RF module, with integrated antenna from Anaren, Inc.</description>
<gates>
<gate name="G$1" symbol="A1101R09A00GR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="A1101R09A00GR">
<connects>
<connect gate="G$1" pin="!CSEL" pad="12"/>
<connect gate="G$1" pin="GDO0" pad="13"/>
<connect gate="G$1" pin="GDO2" pad="14"/>
<connect gate="G$1" pin="GND@1" pad="8"/>
<connect gate="G$1" pin="GND@2" pad="17"/>
<connect gate="G$1" pin="MISO" pad="10"/>
<connect gate="G$1" pin="MOSI" pad="11"/>
<connect gate="G$1" pin="SCLK" pad="9"/>
<connect gate="G$1" pin="VCP1" pad="7"/>
<connect gate="G$1" pin="VCP2" pad="15"/>
<connect gate="G$1" pin="VDD" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CLV1A-FKB" prefix="D">
<gates>
<gate name="G$1" symbol="LED-RGB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PLCC4">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="B" pad="3"/>
<connect gate="G$1" pin="G" pad="4"/>
<connect gate="G$1" pin="R" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDERPOINT" prefix="J" uservalue="yes">
<description>A 1mm through hole solder point</description>
<gates>
<gate name="G$1" symbol="SOLDERPOINT" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="SOLDERPOINT">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$1" library="near-ir" deviceset="FRAME-A3-REF" device=""/>
<part name="U1" library="near-ir" deviceset="STM32L15XCXXXA" device=""/>
<part name="SUP1" library="near-ir" deviceset="GND" device=""/>
<part name="SUP2" library="near-ir" deviceset="GND" device=""/>
<part name="SUP3" library="near-ir" deviceset="+3.3V" device=""/>
<part name="SUP4" library="near-ir" deviceset="+3.3V" device=""/>
<part name="C1" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C2" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C3" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C4" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="SUP5" library="near-ir" deviceset="GND" device=""/>
<part name="SUP6" library="near-ir" deviceset="GND" device=""/>
<part name="SUP7" library="near-ir" deviceset="GND" device=""/>
<part name="SUP8" library="near-ir" deviceset="GND" device=""/>
<part name="Q1" library="near-ir" deviceset="CRYSTAL" device="SMD-2.5X3.2MM-2LEAD" value="8MHz"/>
<part name="C5" library="near-ir" deviceset="C" device="0805" value="10pF"/>
<part name="C6" library="near-ir" deviceset="C" device="0805" value="10pF"/>
<part name="SUP9" library="near-ir" deviceset="GND" device=""/>
<part name="SUP10" library="near-ir" deviceset="GND" device=""/>
<part name="J1" library="near-ir" deviceset="DEBUG" device=""/>
<part name="SUP11" library="near-ir" deviceset="GND" device=""/>
<part name="R1" library="near-ir" deviceset="R" device="" value="10k"/>
<part name="SUP12" library="near-ir" deviceset="+3.3V" device=""/>
<part name="R2" library="near-ir" deviceset="R" device="" value="10k"/>
<part name="J2" library="near-ir" deviceset="OPTICAL" device=""/>
<part name="J3" library="near-ir" deviceset="OPTICAL" device=""/>
<part name="SUP13" library="near-ir" deviceset="GND" device=""/>
<part name="SUP14" library="near-ir" deviceset="+3.3V" device=""/>
<part name="U5" library="near-ir" deviceset="LTC3630A" device=""/>
<part name="L1" library="near-ir" deviceset="L" device="CDRH5D18NP" value="10uH"/>
<part name="C7" library="near-ir" deviceset="C" device="0805" value="47u/6.3V"/>
<part name="SUP15" library="near-ir" deviceset="GND" device=""/>
<part name="SUP16" library="near-ir" deviceset="GND" device=""/>
<part name="C9" library="near-ir" deviceset="C" device="0805" value="10u/16V"/>
<part name="U2" library="near-ir" deviceset="MAG3110" device=""/>
<part name="SUP17" library="near-ir" deviceset="GND" device=""/>
<part name="C8" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C10" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C11" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C12" library="near-ir" deviceset="C" device="0805" value="1u"/>
<part name="C13" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="SUP18" library="near-ir" deviceset="GND" device=""/>
<part name="SUP20" library="near-ir" deviceset="GND" device=""/>
<part name="SUP21" library="near-ir" deviceset="GND" device=""/>
<part name="SUP22" library="near-ir" deviceset="GND" device=""/>
<part name="SUP23" library="near-ir" deviceset="GND" device=""/>
<part name="SUP24" library="near-ir" deviceset="GND" device=""/>
<part name="SUP19" library="near-ir" deviceset="+3.3V" device=""/>
<part name="SUP25" library="near-ir" deviceset="+3.3V" device=""/>
<part name="R5" library="near-ir" deviceset="R" device="" value="4.7k"/>
<part name="R6" library="near-ir" deviceset="R" device="" value="4.7k"/>
<part name="SUP26" library="near-ir" deviceset="+3.3V" device=""/>
<part name="U3" library="near-ir" deviceset="STF202" device=""/>
<part name="J4" library="near-ir" deviceset="USB-B" device=""/>
<part name="SUP27" library="near-ir" deviceset="+3.3V" device=""/>
<part name="U4" library="near-ir" deviceset="A1101R09A00GR" device=""/>
<part name="SUP28" library="near-ir" deviceset="GND" device=""/>
<part name="C14" library="near-ir" deviceset="C" device="0805" value="1u"/>
<part name="SUP30" library="near-ir" deviceset="GND" device=""/>
<part name="SUP29" library="near-ir" deviceset="+3.3V" device=""/>
<part name="D1" library="near-ir" deviceset="CLV1A-FKB" device=""/>
<part name="R7" library="near-ir" deviceset="R" device="" value="150R"/>
<part name="R8" library="near-ir" deviceset="R" device="" value="150R"/>
<part name="R9" library="near-ir" deviceset="R" device="" value="110R"/>
<part name="C15" library="near-ir" deviceset="C" device="0805" value="1u"/>
<part name="SUP31" library="near-ir" deviceset="+3.3V" device=""/>
<part name="SUP32" library="near-ir" deviceset="GND" device=""/>
<part name="C16" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="C17" library="near-ir" deviceset="C" device="0805" value="100n"/>
<part name="SUP33" library="near-ir" deviceset="GND" device=""/>
<part name="SUP34" library="near-ir" deviceset="GND" device=""/>
<part name="J5" library="near-ir" deviceset="SOLDERPOINT" device=""/>
<part name="J6" library="near-ir" deviceset="SOLDERPOINT" device=""/>
<part name="SUP35" library="near-ir" deviceset="GND" device=""/>
<part name="SUP36" library="near-ir" deviceset="GND" device=""/>
<part name="R3" library="near-ir" deviceset="R" device="" value="33R"/>
<part name="R4" library="near-ir" deviceset="R" device="" value="33R"/>
<part name="R10" library="near-ir" deviceset="R" device="" value="33R"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="132.08" y="147.32" size="1.27" layer="91">10-14V supply</text>
<text x="139.7" y="151.638" size="1.27" layer="91">+</text>
<text x="139.7" y="149.098" size="1.27" layer="91">-</text>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="266.7" y="-76.2"/>
<instance part="U1" gate="G$1" x="83.82" y="60.96"/>
<instance part="SUP1" gate="G$1" x="58.42" y="22.86"/>
<instance part="SUP2" gate="G$1" x="109.22" y="22.86"/>
<instance part="SUP3" gate="G$1" x="58.42" y="101.6"/>
<instance part="SUP4" gate="G$1" x="109.22" y="101.6"/>
<instance part="C1" gate="G$1" x="116.84" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="115.316" y="102.997" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="115.316" y="101.473" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C2" gate="G$1" x="116.84" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="115.316" y="90.297" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="115.316" y="88.773" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C3" gate="G$1" x="50.8" y="99.06" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="102.997" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="49.276" y="101.473" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C4" gate="G$1" x="50.8" y="93.98" smashed="yes" rot="R90">
<attribute name="NAME" x="49.276" y="90.297" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="49.276" y="88.773" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP5" gate="G$1" x="124.46" y="99.06" rot="R90"/>
<instance part="SUP6" gate="G$1" x="124.46" y="93.98" rot="R90"/>
<instance part="SUP7" gate="G$1" x="45.72" y="99.06" rot="R270"/>
<instance part="SUP8" gate="G$1" x="45.72" y="93.98" rot="R270"/>
<instance part="Q1" gate="G$1" x="45.72" y="78.74" rot="R90"/>
<instance part="C5" gate="G$1" x="35.56" y="86.36" rot="R90"/>
<instance part="C6" gate="G$1" x="35.56" y="71.12" rot="R90"/>
<instance part="SUP9" gate="G$1" x="30.48" y="86.36" rot="R270"/>
<instance part="SUP10" gate="G$1" x="30.48" y="71.12" rot="R270"/>
<instance part="J1" gate="G$1" x="177.8" y="63.5"/>
<instance part="SUP11" gate="G$1" x="170.18" y="73.66" rot="R180"/>
<instance part="R1" gate="G$1" x="129.54" y="88.9"/>
<instance part="SUP12" gate="G$1" x="129.54" y="93.98"/>
<instance part="R2" gate="G$1" x="121.92" y="88.9"/>
<instance part="J2" gate="G$1" x="22.86" y="-15.24"/>
<instance part="J3" gate="G$1" x="-30.48" y="-15.24" rot="MR0"/>
<instance part="SUP13" gate="G$1" x="5.08" y="-25.4"/>
<instance part="SUP14" gate="G$1" x="-15.24" y="-2.54"/>
<instance part="U2" gate="G$1" x="162.56" y="-5.08"/>
<instance part="SUP17" gate="G$1" x="142.24" y="-17.78"/>
<instance part="C8" gate="G$1" x="134.62" y="-12.7" smashed="yes">
<attribute name="NAME" x="128.27" y="-12.065" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="128.27" y="-14.097" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C10" gate="G$1" x="142.24" y="5.08" smashed="yes" rot="R180">
<attribute name="NAME" x="144.78" y="5.969" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="144.78" y="4.191" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C11" gate="G$1" x="129.54" y="5.08" smashed="yes">
<attribute name="NAME" x="122.936" y="5.969" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="122.936" y="3.937" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C12" gate="G$1" x="134.62" y="5.08" smashed="yes">
<attribute name="NAME" x="135.636" y="1.905" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="135.636" y="0.381" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C13" gate="G$1" x="190.5" y="0" smashed="yes">
<attribute name="NAME" x="193.04" y="0.889" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="193.04" y="-1.143" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP18" gate="G$1" x="142.24" y="10.16" rot="R180"/>
<instance part="SUP20" gate="G$1" x="134.62" y="-17.78"/>
<instance part="SUP21" gate="G$1" x="129.54" y="10.16" rot="R180"/>
<instance part="SUP22" gate="G$1" x="134.62" y="10.16" rot="R180"/>
<instance part="SUP23" gate="G$1" x="190.5" y="5.08" rot="R180"/>
<instance part="SUP24" gate="G$1" x="180.34" y="5.08" rot="R180"/>
<instance part="SUP19" gate="G$1" x="127" y="-2.54" smashed="yes" rot="R90">
<attribute name="VALUE" x="122.682" y="-2.032" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUP25" gate="G$1" x="193.04" y="-5.08" smashed="yes" rot="R270">
<attribute name="VALUE" x="197.612" y="-5.842" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="182.88" y="-17.78" smashed="yes">
<attribute name="NAME" x="184.658" y="-18.161" size="1.27" layer="95"/>
<attribute name="VALUE" x="184.404" y="-19.939" size="1.27" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="180.34" y="-17.78" smashed="yes">
<attribute name="NAME" x="175.26" y="-18.161" size="1.27" layer="95"/>
<attribute name="VALUE" x="175.26" y="-19.939" size="1.27" layer="96"/>
</instance>
<instance part="SUP26" gate="G$1" x="187.96" y="-25.4" smashed="yes" rot="R270">
<attribute name="VALUE" x="192.532" y="-26.162" size="1.778" layer="96"/>
</instance>
<instance part="U3" gate="G$1" x="30.48" y="38.1"/>
<instance part="J4" gate="G$1" x="0" y="43.18"/>
<instance part="U4" gate="G$1" x="-27.94" y="116.84"/>
<instance part="SUP28" gate="G$1" x="-12.7" y="101.6"/>
<instance part="C14" gate="G$1" x="12.7" y="119.38" smashed="yes">
<attribute name="NAME" x="15.24" y="120.015" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="15.24" y="118.237" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP30" gate="G$1" x="12.7" y="114.3"/>
<instance part="SUP29" gate="G$1" x="17.78" y="124.46" rot="R270"/>
<instance part="D1" gate="G$1" x="7.62" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="5.08" y="71.12" size="1.27" layer="95"/>
<attribute name="VALUE" x="5.08" y="68.58" size="1.27" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="20.32" y="55.88" smashed="yes" rot="R90">
<attribute name="NAME" x="20.955" y="57.15" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.495" y="57.15" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R8" gate="G$1" x="20.32" y="60.96" smashed="yes" rot="R90">
<attribute name="NAME" x="20.955" y="62.23" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.495" y="62.23" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="R9" gate="G$1" x="20.32" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="20.955" y="67.31" size="1.27" layer="95" rot="R90"/>
<attribute name="VALUE" x="23.495" y="67.31" size="1.27" layer="96" rot="R90"/>
</instance>
<instance part="C15" gate="G$1" x="-2.54" y="66.04" smashed="yes">
<attribute name="NAME" x="-1.524" y="67.437" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="-1.524" y="63.373" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP31" gate="G$1" x="-5.08" y="60.96" smashed="yes" rot="R90">
<attribute name="VALUE" x="-10.16" y="58.42" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUP32" gate="G$1" x="-2.54" y="71.12" rot="R180"/>
<instance part="C16" gate="G$1" x="-2.54" y="106.68" smashed="yes">
<attribute name="NAME" x="-8.89" y="107.061" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="-8.89" y="105.283" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="C17" gate="G$1" x="2.54" y="106.68" smashed="yes">
<attribute name="NAME" x="5.08" y="107.315" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="5.08" y="105.537" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP33" gate="G$1" x="-2.54" y="101.6"/>
<instance part="SUP34" gate="G$1" x="2.54" y="101.6"/>
<instance part="U5" gate="G$1" x="185.42" y="137.16"/>
<instance part="L1" gate="G$1" x="210.82" y="152.4" smashed="yes">
<attribute name="NAME" x="205.74" y="156.21" size="1.27" layer="95"/>
<attribute name="VALUE" x="212.09" y="156.21" size="1.27" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="220.98" y="134.62" smashed="yes">
<attribute name="NAME" x="223.52" y="135.255" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="223.266" y="133.477" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP15" gate="G$1" x="220.98" y="116.84"/>
<instance part="SUP16" gate="G$1" x="160.02" y="116.84"/>
<instance part="C9" gate="G$1" x="152.4" y="134.62" smashed="yes">
<attribute name="NAME" x="148.082" y="135.255" size="1.27" layer="95" font="vector"/>
<attribute name="VALUE" x="142.24" y="133.477" size="1.27" layer="96" font="vector"/>
</instance>
<instance part="SUP27" gate="G$1" x="226.06" y="152.4" rot="R270"/>
<instance part="J5" gate="G$1" x="142.24" y="152.4" rot="R180"/>
<instance part="J6" gate="G$1" x="142.24" y="149.86" rot="R180"/>
<instance part="SUP35" gate="G$1" x="147.32" y="149.86" rot="R90"/>
<instance part="SUP36" gate="G$1" x="15.24" y="35.56"/>
<instance part="R3" gate="G$1" x="119.38" y="68.58" smashed="yes" rot="R90">
<attribute name="NAME" x="117.602" y="73.533" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.666" y="73.533" size="1.27" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="119.38" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="117.602" y="71.755" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.666" y="71.755" size="1.27" layer="96"/>
</instance>
<instance part="R10" gate="G$1" x="119.38" y="63.5" smashed="yes" rot="R90">
<attribute name="NAME" x="117.602" y="69.977" size="1.27" layer="95"/>
<attribute name="VALUE" x="121.666" y="69.977" size="1.27" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VSSA"/>
<pinref part="SUP1" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="27.94" x2="58.42" y2="27.94" width="0.1524" layer="91"/>
<wire x1="58.42" y1="27.94" x2="58.42" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSS@1"/>
<wire x1="58.42" y1="25.4" x2="58.42" y2="22.86" width="0.1524" layer="91"/>
<wire x1="60.96" y1="25.4" x2="58.42" y2="25.4" width="0.1524" layer="91"/>
<junction x="58.42" y="25.4"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VSS@3"/>
<pinref part="SUP2" gate="G$1" pin="GND"/>
<wire x1="106.68" y1="25.4" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<wire x1="109.22" y1="25.4" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSS@2"/>
<wire x1="106.68" y1="27.94" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
<wire x1="109.22" y1="27.94" x2="109.22" y2="25.4" width="0.1524" layer="91"/>
<junction x="109.22" y="25.4"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<pinref part="SUP7" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="99.06" x2="45.72" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="SUP8" gate="G$1" pin="GND"/>
<wire x1="48.26" y1="93.98" x2="45.72" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUP5" gate="G$1" pin="GND"/>
<wire x1="119.38" y1="99.06" x2="124.46" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="SUP6" gate="G$1" pin="GND"/>
<wire x1="119.38" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="121.92" y1="93.98" x2="124.46" y2="93.98" width="0.1524" layer="91"/>
<wire x1="121.92" y1="93.98" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<junction x="121.92" y="93.98"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="33.02" y1="86.36" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SUP9" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="33.02" y1="71.12" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SUP10" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUP11" gate="G$1" pin="GND"/>
<wire x1="170.18" y1="73.66" x2="170.18" y2="71.12" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="170.18" y1="71.12" x2="172.72" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP13" gate="G$1" pin="GND"/>
<wire x1="5.08" y1="-25.4" x2="5.08" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="GND"/>
<pinref part="J2" gate="G$1" pin="GND"/>
<wire x1="-20.32" y1="-22.86" x2="5.08" y2="-22.86" width="0.1524" layer="91"/>
<junction x="5.08" y="-22.86"/>
<wire x1="5.08" y1="-22.86" x2="12.7" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@1"/>
<pinref part="SUP17" gate="G$1" pin="GND"/>
<wire x1="147.32" y1="-10.16" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="142.24" y1="-10.16" x2="142.24" y2="-17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP22" gate="G$1" pin="GND"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="134.62" y1="10.16" x2="134.62" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP21" gate="G$1" pin="GND"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="129.54" y1="10.16" x2="129.54" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="SUP18" gate="G$1" pin="GND"/>
<wire x1="142.24" y1="7.62" x2="142.24" y2="10.16" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="SUP20" gate="G$1" pin="GND"/>
<wire x1="134.62" y1="-15.24" x2="134.62" y2="-17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP23" gate="G$1" pin="GND"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="190.5" y1="5.08" x2="190.5" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND@2"/>
<pinref part="SUP24" gate="G$1" pin="GND"/>
<wire x1="177.8" y1="0" x2="180.34" y2="0" width="0.1524" layer="91"/>
<wire x1="180.34" y1="0" x2="180.34" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND@2"/>
<pinref part="SUP28" gate="G$1" pin="GND"/>
<wire x1="-15.24" y1="109.22" x2="-12.7" y2="109.22" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="109.22" x2="-12.7" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="GND@1"/>
<wire x1="-12.7" y1="106.68" x2="-12.7" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="106.68" x2="-12.7" y2="106.68" width="0.1524" layer="91"/>
<junction x="-12.7" y="106.68"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="SUP30" gate="G$1" pin="GND"/>
<wire x1="12.7" y1="116.84" x2="12.7" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="SUP32" gate="G$1" pin="GND"/>
<wire x1="-2.54" y1="68.58" x2="-2.54" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="SUP33" gate="G$1" pin="GND"/>
<wire x1="-2.54" y1="104.14" x2="-2.54" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="SUP34" gate="G$1" pin="GND"/>
<wire x1="2.54" y1="104.14" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND@4"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="198.12" y1="121.92" x2="220.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="220.98" y1="121.92" x2="220.98" y2="127" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND@3"/>
<wire x1="220.98" y1="127" x2="220.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="198.12" y1="119.38" x2="220.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="220.98" y1="119.38" x2="220.98" y2="121.92" width="0.1524" layer="91"/>
<wire x1="220.98" y1="119.38" x2="220.98" y2="116.84" width="0.1524" layer="91"/>
<pinref part="SUP15" gate="G$1" pin="GND"/>
<junction x="220.98" y="119.38"/>
<junction x="220.98" y="121.92"/>
<pinref part="U5" gate="G$1" pin="VPRG1"/>
<wire x1="198.12" y1="127" x2="220.98" y2="127" width="0.1524" layer="91"/>
<junction x="220.98" y="127"/>
</segment>
<segment>
<pinref part="U5" gate="G$1" pin="GND@2"/>
<wire x1="170.18" y1="121.92" x2="160.02" y2="121.92" width="0.1524" layer="91"/>
<wire x1="160.02" y1="121.92" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="GND@1"/>
<wire x1="160.02" y1="119.38" x2="160.02" y2="116.84" width="0.1524" layer="91"/>
<wire x1="170.18" y1="119.38" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
<pinref part="SUP16" gate="G$1" pin="GND"/>
<junction x="160.02" y="119.38"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="152.4" y1="132.08" x2="152.4" y2="119.38" width="0.1524" layer="91"/>
<wire x1="152.4" y1="119.38" x2="160.02" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J6" gate="G$1" pin="P$1"/>
<wire x1="144.78" y1="149.86" x2="147.32" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SUP35" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="GND"/>
<pinref part="U3" gate="G$1" pin="GND"/>
<wire x1="10.16" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<pinref part="SUP36" gate="G$1" pin="GND"/>
<wire x1="15.24" y1="38.1" x2="17.78" y2="38.1" width="0.1524" layer="91"/>
<wire x1="15.24" y1="35.56" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<junction x="15.24" y="38.1"/>
</segment>
</net>
<net name="+3.3V" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="SUP3" gate="G$1" pin="+3.3V"/>
<wire x1="53.34" y1="99.06" x2="58.42" y2="99.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="101.6" width="0.1524" layer="91"/>
<wire x1="58.42" y1="99.06" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="58.42" y1="96.52" x2="58.42" y2="93.98" width="0.1524" layer="91"/>
<wire x1="58.42" y1="93.98" x2="53.34" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDA"/>
<wire x1="58.42" y1="93.98" x2="60.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD@1"/>
<wire x1="60.96" y1="96.52" x2="58.42" y2="96.52" width="0.1524" layer="91"/>
<junction x="58.42" y="99.06"/>
<junction x="58.42" y="96.52"/>
<junction x="58.42" y="93.98"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD@2"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="106.68" y1="93.98" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SUP4" gate="G$1" pin="+3.3V"/>
<wire x1="109.22" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="101.6" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDD@3"/>
<wire x1="109.22" y1="99.06" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<wire x1="109.22" y1="96.52" x2="109.22" y2="93.98" width="0.1524" layer="91"/>
<wire x1="106.68" y1="96.52" x2="109.22" y2="96.52" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="114.3" y1="99.06" x2="109.22" y2="99.06" width="0.1524" layer="91"/>
<junction x="109.22" y="99.06"/>
<junction x="109.22" y="96.52"/>
<junction x="109.22" y="93.98"/>
</segment>
<segment>
<pinref part="J3" gate="G$1" pin="VDD"/>
<pinref part="J2" gate="G$1" pin="VDD"/>
<wire x1="-20.32" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="SUP14" gate="G$1" pin="+3.3V"/>
<wire x1="-15.24" y1="-10.16" x2="12.7" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="-2.54" x2="-15.24" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-15.24" y="-10.16"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="134.62" y1="-2.54" x2="129.54" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="129.54" y1="-2.54" x2="129.54" y2="2.54" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="134.62" y1="2.54" x2="134.62" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="SUP19" gate="G$1" pin="+3.3V"/>
<wire x1="129.54" y1="-2.54" x2="127" y2="-2.54" width="0.1524" layer="91"/>
<junction x="129.54" y="-2.54"/>
<pinref part="U2" gate="G$1" pin="VDD"/>
<wire x1="147.32" y1="-2.54" x2="134.62" y2="-2.54" width="0.1524" layer="91"/>
<junction x="134.62" y="-2.54"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="190.5" y1="-2.54" x2="190.5" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VDDIO"/>
<wire x1="190.5" y1="-5.08" x2="177.8" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="SUP25" gate="G$1" pin="+3.3V"/>
<wire x1="193.04" y1="-5.08" x2="190.5" y2="-5.08" width="0.1524" layer="91"/>
<junction x="190.5" y="-5.08"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="182.88" y1="-22.86" x2="182.88" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-25.4" x2="187.96" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-25.4" x2="180.34" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="180.34" y1="-25.4" x2="180.34" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="SUP26" gate="G$1" pin="+3.3V"/>
<junction x="182.88" y="-25.4"/>
</segment>
<segment>
<pinref part="SUP29" gate="G$1" pin="+3.3V"/>
<pinref part="U4" gate="G$1" pin="VDD"/>
<wire x1="17.78" y1="124.46" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="12.7" y1="124.46" x2="-15.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="12.7" y1="121.92" x2="12.7" y2="124.46" width="0.1524" layer="91"/>
<junction x="12.7" y="124.46"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="63.5" x2="-2.54" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SUP31" gate="G$1" pin="+3.3V"/>
<wire x1="-5.08" y1="60.96" x2="-2.54" y2="60.96" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="-2.54" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<junction x="-2.54" y="60.96"/>
</segment>
<segment>
<pinref part="L1" gate="G$1" pin="P$2"/>
<wire x1="218.44" y1="152.4" x2="220.98" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="FB"/>
<wire x1="220.98" y1="152.4" x2="226.06" y2="152.4" width="0.1524" layer="91"/>
<wire x1="198.12" y1="142.24" x2="220.98" y2="142.24" width="0.1524" layer="91"/>
<wire x1="220.98" y1="142.24" x2="220.98" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="220.98" y1="142.24" x2="220.98" y2="137.16" width="0.1524" layer="91"/>
<junction x="220.98" y="142.24"/>
<junction x="220.98" y="152.4"/>
<pinref part="SUP27" gate="G$1" pin="+3.3V"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="SUP12" gate="G$1" pin="+3.3V"/>
<wire x1="129.54" y1="91.44" x2="129.54" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PH0/OSC_IN"/>
<wire x1="60.96" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<wire x1="55.88" y1="81.28" x2="55.88" y2="86.36" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="55.88" y1="86.36" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="P$2"/>
<wire x1="45.72" y1="86.36" x2="38.1" y2="86.36" width="0.1524" layer="91"/>
<wire x1="45.72" y1="83.82" x2="45.72" y2="86.36" width="0.1524" layer="91"/>
<junction x="45.72" y="86.36"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="38.1" y1="71.12" x2="45.72" y2="71.12" width="0.1524" layer="91"/>
<wire x1="45.72" y1="71.12" x2="50.8" y2="71.12" width="0.1524" layer="91"/>
<wire x1="50.8" y1="71.12" x2="50.8" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PH1/OSC_OUT"/>
<wire x1="50.8" y1="78.74" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<pinref part="Q1" gate="G$1" pin="P$1"/>
<wire x1="45.72" y1="73.66" x2="45.72" y2="71.12" width="0.1524" layer="91"/>
<junction x="45.72" y="71.12"/>
</segment>
</net>
<net name="!RESET" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="RES"/>
<wire x1="172.72" y1="58.42" x2="165.1" y2="58.42" width="0.1524" layer="91"/>
<label x="165.1" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="!RESET!"/>
<wire x1="106.68" y1="78.74" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="129.54" y1="83.82" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<label x="132.08" y="78.74" size="1.27" layer="95" xref="yes"/>
<wire x1="132.08" y1="78.74" x2="129.54" y2="78.74" width="0.1524" layer="91"/>
<junction x="129.54" y="78.74"/>
</segment>
</net>
<net name="PROG" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="BOOT0"/>
<wire x1="106.68" y1="81.28" x2="121.92" y2="81.28" width="0.1524" layer="91"/>
<label x="132.08" y="81.28" size="1.27" layer="95" xref="yes"/>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="121.92" y1="81.28" x2="132.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="121.92" y1="83.82" x2="121.92" y2="81.28" width="0.1524" layer="91"/>
<junction x="121.92" y="81.28"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="PRG"/>
<wire x1="172.72" y1="66.04" x2="165.1" y2="66.04" width="0.1524" layer="91"/>
<label x="165.1" y="66.04" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TXD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA9"/>
<wire x1="60.96" y1="48.26" x2="53.34" y2="48.26" width="0.1524" layer="91"/>
<label x="53.34" y="48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="TXD"/>
<wire x1="172.72" y1="60.96" x2="165.1" y2="60.96" width="0.1524" layer="91"/>
<label x="165.1" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="RXD" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA10"/>
<wire x1="60.96" y1="45.72" x2="53.34" y2="45.72" width="0.1524" layer="91"/>
<label x="53.34" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="RXD"/>
<wire x1="172.72" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<label x="165.1" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="MISO"/>
<pinref part="J2" gate="G$1" pin="MISO"/>
<wire x1="-20.32" y1="-17.78" x2="2.54" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-17.78" x2="12.7" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="2.54" width="0.1524" layer="91"/>
<wire x1="2.54" y1="2.54" x2="12.7" y2="2.54" width="0.1524" layer="91"/>
<junction x="2.54" y="-17.78"/>
<label x="12.7" y="2.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="MISO"/>
<wire x1="-43.18" y1="119.38" x2="-50.8" y2="119.38" width="0.1524" layer="91"/>
<label x="-50.8" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="124.46" y1="66.04" x2="127" y2="66.04" width="0.1524" layer="91"/>
<label x="127" y="66.04" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="MOSI"/>
<pinref part="J2" gate="G$1" pin="MOSI"/>
<wire x1="-20.32" y1="-15.24" x2="0" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="0" y1="-15.24" x2="12.7" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="0" y1="-15.24" x2="0" y2="5.08" width="0.1524" layer="91"/>
<wire x1="0" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="91"/>
<junction x="0" y="-15.24"/>
<label x="12.7" y="5.08" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="MOSI"/>
<wire x1="-43.18" y1="121.92" x2="-50.8" y2="121.92" width="0.1524" layer="91"/>
<label x="-50.8" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="124.46" y1="63.5" x2="127" y2="63.5" width="0.1524" layer="91"/>
<label x="127" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCLK" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="SCLK"/>
<pinref part="J2" gate="G$1" pin="SCLK"/>
<wire x1="-20.32" y1="-12.7" x2="-2.54" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-12.7" x2="12.7" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-12.7" x2="-2.54" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="91"/>
<junction x="-2.54" y="-12.7"/>
<label x="12.7" y="7.62" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="SCLK"/>
<wire x1="-43.18" y1="124.46" x2="-50.8" y2="124.46" width="0.1524" layer="91"/>
<label x="-50.8" y="124.46" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="124.46" y1="68.58" x2="127" y2="68.58" width="0.1524" layer="91"/>
<label x="127" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="!CSEL2" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="!CSEL"/>
<wire x1="12.7" y1="-20.32" x2="7.62" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-20.32" x2="7.62" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-2.54" x2="12.7" y2="-2.54" width="0.1524" layer="91"/>
<label x="12.7" y="-2.54" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB9"/>
<wire x1="106.68" y1="48.26" x2="114.3" y2="48.26" width="0.1524" layer="91"/>
<label x="114.3" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="!CSEL1" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="!CSEL"/>
<wire x1="-20.32" y1="-20.32" x2="5.08" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-20.32" x2="5.08" y2="0" width="0.1524" layer="91"/>
<wire x1="5.08" y1="0" x2="12.7" y2="0" width="0.1524" layer="91"/>
<label x="12.7" y="0" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB8"/>
<wire x1="106.68" y1="50.8" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<label x="114.3" y="50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="142.24" y1="0" x2="142.24" y2="2.54" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CAP-A"/>
<wire x1="147.32" y1="0" x2="142.24" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SCL"/>
<wire x1="177.8" y1="-7.62" x2="180.34" y2="-7.62" width="0.1524" layer="91"/>
<label x="185.42" y="-7.62" size="1.27" layer="95" xref="yes"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="180.34" y1="-7.62" x2="185.42" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="180.34" y1="-15.24" x2="180.34" y2="-7.62" width="0.1524" layer="91"/>
<junction x="180.34" y="-7.62"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB10"/>
<wire x1="106.68" y1="45.72" x2="114.3" y2="45.72" width="0.1524" layer="91"/>
<label x="114.3" y="45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SDA"/>
<wire x1="177.8" y1="-10.16" x2="182.88" y2="-10.16" width="0.1524" layer="91"/>
<label x="185.42" y="-10.16" size="1.27" layer="95" xref="yes"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="182.88" y1="-10.16" x2="185.42" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-15.24" x2="182.88" y2="-10.16" width="0.1524" layer="91"/>
<junction x="182.88" y="-10.16"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB11"/>
<wire x1="106.68" y1="43.18" x2="114.3" y2="43.18" width="0.1524" layer="91"/>
<label x="114.3" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="D-"/>
<pinref part="U1" gate="G$1" pin="PA11/USB-"/>
<wire x1="45.72" y1="43.18" x2="60.96" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="D+"/>
<pinref part="U1" gate="G$1" pin="PA12/USB+"/>
<wire x1="45.72" y1="40.64" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="VBUS"/>
<pinref part="U3" gate="G$1" pin="VBUS"/>
<wire x1="10.16" y1="45.72" x2="17.78" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB-" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D-"/>
<pinref part="U3" gate="G$1" pin="D-BUS"/>
<wire x1="10.16" y1="43.18" x2="17.78" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USB+" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="D+"/>
<pinref part="U3" gate="G$1" pin="D+BUS"/>
<wire x1="10.16" y1="40.64" x2="17.78" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!CSEL3" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="!CSEL"/>
<wire x1="-43.18" y1="116.84" x2="-50.8" y2="116.84" width="0.1524" layer="91"/>
<label x="-50.8" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB12"/>
<wire x1="106.68" y1="40.64" x2="114.3" y2="40.64" width="0.1524" layer="91"/>
<label x="114.3" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GDO0" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="GDO0"/>
<wire x1="-43.18" y1="109.22" x2="-50.8" y2="109.22" width="0.1524" layer="91"/>
<label x="-50.8" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB6"/>
<wire x1="106.68" y1="58.42" x2="114.3" y2="58.42" width="0.1524" layer="91"/>
<label x="114.3" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GDO2" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="GDO2"/>
<wire x1="-43.18" y1="106.68" x2="-50.8" y2="106.68" width="0.1524" layer="91"/>
<label x="-50.8" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PB7"/>
<wire x1="106.68" y1="55.88" x2="114.3" y2="55.88" width="0.1524" layer="91"/>
<label x="114.3" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA7"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="60.96" y1="55.88" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA6"/>
<wire x1="60.96" y1="58.42" x2="27.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="27.94" y1="58.42" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="27.94" y1="60.96" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="25.4" y1="66.04" x2="30.48" y2="66.04" width="0.1524" layer="91"/>
<wire x1="30.48" y1="66.04" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PA5"/>
<wire x1="30.48" y1="60.96" x2="60.96" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="B"/>
<wire x1="17.78" y1="66.04" x2="12.7" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="G"/>
<wire x1="17.78" y1="60.96" x2="12.7" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="D1" gate="G$1" pin="R"/>
<wire x1="17.78" y1="55.88" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="VCP2"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="114.3" x2="-2.54" y2="114.3" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="114.3" x2="-2.54" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="VCP1"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="119.38" x2="2.54" y2="119.38" width="0.1524" layer="91"/>
<wire x1="2.54" y1="119.38" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SW"/>
<pinref part="L1" gate="G$1" pin="P$1"/>
<wire x1="198.12" y1="152.4" x2="203.2" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="SS"/>
<wire x1="198.12" y1="132.08" x2="200.66" y2="132.08" width="0.1524" layer="91"/>
<wire x1="200.66" y1="132.08" x2="200.66" y2="129.54" width="0.1524" layer="91"/>
<pinref part="U5" gate="G$1" pin="VPRG2"/>
<wire x1="200.66" y1="129.54" x2="198.12" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U5" gate="G$1" pin="VIN"/>
<wire x1="170.18" y1="152.4" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="152.4" y1="137.16" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
<junction x="152.4" y="152.4"/>
<pinref part="J5" gate="G$1" pin="P$1"/>
<wire x1="144.78" y1="152.4" x2="152.4" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB5"/>
<wire x1="106.68" y1="60.96" x2="111.76" y2="60.96" width="0.1524" layer="91"/>
<wire x1="111.76" y1="60.96" x2="114.3" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="114.3" y1="63.5" x2="116.84" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB4"/>
<wire x1="106.68" y1="63.5" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<wire x1="111.76" y1="63.5" x2="114.3" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="114.3" y1="66.04" x2="116.84" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PB3"/>
<wire x1="106.68" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<wire x1="111.76" y1="66.04" x2="114.3" y2="68.58" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="114.3" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="134.62" y1="-10.16" x2="134.62" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="CAP-R"/>
<wire x1="147.32" y1="-7.62" x2="134.62" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
