<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="6.4">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="9" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Courtyard" color="13" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="near-ir">
<description>5mm x 4mm, 16 + 1 pin DFN package</description>
<packages>
<package name="OPTICAL">
<pad name="GND" x="0" y="6.35" drill="1" shape="square"/>
<pad name="SCLK" x="0" y="3.81" drill="1"/>
<pad name="MISO" x="0" y="1.27" drill="1"/>
<pad name="MOSI" x="0" y="-1.27" drill="1"/>
<pad name="!CSEL" x="0" y="-3.81" drill="1"/>
<pad name="VDD" x="0" y="-6.35" drill="1"/>
<wire x1="-1.27" y1="7.62" x2="1.27" y2="7.62" width="0.2" layer="21"/>
<wire x1="1.27" y1="7.62" x2="1.27" y2="-7.62" width="0.2" layer="21"/>
<wire x1="1.27" y1="-7.62" x2="-1.27" y2="-7.62" width="0.2" layer="21"/>
<wire x1="-1.27" y1="-7.62" x2="-1.27" y2="7.62" width="0.2" layer="21"/>
<text x="-2.54" y="-6.35" size="1" layer="25" font="vector" ratio="18" rot="R90">&gt;NAME</text>
<text x="-2.54" y="1.27" size="1" layer="27" font="vector" ratio="18" rot="R90">&gt;VALUE</text>
<circle x="1.7" y="7.4" radius="0.111803125" width="0.2" layer="21"/>
</package>
<package name="CAPPRD500W50D1050H1750">
<description>&lt;b&gt;UVZ (10x16)&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="0.7" diameter="1.1" shape="square"/>
<pad name="2" x="5" y="0" drill="0.7" diameter="1.1"/>
<text x="-6" y="5.1" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-5.5" y="-4.5" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="2.5" y="0" radius="5.75" width="0.05" layer="21"/>
<circle x="2.5" y="0" radius="5.25" width="0.2" layer="25"/>
<circle x="2.5" y="0" radius="5.25" width="0.1" layer="51"/>
</package>
<package name="T-1(3MM)_2">
<description>&lt;b&gt;T-1(3MM)_2&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="-1.27" y="0" drill="0.98" diameter="1.54"/>
<pad name="2" x="1.27" y="0" drill="0.98" diameter="1.54"/>
<text x="-0.339" y="2.598" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.039" y="-2.642" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="0" y="0" radius="1.6" width="0.2" layer="51"/>
<wire x1="-1.249" y1="-1" x2="1.249" y2="-1" width="0.2" layer="21" curve="102.6"/>
<wire x1="-1.249" y1="1" x2="1.249" y2="1" width="0.2" layer="21" curve="-102.6"/>
<circle x="-2.274" y="0.04" radius="0.046" width="0.2" layer="25"/>
</package>
<package name="ADNS5090-I">
<pad name="MISO" x="-2.8" y="6.4" drill="0.8"/>
<pad name="LED" x="-0.8" y="6.4" drill="0.8"/>
<pad name="MOTION" x="1.2" y="6.4" drill="0.8"/>
<pad name="NCS" x="3.2" y="6.4" drill="0.8"/>
<pad name="MOSI" x="-3.8" y="-6.4" drill="0.8"/>
<pad name="VDD" x="-1.8" y="-6.4" drill="0.8"/>
<pad name="GND" x="0.2" y="-6.4" drill="0.8"/>
<pad name="SCLK" x="2.2" y="-6.4" drill="0.8"/>
<wire x1="-5.2" y1="4.6" x2="5" y2="4.6" width="0.127" layer="21"/>
<wire x1="5" y1="4.6" x2="5" y2="-4.6" width="0.127" layer="21"/>
<wire x1="5" y1="-4.6" x2="-5.2" y2="-4.6" width="0.127" layer="21"/>
<wire x1="-5.2" y1="-4.6" x2="-5.2" y2="4.6" width="0.127" layer="21"/>
<circle x="-4.4" y="4" radius="0.282840625" width="0.127" layer="21"/>
<text x="-5.2" y="7.6" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.2" y="-9.2" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="OPTICAL">
<pin name="VDD" x="-10.16" y="5.08" length="middle"/>
<pin name="SCLK" x="-10.16" y="2.54" length="middle"/>
<pin name="MOSI" x="-10.16" y="0" length="middle"/>
<pin name="MISO" x="-10.16" y="-2.54" length="middle"/>
<pin name="!CSEL" x="-10.16" y="-5.08" length="middle"/>
<pin name="GND" x="-10.16" y="-7.62" length="middle"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<text x="-5.08" y="-12.7" size="1.27" layer="96" ratio="16">&gt;VALUE</text>
<text x="-5.08" y="10.16" size="1.27" layer="95" ratio="16">&gt;NAME</text>
</symbol>
<symbol name="UVZ2E4R7MPD">
<wire x1="5.588" y1="2.54" x2="5.588" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.588" y2="0" width="0.254" layer="94"/>
<wire x1="7.112" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" visible="pad" length="middle" direction="pwr"/>
<pin name="-" x="12.7" y="0" visible="pad" length="middle" direction="pwr" rot="R180"/>
</symbol>
<symbol name="L-934HD">
<wire x1="5.08" y1="0" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="10.16" y2="2.54" width="0.254" layer="94"/>
<wire x1="10.16" y1="-2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="3.81" y2="5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="2.54" x2="6.35" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="12.7" y2="0" width="0.254" layer="94"/>
<text x="12.7" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="12.7" y="6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="K" x="0" y="0" visible="pad" length="short"/>
<pin name="A" x="15.24" y="0" visible="pad" length="short" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="5.334" y="4.318"/>
<vertex x="4.572" y="3.556"/>
<vertex x="3.81" y="5.08"/>
</polygon>
<polygon width="0.254" layer="94">
<vertex x="7.874" y="4.318"/>
<vertex x="7.112" y="3.556"/>
<vertex x="6.35" y="5.08"/>
</polygon>
</symbol>
<symbol name="GND">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.6096" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.016" width="0.254" layer="94"/>
<pin name="GND" x="0" y="0" visible="off" length="point" direction="sup" rot="R270"/>
</symbol>
<symbol name="ADNS5090-I">
<pin name="MISO" x="-33.02" y="15.24" length="middle" direction="pas"/>
<pin name="LED" x="-33.02" y="10.16" length="middle" direction="pas"/>
<pin name="MOTION" x="-33.02" y="5.08" length="middle" direction="pas"/>
<pin name="NCS" x="-33.02" y="0" length="middle" direction="pas"/>
<pin name="SCLK" x="0" y="15.24" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="0" y="10.16" length="middle" direction="pas" rot="R180"/>
<pin name="VDD" x="0" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="MOSI" x="0" y="0" length="middle" direction="pas" rot="R180"/>
<wire x1="-27.94" y1="17.78" x2="-27.94" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="-5.08" y1="17.78" x2="-27.94" y2="17.78" width="0.254" layer="94"/>
<text x="-27.94" y="-5.08" size="1.27" layer="96">&gt;VALUE</text>
<text x="-27.94" y="20.32" size="1.27" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="OPTICAL" prefix="J">
<gates>
<gate name="G$1" symbol="OPTICAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OPTICAL">
<connects>
<connect gate="G$1" pin="!CSEL" pad="!CSEL"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="SCLK" pad="SCLK"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UVZ2E4R7MPD" prefix="C">
<description>&lt;b&gt;Nichicon 4.7F 250 V Aluminium Electrolytic Capacitor, VZ Series 1000h 8 x 11.5mm&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://nichicon-us.com/english/products/pdfs/e-uvz.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="UVZ2E4R7MPD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="CAPPRD500W50D1050H1750">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Nichicon 4.7F 250 V Aluminium Electrolytic Capacitor, VZ Series 1000h 8 x 11.5mm" constant="no"/>
<attribute name="DIGIKEY_PART_NUMBER" value="493-1414-ND" constant="no"/>
<attribute name="HEIGHT" value="17.5mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Nichicon" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="UVZ2E4R7MPD" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L-934HD" prefix="LED">
<description>&lt;b&gt;Red diffused LED,L934HD 25mA 3mm Kingbright L-934HD, Round Series Red LED, 660 nm 3mm (T-1), Round Lens Through Hole package&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://uk.rs-online.com/web/p/products/2285922A"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="L-934HD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="T-1(3MM)_2">
<connects>
<connect gate="G$1" pin="A" pad="2"/>
<connect gate="G$1" pin="K" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Red diffused LED,L934HD 25mA 3mm Kingbright L-934HD, Round Series Red LED, 660 nm 3mm (T-1), Round Lens Through Hole package" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Kingbright" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="L-934HD" constant="no"/>
<attribute name="RS_PART_NUMBER" value="2285922A" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="SUP">
<gates>
<gate name="G$1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADNS5090-I" prefix="U">
<description>The inverse of ADNS5090 due to the optical window position,</description>
<gates>
<gate name="G$1" symbol="ADNS5090-I" x="17.78" y="-7.62"/>
</gates>
<devices>
<device name="" package="ADNS5090-I">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="LED" pad="LED"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="MOTION" pad="MOTION"/>
<connect gate="G$1" pin="NCS" pad="NCS"/>
<connect gate="G$1" pin="SCLK" pad="SCLK"/>
<connect gate="G$1" pin="VDD" pad="VDD"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="UPS1E101MED">
<description>&lt;Al electrolytic cap,PS,25V,100 F Nichicon 100F 25 V dc Aluminium Electrolytic Capacitor, PS Series 2000h 6.3 (Dia.) x 11mm&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="UPS(6.3X11)">
<description>&lt;b&gt;UPS(6.3x11)&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="1.25" drill="0.7" diameter="1.2"/>
<pad name="2" x="0" y="-1.25" drill="0.7" diameter="1.2"/>
<text x="-0.316" y="0.064" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.316" y="0.064" size="1.27" layer="27" align="center">&gt;VALUE</text>
<circle x="0" y="0" radius="3.15" width="0.254" layer="25"/>
<circle x="0" y="0" radius="3.15" width="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="UPS1E101MED">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.842" y2="2.54" width="0.254" layer="94"/>
<wire x1="4.572" y1="1.27" x2="3.556" y2="1.27" width="0.254" layer="94"/>
<wire x1="4.064" y1="1.778" x2="4.064" y2="0.762" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<text x="8.89" y="6.35" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="8.89" y="3.81" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="+" x="0" y="0" visible="pad" length="short"/>
<pin name="-" x="12.7" y="0" visible="pad" length="short" rot="R180"/>
<polygon width="0.254" layer="94">
<vertex x="7.62" y="2.54"/>
<vertex x="7.62" y="-2.54"/>
<vertex x="6.858" y="-2.54"/>
<vertex x="6.858" y="2.54"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="UPS1E101MED" prefix="C">
<description>&lt;b&gt;Al electrolytic cap,PS,25V,100 F Nichicon 100F 25 V dc Aluminium Electrolytic Capacitor, PS Series 2000h 6.3 (Dia.) x 11mm&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://uk.rs-online.com/web/p/products/1710679"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="UPS1E101MED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UPS(6.3X11)">
<connects>
<connect gate="G$1" pin="+" pad="1"/>
<connect gate="G$1" pin="-" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="ALLIED_NUMBER" value="70187653" constant="no"/>
<attribute name="DESCRIPTION" value="Al electrolytic cap,PS,25V,100 F Nichicon 100F 25 V dc Aluminium Electrolytic Capacitor, PS Series 2000h 6.3 (Dia.) x 11mm" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Nichicon" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="UPS1E101MED" constant="no"/>
<attribute name="RS_PART_NUMBER" value="1710679" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="J1" library="near-ir" deviceset="OPTICAL" device=""/>
<part name="LED1" library="near-ir" deviceset="L-934HD" device=""/>
<part name="SUP1" library="near-ir" deviceset="GND" device=""/>
<part name="SUP2" library="near-ir" deviceset="GND" device=""/>
<part name="SUP5" library="near-ir" deviceset="GND" device=""/>
<part name="C3" library="UPS1E101MED" deviceset="UPS1E101MED" device="" value="100u"/>
<part name="C4" library="near-ir" deviceset="UVZ2E4R7MPD" device="" value="4.7u"/>
<part name="U2" library="near-ir" deviceset="ADNS5090-I" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="J1" gate="G$1" x="132.08" y="58.42"/>
<instance part="LED1" gate="G$1" x="-17.78" y="55.88" smashed="yes" rot="R180">
<attribute name="NAME" x="-30.48" y="46.99" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-30.48" y="49.53" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUP1" gate="G$1" x="116.84" y="50.8" rot="R270"/>
<instance part="SUP2" gate="G$1" x="27.94" y="55.88" rot="R90"/>
<instance part="SUP5" gate="G$1" x="58.42" y="27.94"/>
<instance part="C3" gate="G$1" x="43.18" y="45.72" smashed="yes" rot="R270">
<attribute name="NAME" x="49.53" y="36.83" size="1.778" layer="95" rot="R270" align="center-left"/>
<attribute name="VALUE" x="46.99" y="36.83" size="1.778" layer="96" rot="R270" align="center-left"/>
</instance>
<instance part="C4" gate="G$1" x="58.42" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="52.07" y="41.91" size="1.778" layer="95" rot="R90" align="center-left"/>
<attribute name="VALUE" x="54.61" y="41.91" size="1.778" layer="96" rot="R90" align="center-left"/>
</instance>
<instance part="U2" gate="G$1" x="22.86" y="45.72"/>
</instances>
<busses>
</busses>
<nets>
<net name="VDD" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="VDD"/>
<wire x1="116.84" y1="63.5" x2="121.92" y2="63.5" width="0.1524" layer="91"/>
<label x="116.84" y="63.5" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="22.86" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="43.18" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="45.72" width="0.1524" layer="91"/>
<wire x1="58.42" y1="50.8" x2="63.5" y2="50.8" width="0.1524" layer="91"/>
<label x="63.5" y="50.8" size="1.27" layer="95" xref="yes"/>
<junction x="58.42" y="50.8"/>
<pinref part="C3" gate="G$1" pin="+"/>
<wire x1="43.18" y1="45.72" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<junction x="43.18" y="50.8"/>
<pinref part="C4" gate="G$1" pin="-"/>
<pinref part="U2" gate="G$1" pin="VDD"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<wire x1="-33.02" y1="55.88" x2="-38.1" y2="55.88" width="0.1524" layer="91"/>
<label x="-38.1" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="MISO"/>
<wire x1="121.92" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<label x="116.84" y="55.88" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-10.16" y1="60.96" x2="-17.78" y2="60.96" width="0.1524" layer="91"/>
<label x="-17.78" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="MISO"/>
</segment>
</net>
<net name="!CSEL" class="0">
<segment>
<wire x1="-10.16" y1="45.72" x2="-15.24" y2="45.72" width="0.1524" layer="91"/>
<label x="-15.24" y="45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="U2" gate="G$1" pin="NCS"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="!CSEL"/>
<wire x1="121.92" y1="53.34" x2="116.84" y2="53.34" width="0.1524" layer="91"/>
<label x="116.84" y="53.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCLK" class="0">
<segment>
<wire x1="22.86" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
<label x="27.94" y="60.96" size="1.27" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="SCLK"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="SCLK"/>
<wire x1="121.92" y1="60.96" x2="116.84" y2="60.96" width="0.1524" layer="91"/>
<label x="116.84" y="60.96" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="22.86" y1="45.72" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
<label x="27.94" y="45.72" size="1.27" layer="95" xref="yes"/>
<pinref part="U2" gate="G$1" pin="MOSI"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="MOSI"/>
<wire x1="121.92" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
<label x="116.84" y="58.42" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="GND"/>
<wire x1="121.92" y1="50.8" x2="116.84" y2="50.8" width="0.1524" layer="91"/>
<pinref part="SUP1" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUP2" gate="G$1" pin="GND"/>
<wire x1="22.86" y1="55.88" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
<segment>
<pinref part="SUP5" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="33.02" x2="58.42" y2="30.48" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="+"/>
<pinref part="C3" gate="G$1" pin="-"/>
<wire x1="58.42" y1="30.48" x2="58.42" y2="27.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="33.02" x2="43.18" y2="30.48" width="0.1524" layer="91"/>
<wire x1="43.18" y1="30.48" x2="58.42" y2="30.48" width="0.1524" layer="91"/>
<junction x="58.42" y="30.48"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="K"/>
<wire x1="-17.78" y1="55.88" x2="-10.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="LED"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
