## Description for Python codes

| Name          | Update date   | Update details                                  |
| :---:               | ---                       | ---                                               |
| version 1         |    23/8/2018 | Complete the basic function.  |
| version 2         |    12/9/2018 | Provide the interface of choosing specific trained model. <br>   Provide the metrics of mean square error and absolute error.  <br> Avoid outputting unlabelled sample images.|
| version 3         |    28/9/2018 | Provide the interface of choosing specific cropping boundary. |
| version 4         |    29/9/2018 | Provide the interface of randomly rotating images  <br> Provide an interface for displaying training image. <br> Encapsulate all the processing steps into one function. |
| version 5         |    2/10/2018 | Removing interface for displaying training image.<br> Provide a detailed output form including predicted angle, ground truth and index.<br> Optimize the process of the encapsulated function <br> Provide the interface of outputting the predicting orientation as csv file|
| version 5_gui     |    3/10/2018 | Include a Graphical User Interface, providing both training and testing options|
| version 6         |    4/10/2018 | Split the calculation of orientation out of the main function, and it will be turned into an interface in future work.<br>Provide the interface of choosing head or body as the detecting target.|
| version 7         |    5/10/2018 | Correct the inline comments.<br>Add error warning.<br>Remove some unnecessary metrics.<br>Add the interface for choosing either video or image as input.|
| version 8         |    6/10/2018 | Add the function of generating pos file automatically.                       |
| version 9         |    7/10/2018 | Remove the normalization mechanism.<br> Correct the calculation of acquiring the body direction.           |
| version 9_gui     |   12/10/2018 | Include a Graphical User Interface for version 9 |
