from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.optimizers import Adam
import numpy as np
import cv2
from keras.layers import Flatten, Dropout, Dense
from keras.applications.vgg16 import VGG16
from keras.models import Model, load_model
import random
import tensorflow as tf
import keras.backend.tensorflow_backend as K
K.set_session(tf.Session(config=tf.ConfigProto(device_count={'gpu':0})))
import os
import imutils
import pandas 

from tkinter import *
from tkinter import filedialog

# We create a layer which take as input movies of shape
# (n_frames, width, height, channels) and returns a movie
# of identical shape.
K.set_image_dim_ordering('tf')


def data_generator(label_dict, batch_size, cropping_list, min_angel, max_angel, size_x=None, size_y=None, training=True, angle_range=1):
    
    #set the parameter of images
    # warning: not interface of changing these parameters
    x_centre = 1280/2
    y_centre = 720/2
    
    num_dataset = len(label_dict)
    steps_each_epoch = int(num_dataset / batch_size)
    # Training generator
    if training:
        while 1:  
            for i in range(steps_each_epoch):
                #start load data in batch 
                mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
                X = []
                Y = []
                for image_pair in mini_batch:
                    #image_pair[0]: name 
                    #image_pair[1]: label
                    image_path = image_pair[0]
                    #print(image_pair)
                    #cropping index
                    cropping = cropping_list[image_pair[2]]
                    image = cv2.imread(image_path)
                    ##randomly rotate the image
                    # here range is -15 to 15 degree
                    rand_angle = angle_range
                    rand_angle = np.random.randint(rand_angle)
                    rand_angle = rand_angle - (rand_angle/2)
                    # rotate the image
                    image = imutils.rotate(image, rand_angle)
                    #get the center and half side length of cropping area
                    x_point = np.mean([cropping[2],cropping[3]])
                    y_point = np.mean([cropping[0],cropping[1]])
                    side = int(np.abs(x_point - cropping[2]))
                    # relevant placement vs centre
                    xx = (x_point - x_centre)
                    yy = (y_point - y_centre)
                    # get distance between centre and image centre
                    rr = (xx**2+yy**2)**0.5
                    # get abs position
                    if xx > 0:
                        if yy > 0:
                            hori_angle = np.arctan(-yy/xx) - rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = np.arctan(-yy/xx) + rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    if xx < 0:
                        if yy > 0:
                            hori_angle = np.arctan(-yy/xx) + rand_angle*np.pi/180 + np.pi
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = np.pi - np.arctan(-yy/xx) + rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    if xx == 0:
                        if yy == 0:
                            x_2 = x_point
                            y_2 = y_point
                        if yy > 0:
                            hori_angle = -(90-rand_angle)*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = (90+rand_angle)*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    if yy == 0:
                        if xx > 0:
                            hori_angle = rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = (180+rand_angle)*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    
                    #image cropping
                    image = image[y_2-side:y_2+side, x_2-side:x_2+side,:]
                    #### keep image normalization please
                    image = np.divide(image, 255.0)
                    image = np.subtract(image, 0.5)
                    image = np.multiply(image, 2.0)
                    if (size_x!=None) & (size_y!=None):
                        image = cv2.resize(image, (size_x, size_y))
                    #x,y,z = np.shape(image)
                    #image = np.reshape(image,(x,y,1))
                    #### data normalization please
                    label = image_pair[1]
                    label = label + rand_angle
                    if label>360:
                        label = label-360
                    if label<0:
                        label = label+360
                    label = (label - min_angel)/ (max_angel-min_angel)
                    #print('label',label)
                    X.append(image)
                    Y.append(label)
                X = np.array(X)
                Y = np.array(Y)
                yield (X,Y)
    # Test generator
    else:
        for i in range(steps_each_epoch):
            #start load data in batch 
            mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
            X = []
            Y = []
            Z = []
            index = []
            for image_pair in mini_batch:
                #image_pair[0]: name 
                #image_pair[1]: label
                image_path = image_pair[0]
                #cropping index
                cropping = cropping_list[image_pair[2]]
                image = cv2.imread(image_path)
                image = image[cropping[0]:cropping[1],cropping[2]:cropping[3],:]
                #### keep image normalization please
                image = np.divide(image, 255.0)
                image = np.subtract(image, 0.5)
                image = np.multiply(image, 2.0)
                if (size_x!=None) & (size_y!=None):
                    image = cv2.resize(image, (size_x, size_y))
                #x,y,z = np.shape(image)
                #image = np.reshape(image,(x,y,1))
                #### data normalization please
                label = image_pair[1]
                label = (label - min_angel)/ (max_angel-min_angel)
                X.append(image)
                Y.append(label)
                Z.append(image_path)
                index.append(image_pair[2])
            X = np.array(X)
            Y = np.array(Y)
            yield (X,Y,Z,index)

def build_model(input_shape,regr_num=1):

    # 初始化全连接层的resnet网络
    baseModel = VGG16(include_top=False, weights=None, input_shape=input_shape)
    # 导入权重
    baseModel.load_weights('./vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',by_name=True)
    # 导出resnet最后一层输出，接上全连接层
    x = baseModel.output
    x = Flatten()(x)
    # 注意：在回归函数中，（vv）选择sigmoid作为激活函数是为了将输出归一化到0-1之间，使得loss稳定 容易收敛，相对的输入也必须归一化到0-1之间。
    # 可根据实际情况，选择激活函数，也可以恒等映射（不建议）
    regr_predictions = Dense(regr_num, kernel_initializer='normal', activation='sigmoid', name='fc_regr')(x)

    # 初始化以baseModel的input（输入数据）为input，全连接层输出的预测回归结果为output的模型
    regr_model = Model(inputs=baseModel.input, outputs=regr_predictions)
    
    return regr_model


def train_model(model, epoch,
                train_generator, train_steps, valid_generator, valid_steps, learn_rate, trained_model_path = False):
    
    '''
    if trained_model:
        print('not complated')
    else:
    '''
    model.compile(optimizer=Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08),
            loss='mse', metrics=['mse'])
    
    if trained_model_path:
        checkpointer = ModelCheckpoint(filepath='./'+ trained_model_path,
                        monitor='mean_squared_error',verbose=1, save_best_only=True, mode='min')    
        model.fit_generator(generator=train_generator,
                            epochs=epoch,
                            steps_per_epoch=train_steps,
                            verbose=1,
                            validation_data=valid_generator,
                            validation_steps=valid_steps,
                            callbacks=[checkpointer])
    else:
        model.fit_generator(generator=train_generator,
                    epochs=epoch,
                    steps_per_epoch=train_steps,
                    verbose=1,
                    validation_data=valid_generator,
                    validation_steps=valid_steps)

def predict_model(model, test_generator, batch_size=1):

    #model = load_model('./vgg_sunshine.h5')
    ground = []
    predicted = []
    name = []
    index = []
    for elem in test_generator:
        result_elem = model.predict(elem[0], batch_size)
        # ground truth / evaluated result
        ground.append(elem[1])
        predicted.append(result_elem)
        name.append(elem[2])
        index.append(elem[3])
    ground = np.array(ground)
    predicted = np.array(predicted)
    ground = np.reshape(ground, -1)
    predicted = np.reshape(predicted, -1)
    # diff = (ground-predicted)/ground
    # output_list = [ground, predicted, diff]
    return ground, predicted, name, index

def construct_label_dict(dir_path, label_file_list, testing = False):
    
    def get_direction(x1,y1,x2,y2):
        # 1 is left eye, 2 is right eye
        diff_x = (x2-x1)
        diff_y = -(y2-y1)
        
        if diff_y >=0:
            if diff_x>0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            elif diff_x==0:
                eye_angle = 90
            else:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 180 - eye_angle
        else:
            if diff_x>0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 360 - eye_angle
            elif diff_x==0:
                eye_angle = 270
            else:   
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = eye_angle + 180
        eye_angle =  eye_angle+90
        if eye_angle>360:
            eye_angle = eye_angle-360
        return eye_angle

	#get the elems from differen dirs
    content = []	
    for file_index, file_path in enumerate(label_file_list):
        n = 0
        current_path = dir_path + file_path
        current_files =  os.listdir(current_path)
        for file in current_files:            
            if file.endswith('pos'):
                pos_path = file
        f = open(dir_path + file_path +'//'+ pos_path)
        for line in f.readlines():
    			
            name = line[0:5].replace(' ','')
            labels = line[16:-1]
            labels = labels.split('    ')
            n +=1
            if n >3:
                content.append([file_path+ '//' + name+'.png', labels, file_index])

	#process the elems and put all elems from different dirs into one 'new_content'
    new_content = []	
    for elems in content:
        error_data = 0
        new_elems = []
        new_elems.append(dir_path+ elems[0])
        new_labels = []
        num_var = 4
        for i in elems[1]:
            new_labels.append(int(i))
            num_var = num_var-1
            if num_var <= 0:
                break
		#process the labels 
        if testing:
            new_elems.append(np.mean(new_labels))
        else:
            
            x1 = new_labels[0]
            y1 = new_labels[1]
            x2 = new_labels[2]
            y2 = new_labels[3]
            if (x1!=0)&(x2 !=0):
                new_elems.append(get_direction(x1,y1,x2,y2))
            else:
                error_data = 1
		#add the dir index in elem 
        new_elems.append(elems[2])
		#avoid error data be in dataset
        if error_data!=1:
            new_content.append(new_elems)
		
    return new_content

def find_normalise_parameters(dataset):

    angel = []
    for d in dataset:
        subset_angel = []
        subset_angel = [d[i][1] for i in range(len(d))]
        angel += subset_angel
    max_angel = np.sort(np.array(angel))[-1] + 0.01
    min_angel = np.sort(np.array(angel))[0]
    return min_angel, max_angel

def restore_prediction(prediction, min_angel, max_angel):

    return [pred*(max_angel-min_angel) + min_angel for pred in prediction]

def cal_mse(ground_truth, prediction):
    return (ground_truth- prediction)/ground_truth

def save_model(model, name = 'new_vgg_model.h5'):
    model.save(name)
    

def overall_backend_function(batch_size, epoch, learn_rate, source_path, label_file_list, cropping_list, training, randomly_shuffling, trained_model, save_checkpoint):
    # construct train and valid set
    label_dict = construct_label_dict(source_path, label_file_list)

    num_data = len(label_dict)    
    # training version
    if training:
        num_train = int(np.floor(num_data*0.8))
        num_valid = int(np.floor(num_data*0.1))
        num_test = int(np.floor(num_data*0.1))
    # testing version
    else:
        num_train = int(np.floor(num_data*0))
        num_valid = int(np.floor(num_data*0))
        num_test = int(np.floor(num_data*1))
        #batch_size = 1
        
    # randomly shuffle the entire set
    if randomly_shuffling:
        random.shuffle(label_dict)
    
    # divide the dataset into train, valid and test. 
    train_dict = label_dict[0:num_train]
    valid_dict = label_dict[num_train:num_train+num_valid]
    test_dict = label_dict[num_train+num_valid:] 

    # get min and max angel as normalise parameters instead of 360
    # Attentation: you should remove test_dict!!!
    dicts = [train_dict,valid_dict, test_dict]
    min_angel, max_angel = find_normalise_parameters(dicts)
    
    
    steps_each_epoch = int(num_data / batch_size)
    #### please keep image size as imagenet dataset
    model = build_model([224, 224,3])
    # load weight
    if trained_model:
        model.load_weights(trained_model)
    
    print('load success')
    #### note: data normalization
    train_generator = data_generator(train_dict, batch_size, cropping_list, min_angel, max_angel, 224, 224)
    valid_generator = data_generator(valid_dict, batch_size, cropping_list, min_angel, max_angel, 224, 224)
    test_generator  = data_generator(test_dict,  batch_size, cropping_list, min_angel, max_angel, 224, 224, False)

    print('training start')
    if training:
        train_model(model, epoch, train_generator, steps_each_epoch, valid_generator, steps_each_epoch, learn_rate, trained_model_path = None )
    
    else:
        #### note changes
        ground, predicted, name, index = predict_model(model, test_generator, batch_size)
        ground = ground*360

        #### restore model's output
        pred_angle = restore_prediction(predicted ,min_angel, max_angel)
        #### calculate mse  for single sample, you can sum
        mse_error = cal_mse(ground, pred_angle)
        abs_error = np.abs(np.mean(np.array(ground)- np.array(pred_angle)))
        print('overall mse error:', np.mean(mse_error))
        print('overall abs error:', np.mean(abs_error))
        
        name = np.reshape(name, [-1,1])
        index = np.reshape(index, [-1,1])
        name = np.reshape(name, [-1])
        index = np.reshape(index, [-1])
        #return name, index, ground, pred_angle
        '''
        print(np.shape(name))
        print(np.shape(index))
        print(np.shape(ground))
        print(np.shape(pred_angle))
        '''
        output_pd = pandas.DataFrame(
                            {'name':name,
                             'index':index,
                             'ground truth': ground,
                             'pred_angle': pred_angle})
        try:
            output_pd.to_csv('output.csv')
        except:  
            print('file is already generated')
    if save_checkpoint:    
        
        save_model(model, save_checkpoint)    
    
    return model
    
    
if __name__ =="__main__":

    gui_main = Tk()
    gui_main.title('Orientation Prediction')
    gui_main.resizable(0, 0)
    
    class gui:
        
        # Initiate GUI by adding elementary contents
        def __init__(self, master):
            
            self.master = master
            
            # Mina frame
            self.main_frame = Frame(self.master, borderwidth = 2, relief = 'solid')
            self.main_frame.grid(row = 0, column = 0, padx = 10, pady = 10)
            
            # Column space indicator, row -= 1 if removed
            self.col_label_1 = Label(self.main_frame, text = '1', fg = 'black')
            self.col_label_1.grid(row = 0, column = 0, pady = 5, padx = 50)
            self.col_label_2 = Label(self.main_frame, text = '2', fg = 'black')
            self.col_label_2.grid(row = 0, column = 1, pady = 5, padx = 50)
            self.col_label_3 = Label(self.main_frame, text = '3', fg = 'black')
            self.col_label_3.grid(row = 0, column = 2, pady = 5, padx = 50)
            
            # Model training or image testing
            self.training_button = Button(self.main_frame, text = 'Model\nTraining', bg = 'gray80', fg = 'black', command = lambda: gui.training_button_func(self))
            self.testing_button = Button(self.main_frame, text = 'Image\nTesting', bg = 'gray80', fg = 'black', command = lambda: gui.testing_button_func(self))
            self.training_button.grid(row = 1, column = 0, pady = 5)
            self.testing_button.grid(row = 1, column = 1, pady = 5)
            
        def training_button_func(self):
            
            self.action = True
            
            self.testing_button.grid_forget()
            self.action_status = Label(self.main_frame, text = 'Training\nSelected!', fg = 'black')
            self.action_status.grid(row = 1, column = 2, pady = 5)
            
            # Number of image sets entry
            self.number_of_image_sets_entry_label = Label(self.main_frame, text = 'Total Image Sets', fg = 'black')
            self.number_of_image_sets_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.number_of_image_sets_entry_label.grid(row = 2, column = 0, pady = 5)
            self.number_of_image_sets_entry.grid(row = 2, column = 1, pady = 5)
            self.number_of_image_sets_entry_button = Button(self.main_frame, text = 'Confirm', bg = 'gray80', fg = 'black', command = lambda: gui.number_of_image_sets_entry_button_func(self))
            self.number_of_image_sets_entry_button.grid(row = 2, column = 2, pady = 5)
            
            # Image sets directory entry
            self.directory_entry_label = Label(self.main_frame, text = 'Choose Image\nSet Directories', fg = 'black')
            self.directory_entry_label.grid(row = 3, column = 0, pady = 5, padx = 5)
            self.directory_entry_dialog_button = Button(self.main_frame, text = 'Choose', bg = 'gray80', fg = 'black', command = lambda: gui.directory_entry_dialog_button_func(self))
            self.directory_entry_dialog_button.grid(row = 3, column = 1, pady = 5, padx = 5)
            self.directory_entry_button = Button(self.main_frame, text = 'Confirm', bg = 'gray80', fg = 'black', command = lambda: gui.directory_entry_button_func(self))
            self.directory_entry_button.grid(row = 3, column = 2, pady = 5)
            self.directory_entry = Text(self.main_frame, width = 40, height = 3, borderwidth = 1, relief = 'solid')
            self.directory_entry.grid(row = 4, columnspan = 3, pady = 5, padx = 5)
            
            self.batch_size_entry_label = Label(self.main_frame, text = 'Batch Size', fg = 'black')
            self.epoch_amount_entry_label = Label(self.main_frame, text = 'Epoch', fg = 'black')
            self.learning_rate_entry_label = Label(self.main_frame, text = 'Learning Rate', fg = 'black')
            self.batch_size_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.epoch_amount_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.learning_rate_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.batch_size_entry_label.grid(row = 5, column = 0, pady = 5)
            self.epoch_amount_entry_label.grid(row = 6, column = 0, pady = 5)
            self.learning_rate_entry_label.grid(row = 7, column = 0, pady = 5)
            self.batch_size_entry.grid(row = 5, column = 1, pady = 5)
            self.epoch_amount_entry.grid(row = 6, column = 1, pady = 5)
            self.learning_rate_entry.grid(row = 7, column = 1, pady = 5)
        
            self.trained_model_entry_label = Label(self.main_frame, text = 'Previous Model', fg = 'black')
            self.trained_model_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.save_model_entry_label = Label(self.main_frame, text = 'Model to be Saved', fg = 'black')
            self.save_model_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.trained_model_entry_label.grid(row = 8, column = 0, pady = 5)
            self.save_model_entry_label.grid(row = 9, column = 0, pady = 5)
            self.trained_model_entry.grid(row = 8, column = 1, pady = 5)
            self.save_model_entry.grid(row = 9, column = 1, pady = 5)
    
            self.training_proceed_button = Button(self.main_frame, text = 'Start Training', bg = 'gray80', fg = 'black', command = lambda: gui.training_proceed_button_func(self))
            self.training_proceed_button.grid(row = 10, columnspan = 3, pady = 5)
        
        def testing_button_func(self):
            
            self.action = False
            
            self.training_button.grid_forget()
            self.action_status = Label(self.main_frame, text = 'Testing\nSelected!', fg = 'black')
            self.action_status.grid(row = 1, column = 2, pady = 5)
            
            # Number of image sets entry
            self.number_of_image_sets_entry_label = Label(self.main_frame, text = 'Total Image Sets', fg = 'black')
            self.number_of_image_sets_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.number_of_image_sets_entry_label.grid(row = 2, column = 0, pady = 5)
            self.number_of_image_sets_entry.grid(row = 2, column = 1, pady = 5)
            self.number_of_image_sets_entry_button = Button(self.main_frame, text = 'Confirm', bg = 'gray80', fg = 'black', command = lambda: gui.number_of_image_sets_entry_button_func(self))
            self.number_of_image_sets_entry_button.grid(row = 2, column = 2, pady = 5)
            
            # Image sets directory entry
            self.directory_entry_label = Label(self.main_frame, text = 'Choose Image\nSet Directories', fg = 'black')
            self.directory_entry_label.grid(row = 3, column = 0, pady = 5, padx = 5)
            self.directory_entry_dialog_button = Button(self.main_frame, text = 'Choose', bg = 'gray80', fg = 'black', command = lambda: gui.directory_entry_dialog_button_func(self))
            self.directory_entry_dialog_button.grid(row = 3, column = 1, pady = 5, padx = 5)
            self.directory_entry_button = Button(self.main_frame, text = 'Confirm', bg = 'gray80', fg = 'black', command = lambda: gui.directory_entry_button_func(self))
            self.directory_entry_button.grid(row = 3, column = 2, pady = 5)
            self.directory_entry = Text(self.main_frame, width = 40, height = 3, borderwidth = 1, relief = 'solid')
            self.directory_entry.grid(row = 4, columnspan = 3, pady = 5, padx = 5)
            
            self.trained_model_entry_label = Label(self.main_frame, text = 'Previous Model', fg = 'black')
            self.trained_model_entry = Text(self.main_frame, width = 10, height = 1, borderwidth = 1, relief = 'solid')
            self.trained_model_entry_label.grid(row = 8, column = 0, pady = 5)
            self.trained_model_entry.grid(row = 8, column = 1, pady = 5)
    
            self.training_proceed_button = Button(self.main_frame, text = 'Start Testing', bg = 'gray80', fg = 'black', command = lambda: gui.testing_proceed_button_func(self))
            self.training_proceed_button.grid(row = 10, columnspan = 3, pady = 5)
    
        def training_proceed_button_func(self):
    
            # Parameter
            self.number_of_image_sets = int(self.number_of_image_sets_entry.get('1.0', 'end'))
            self.batch_size = int(self.batch_size_entry.get('1.0', '1.end'))
            self.epoch_amount = int(self.epoch_amount_entry.get('1.0', '1.end'))
            self.learning_rate = float(self.learning_rate_entry.get('1.0', '1.end'))
            self.trained_model = str(self.trained_model_entry.get('1.0', '1.end'))
            self.save_model = str(self.save_model_entry.get('1.0', '1.end'))
            
            cropping_1 = [240,440,550,750]
            cropping_2 = [410,610,505,705]
            cropping_3 = [360,580,510,730]
            cropping_list = [cropping_1, cropping_2, cropping_3]
            
            new_model = overall_backend_function(batch_size = self.batch_size, epoch = self.epoch_amount, learn_rate = self.learning_rate,
                                 source_path = self.root_directory, label_file_list = self.directory_list,
                                 cropping_list = cropping_list, training = self.action, 
                                 randomly_shuffling = self.action, trained_model = self.trained_model,
                                 save_checkpoint = self.save_model)
            
        def testing_proceed_button_func(self):
    
            # Parameter
            self.number_of_image_sets = int(self.number_of_image_sets_entry.get('1.0', 'end'))
            self.trained_model = str(self.trained_model_entry.get('1.0', '1.end'))        
            
            cropping_1 = [240,440,550,750]
            cropping_2 = [410,610,505,705]
            cropping_3 = [360,580,510,730]
            cropping_list = [cropping_1, cropping_2, cropping_3]
            
            new_model = overall_backend_function(batch_size = 1, epoch = 1, learn_rate = 1,
                                 source_path = self.root_directory, label_file_list = self.directory_list,
                                 cropping_list = cropping_list, training = self.action, 
                                 randomly_shuffling = self.action, trained_model = self.trained_model,
                                 save_checkpoint = 'empty.h5')
            
        def number_of_image_sets_entry_button_func(self):
            
            self.number_of_image_sets = int(self.number_of_image_sets_entry.get('1.0', 'end'))
        
        def directory_entry_dialog_button_func(self):
            directory = filedialog.askdirectory()
            self.directory_entry.insert(END, directory + '\n')
            
        def directory_entry_button_func(self):
            self.raw_directory_list = []
            line_count = 1
            while line_count <= self.number_of_image_sets:
                self.raw_directory_list.append(self.directory_entry.get(('%d.0') % (line_count), '%d.end' % (line_count)))
                line_count += 1
            self.root_directory = self.directory_entry.get(('%d.0') % (line_count), '%d.end' % (line_count))
            root_directory_length = len(self.root_directory)
            self.directory_list = []
            for el in self.raw_directory_list:
                el = el[root_directory_length : len(el)]
                self.directory_list.append(el)
            print(self.directory_list)
            
    gui(gui_main)
    gui_main.mainloop()
