from keras.callbacks import ModelCheckpoint, Callback, EarlyStopping
from keras.optimizers import Adam
import numpy as np
import cv2
from keras.layers import Flatten, Dropout, Dense
from keras.applications.vgg16 import VGG16
from keras.models import Model, load_model
import random
import tensorflow as tf
import keras.backend.tensorflow_backend as K
K.set_session(tf.Session(config=tf.ConfigProto(device_count={'gpu':0})))
import os
import imutils
import pandas 

# We create a layer which take as input movies of shape
# (n_frames, width, height, channels) and returns a movie
# of identical shape.
K.set_image_dim_ordering('tf')


def data_generator(label_dict, batch_size, cropping_list, min_angel, max_angel,
                   size_x=None, size_y=None, training=True, angle_range=1):
    
    #set the parameter of images
    # warning: not interface of changing these parameters
    x_centre = 1280/2
    y_centre = 720/2
    
    num_dataset = len(label_dict)
    steps_each_epoch = int(num_dataset / batch_size)
    # Training generator
    if training:
        while 1:  
            for i in range(steps_each_epoch):
                #start load data in batch 
                mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
                X = []
                Y = []
                for image_pair in mini_batch:
                    #image_pair[0]: name 
                    #image_pair[1]: label
                    image_path = image_pair[0]
                    #print(image_pair)
                    #cropping index
                    cropping = cropping_list[image_pair[2]]
                    image = cv2.imread(image_path)
                    ##randomly rotate the image
                    # here range is -15 to 15 degree
                    rand_angle = angle_range
                    rand_angle = np.random.randint(rand_angle)
                    rand_angle = rand_angle - (rand_angle/2)
                    # rotate the image
                    image = imutils.rotate(image, rand_angle)
                    #get the center and half side length of cropping area
                    x_point = np.mean([cropping[2],cropping[3]])
                    y_point = np.mean([cropping[0],cropping[1]])
                    side = int(np.abs(x_point - cropping[2]))
                    # relevant placement vs centre
                    xx = (x_point - x_centre)
                    yy = (y_point - y_centre)
                    # get distance between centre and image centre
                    rr = (xx**2+yy**2)**0.5
                    # get abs position
                    if xx > 0:
                        if yy > 0:
                            hori_angle = np.arctan(-yy/xx) - rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = np.arctan(-yy/xx) + rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    if xx < 0:
                        if yy > 0:
                            hori_angle = np.arctan(-yy/xx) + rand_angle*np.pi/180 + np.pi
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = np.pi - np.arctan(-yy/xx) + rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    if xx == 0:
                        if yy == 0:
                            x_2 = x_point
                            y_2 = y_point
                        if yy > 0:
                            hori_angle = -(90-rand_angle)*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = (90+rand_angle)*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    if yy == 0:
                        if xx > 0:
                            hori_angle = rand_angle*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                        if yy < 0:
                            hori_angle = (180+rand_angle)*np.pi/180
                            x_2 = int(np.cos(hori_angle)*rr+x_centre)
                            y_2 = int(y_centre-np.sin(hori_angle)*rr)
                    
                    #image cropping
                    image = image[y_2-side:y_2+side, x_2-side:x_2+side,:]
                    #### keep image normalization please
                    image = np.divide(image, 255.0)
                    image = np.subtract(image, 0.5)
                    image = np.multiply(image, 2.0)
                    if (size_x!=None) & (size_y!=None):
                        image = cv2.resize(image, (size_x, size_y))
                    #x,y,z = np.shape(image)
                    #image = np.reshape(image,(x,y,1))
                    #### data normalization please
                    label = image_pair[1]
                    label = label + rand_angle
                    if label>360:
                        label = label-360
                    if label<0:
                        label = label+360
                    label = (label - min_angel)/ (max_angel-min_angel)
                    #print('label',label)
                    X.append(image)
                    Y.append(label)
                X = np.array(X)
                Y = np.array(Y)
                yield (X,Y)
    # Test generator
    else:
        for i in range(steps_each_epoch):
            #start load data in batch 
            mini_batch = label_dict[i * batch_size: (i + 1) * batch_size]
            X = []
            Y = []
            Z = []
            index = []
            for image_pair in mini_batch:
                #image_pair[0]: name 
                #image_pair[1]: label
                image_path = image_pair[0]
                #cropping index
                cropping = cropping_list[image_pair[2]]
                image = cv2.imread(image_path)
                image = image[cropping[0]:cropping[1],cropping[2]:cropping[3],:]
                #### keep image normalization please
                image = np.divide(image, 255.0)
                image = np.subtract(image, 0.5)
                image = np.multiply(image, 2.0)
                if (size_x!=None) & (size_y!=None):
                    image = cv2.resize(image, (size_x, size_y))
                #x,y,z = np.shape(image)
                #image = np.reshape(image,(x,y,1))
                #### data normalization please
                label = image_pair[1]
                label = (label - min_angel)/ (max_angel-min_angel)
                X.append(image)
                Y.append(label)
                Z.append(image_path)
                index.append(image_pair[2])
            X = np.array(X)
            Y = np.array(Y)
            yield (X,Y,Z,index)

def build_model(input_shape,regr_num=1):

    # create model
    baseModel = VGG16(include_top=False, weights=None, input_shape=input_shape)
    # load weights
    baseModel.load_weights('./vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',by_name=True)
    # transfer the last layer
    x = baseModel.output
    x = Flatten()(x)
    regr_predictions = Dense(regr_num, kernel_initializer='normal', activation='sigmoid', name='fc_regr')(x)
    regr_model = Model(inputs=baseModel.input, outputs=regr_predictions)
    
    return regr_model


def train_model(model, epoch,
                train_generator, train_steps, valid_generator, valid_steps, learn_rate, trained_model_path = False):
    
    '''
    if trained_model:
        print('not complated')
    else:
    '''
    model.compile(optimizer=Adam(lr=learn_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08),
            loss='mse', metrics=['mse'])
    
    if trained_model_path:
        checkpointer = ModelCheckpoint(filepath='./'+ trained_model_path,
                        monitor='mean_squared_error',verbose=1, save_best_only=True, mode='min')    
        model.fit_generator(generator=train_generator,
                            epochs=epoch,
                            steps_per_epoch=train_steps,
                            verbose=1,
                            validation_data=valid_generator,
                            validation_steps=valid_steps,
                            callbacks=[checkpointer])
    else:
        model.fit_generator(generator=train_generator,
                    epochs=epoch,
                    steps_per_epoch=train_steps,
                    verbose=1,
                    validation_data=valid_generator,
                    validation_steps=valid_steps)

def predict_model(model, test_generator, batch_size=1):

    #model = load_model('./vgg_sunshine.h5')
    ground = []
    predicted = []
    name = []
    index = []
    for elem in test_generator:
        result_elem = model.predict(elem[0], batch_size)
        # ground truth / evaluated result
        ground.append(elem[1])
        predicted.append(result_elem)
        name.append(elem[2])
        index.append(elem[3])
    ground = np.array(ground)
    predicted = np.array(predicted)
    ground = np.reshape(ground, -1)
    predicted = np.reshape(predicted, -1)
    print(predicted)
    return ground, predicted, name, index

def get_direction_1(x1,y1,x2,y2):
    # 1 is left eye, 2 is right eye
    diff_x = (x2-x1)
    diff_y = -(y2-y1)
    
    if diff_y >=0:
        if diff_x>0:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
        elif diff_x==0:
            eye_angle = 90
        else:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 180 - eye_angle
    else:
        if diff_x>0:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 360 - eye_angle
        elif diff_x==0:
            eye_angle = 270
        else:   
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = eye_angle + 180
    eye_angle =  eye_angle+90
    if eye_angle>360:
        eye_angle = eye_angle-360
    return eye_angle

def get_direction_2(x1,y1,x2,y2,x3,y3, r = 12):
    # 1 is left eye, 2 is right eye, 3 is abdomen
    # r stands for the length of head
    diff_x = x2-x1
    diff_y = y2-y1          
        
    mid_x = np.mean([x1,x2])
    mid_y = np.mean([y1,y2])
    
    factor = np.sqrt((r**2)/(diff_x**2+diff_y**2))
    
    x_move = diff_y*factor
    y_move = diff_x*factor
    #get the position of neck
    if diff_x >0:
        new_y = int(mid_y - y_move)
    elif diff_x <0:
        new_y = int(mid_y + y_move)
    else:
        new_y = int(mid_y) 
        
    if diff_y >0:
        new_x = int(mid_x - x_move)
    elif diff_y <0:
        new_x = int(mid_x + x_move)
    else:
        new_x = int(mid_x) 
    #get the orientation of body
    body_diff_x = new_x-x3
    body_diff_y = -(new_y-y3)
    #turn the direction into angle
    if body_diff_y >=0:
        if body_diff_x>0:
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
        elif body_diff_x==0:
            eye_angle = 90
        else:
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 180 - eye_angle
    else:
        if body_diff_x>0:
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 360 - eye_angle
        elif body_diff_x==0:
            eye_angle = 270
        else:   
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = eye_angle + 180 
    if eye_angle>360:
        eye_angle = eye_angle-360
    return eye_angle



def construct_label_dict(dir_path, label_file_list, testing = False, target = 'head', r =12):
    print('get target:',target)
    # target gives the predict target of this dict, 0 represents head, 1 represents body

	#get the elems from differen dirs
    content = []	
    for file_index, file_path in enumerate(label_file_list):
        n = 0
        current_path = dir_path + file_path
        current_files =  os.listdir(current_path)
        for file in current_files:            
            if file.endswith('pos'):
                pos_path = file
        f = open(dir_path + file_path +'//'+ pos_path)
        for line in f.readlines():
    			
            name = line[0:5].replace(' ','')
            labels = line[16:-1]
            labels = labels.split('    ')
            n +=1
            if n >3:
                content.append([file_path+ '//' + name+'.png', labels, file_index])

	#process the elems and put all elems from different dirs into one 'new_content'
    new_content = []	
    for elems in content:
        if target == 'head':
            error_data = 0
            new_elems = []
            new_elems.append(dir_path+ elems[0])
            new_labels = []   
    		#process the labels 
            if testing:
                new_elems.append(np.mean(new_labels))
            else:
                num_var = 4
                for i in elems[1]:
                    new_labels.append(int(i))
                    num_var = num_var-1
                    if num_var <= 0:
                        break
                #left eye,0:x, 1:y
                #right : 2：x, 3:y
                x1 = new_labels[0]
                y1 = new_labels[1]
                x2 = new_labels[2]
                y2 = new_labels[3]
                if (x1!=0)&(x2 !=0):
                    new_elems.append(get_direction_1(x1,y1,x2,y2))
                else:
                    error_data = 1
    		 #add the dir index in elem 
            new_elems.append(elems[2])
    		 #avoid error data be in dataset
            if error_data!=1:
                new_content.append(new_elems)
        elif target == 'body':
            error_data = 0
            new_elems = []
            new_elems.append(dir_path+ elems[0])
            new_labels = [] 
    		#process the labels 
            if testing:
                new_elems.append(np.mean(new_labels))
            else:
                num_var = 6
                for i in elems[1]:
                    new_labels.append(int(i))
                    num_var = num_var-1
                    if num_var <= 0:
                        break
                #left eye,0:x, 1:y
                #right : 2：x, 3:y
                x1 = new_labels[0]
                y1 = new_labels[1]
                x2 = new_labels[2]
                y2 = new_labels[3]
                x3 = new_labels[4]
                y3 = new_labels[5]
                if (x1!=0)&(x2!=0)&(x3!=0):
                    new_elems.append(get_direction_2(x1,y1,x2,y2,x3,y3,r))
                else:
                    error_data = 1
    		 #add the dir index in elem 
            new_elems.append(elems[2])
    		 #avoid error data be in dataset
            if error_data!=1:
                new_content.append(new_elems)
        else:
            print('receive wrong target ')
            print(1/0)
    return new_content

def find_normalise_parameters(dataset):

    angel = []
    for d in dataset:
        subset_angel = []
        subset_angel = [d[i][1] for i in range(len(d))]
        angel += subset_angel
    max_angel = np.sort(np.array(angel))[-1] + 0.01
    min_angel = np.sort(np.array(angel))[0]
    return min_angel, max_angel

def restore_prediction(prediction, min_angel, max_angel):

    return [pred*(max_angel-min_angel) + min_angel for pred in prediction]

def cal_mse(ground_truth, prediction):
    return (ground_truth- prediction)/ground_truth

def save_model(model, name = 'new_vgg_model.h5'):
    model.save(name)
    

def overall_backend_function(batch_size, epoch, learn_rate, source_path,
                             label_file_list, cropping_list, training,
                             randomly_shuffling, trained_model,
                             save_checkpoint, target, r, test_case):
    # get the flag of testing if not training
    if training:
        testing = False
    else:
        testing = True
     # construct train and valid set
    label_dict = construct_label_dict(data_path, label_file_list, testing, target, r)
        
    num_data = len(label_dict)    
    # training model
    if training:
        num_train = int(np.floor(num_data*0.8))
        num_valid = int(np.floor(num_data*0.1))
        num_test = int(np.floor(num_data*0.1))
    # testing model
    else:
        num_train = int(np.floor(num_data*0))
        num_valid = int(np.floor(num_data*0))
        num_test = int(np.floor(num_data*1))
    
    if test_case:
        num_train = int(1)
        num_valid = int(1)
        num_test = int(1)
    
        
    # randomly shuffle the entire setd
    if randomly_shuffling:
        random.shuffle(label_dict)
    
    # divide the dataset into train, valid and test. 
    train_dict = label_dict[0:num_train]
    valid_dict = label_dict[num_train:num_train+num_valid]
    test_dict = label_dict[num_train+num_valid:] 

    # get min and max angel as normalise parameters instead of 360
    # Attentation: you should remove test_dict!!!
    dicts = [train_dict,valid_dict, test_dict]
    min_angel, max_angel = find_normalise_parameters(dicts)
    
    
    steps_each_epoch = int(num_data / batch_size)
    #### please keep image size as imagenet dataset
    model = build_model([224, 224,3])
    # load weight
    if trained_model:
        model.load_weights(trained_model)
    
    print('load success')
    #### note: data normalization
    train_generator = data_generator(train_dict, batch_size, cropping_list, min_angel, max_angel, 224, 224)
    valid_generator = data_generator(valid_dict, batch_size, cropping_list, min_angel, max_angel, 224, 224)
    test_generator  = data_generator(test_dict,  batch_size, cropping_list, min_angel, max_angel, 224, 224, False)
    
    #initialize the pred_angle
    pred_angle = None
    
    print('training start')
    if training:
        train_model(model, epoch, train_generator, steps_each_epoch, valid_generator, steps_each_epoch, learn_rate, trained_model_path = None )
    
    else:
        #### note changes
        ground, predicted, name, index = predict_model(model, test_generator, batch_size)
        ground = ground*360
        pred_angle = predicted*360
        #### restore model's output
        print('-=--==--=-=')
        print(pred_angle)
        #pred_angle = restore_prediction(predicted ,min_angel, max_angel)
        
        #### calculate mse  for single sample, you can sum
        mse_error = cal_mse(ground, pred_angle)
        abs_error = np.abs(np.mean(np.array(ground)- np.array(pred_angle)))
        print('overall mse error:', np.mean(mse_error))
        print('overall abs error:', np.mean(abs_error))
        
        name = np.reshape(name, [-1,1])
        index = np.reshape(index, [-1,1])
        name = np.reshape(name, [-1])
        index = np.reshape(index, [-1])
        #return name, index, ground, pred_angle
        output_pd = pandas.DataFrame(
                            {'name':name,
                             'index':index,
                             'ground truth': ground,
                             'pred_angle': pred_angle})
        try:
            output_pd.to_csv('output.csv')
        except:  
            print('file is already generated')
        
    if save_checkpoint:    
        if training:
            save_model(model, save_checkpoint)    
    
    return model, pred_angle
    
def check_img_or_video(path):
    file_list = os.listdir(path)
    for file in file_list:
        if file.endswith('png'):
            return True
        elif file.endswith('mp4'):
            return False
        else:
            raise Exception('the file contains unexpected file:', file)
        
def change_img_name(path):
    file_list = os.listdir(path)
    #check whether it is converted
    test_file = None
    for file in file_list:
        if file.endswith('png'):
            #acquire a file ends with .png
            test_file = file    
    
    if test_file==None:
        raise Exception('current path contains no images')
    
    if test_file[0] in '1234567890':
        print('the images in this file have been covnerted')
    else:
        for file in file_list:
            if (not file.endswith('pos')) and (file.endswith('png')):
                name = file[1:]
                name = int(name)
                name = name-1
                name = str(name)+'.png'
                os.rename(path+'//'+file, path+'//'+name)
                
def video_to_image(path, n = 1):
    # requires images with a gap of n seconds
    # for example, read one image per 10 seconds
    file_list = os.listdir(path)
    count = 0
    name = 0
    for file in file_list:
        if file.endswith('mp4'):
            vidcap = cv2.VideoCapture(path+'//'+file)
            fps = vidcap.get(5)
            # the gap of frames defined as fps*n
            read_period = fps*n
            print(read_period)
            success = True
            while success:
                success,image = vidcap.read()
                if count % read_period == 0: 
                    cv2.imwrite(path + '//' + str(name)+'.png', image)
                    name = name + 1
                count = count + 1
                
def pos_create(path):
    count = 0
    files = os.listdir(path)
    # count the number of images in path
    for file in files:
        if file.endswith('png'):
            count = count + 1
    
    pos_file = open(path+'//temp.pos','w')
    #set the title of pos file
    pos_file.write('This is a position co-ordinate file created by Tracking Ant\n')
    pos_file.write('no title for this temp pose file\n')
    pos_file.write('Frame (please ignore the following data) \n')
    index = 0
    
    while count > 0:
        # set the index of images
        current_index = str(index)
        while len(current_index)<15:
            current_index = current_index + ' '
        pos_file.write(current_index+'\n')
        # set the fake position of images
        index = index + 1
        count = count - 1
    pos_file.close()

def check_pos_exist(path):
    files = os.listdir(path)
    for file in files:
        if file.endswith('pos'):
            return True
    return False



if __name__ =="__main__":
    ## parameter setting------------------------------------------------------
    data_path = r'C:\Users\xut\Desktop\shu_liu\mixed_data'
    ''
    label_file_1 = r'\west_data'
    label_file_2 = r'\south_data'
    label_file_3 = r'\vr_version'
    label_file_list = [label_file_1, label_file_2, label_file_3]
    target = 'head'
    length = 12	
    
    epoch = 100
    learning_rate = 0.000001
    trained_model = 'head_6.h5'
    save_checkpoint = 'head_1.h5'
    
    training = False
    # it is the parameter for video
    time_period_between_frames = 100

    ## preparing period-------------------------------------------------------
    #convert the all source file
    for file_name in label_file_list:
        path = data_path + file_name
        flag = check_img_or_video(path)
        # if it is images
        if flag:
            change_img_name(path)
        # if it is video
        else:
            video_to_image(path, n=time_period_between_frames)
            
        flag = check_pos_exist(path)
        # if it contains pos file
        if flag:
            pass
        # if it contains no pos file
        else:
            pos_create(path)
    # check and create pos file if requires

       
    ## testing period---------------------------------------------------------
    #only for test whether the framework is working
    test_case = False
    
    ## running period---------------------------------------------------------
    cropping_1 = [240,440,550,750]
    cropping_2 = [410,610,505,705]
    cropping_3 = [360,580,510,730]
    cropping_list = [cropping_1, cropping_2, cropping_3]

    #shuffling will only happened in training mode
    randomly_shuffling = training 
    
    model, pred_angle = overall_backend_function(batch_size = 2, epoch = epoch, learn_rate = learning_rate,
                             source_path = data_path, label_file_list = label_file_list,
                             cropping_list = cropping_list, training = training, 
                             randomly_shuffling = randomly_shuffling, trained_model = trained_model,
                             save_checkpoint = save_checkpoint, target=target, r=length, test_case = test_case)

    ## post-process of the result---------------------------------------------
    # if the training is False, user has the option to convert the output to be gif
    
    '''
    question 2:
    save as gif
    '''
    