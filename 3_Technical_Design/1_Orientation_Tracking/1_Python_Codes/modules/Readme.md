## Back-up modules



### centre_img_rotation.py:

It is the test function for rotating the images along with the cropping box.


### demo_creator.py:

It is the integrated function for gif generator, which matches the main function with version 5.

### gif.py:

It is the test function for python-version gif generator.

### gif_2.1+.py:

It is the integrated function for gif generator, which matches the main function with version 8.

### image_showing.py:

It is the test function for showing image.

### img_pre.py:

It is the test function for a preview of a series of images.

### load.py:
It is the test function for loading images.