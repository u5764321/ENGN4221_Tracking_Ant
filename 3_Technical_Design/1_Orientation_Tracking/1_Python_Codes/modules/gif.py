# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 04:02:08 2018

@author: xut
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Aug 25 22:52:53 2018

@author: xut
"""

import cv2 
import numpy as np
import imageio
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import os

def construct_label_dict(dir_path, label_file, testing = False):
    
    def get_direction(x1,y1,x2,y2):
        # 1 is left eye, 2 is right eye
        diff_x = (x2-x1)
        diff_y = -(y2-y1)
        
        if diff_y >=0:
            if diff_x>0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            elif diff_x==0:
                eye_angle = 90
            else:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 180 - eye_angle
        else:
            if diff_x>0:
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = 360 - eye_angle
            elif diff_x==0:
                eye_angle = 270
            else:   
                eyes_vector = np.abs((y2-y1)/(x2-x1))
                eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
                eye_angle = eye_angle + 180
        eye_angle =  eye_angle+90
        if eye_angle>360:
            eye_angle = eye_angle-360
        return eye_angle

    f = open(dir_path+label_file)
    n = 0
    content = []
    for line in f.readlines():
        
        name = line[0:5].replace(' ','')
        labels = line[16:-1]
        labels = labels.split('    ')
        content.append([name+'.png', labels])
        n +=1
    
    content = content[3:]
    new_content = []
    for elems in content:
    
        error_data = 0
        new_elems = []
        new_elems.append(elems[0])
        new_labels = []
        num_var = 4
        for i in elems[1]:
            new_labels.append(int(i))
            num_var = num_var-1
            if num_var <= 0:
                break
        if testing:
            new_elems.append(new_labels)
        else:
            
            x1 = new_labels[0]
            y1 = new_labels[1]
            x2 = new_labels[2]
            y2 = new_labels[3]
            if (x1!=0)&(x2 !=0):
                new_elems.append(get_direction(x1,y1,x2,y2))
            else:
                error_data = 1
                new_elems = 1
        if error_data!=1:
            new_content.append(new_elems)
    return new_content

def create_gif(gif_name, path, duration = 0.3):
    '''
    生成gif文件，原始图片仅支持png格式
    gif_name ： 字符串，所生成的 gif 文件名，带 .gif 后缀
    path :      需要合成为 gif 的图片所在路径
    duration :  gif 图像时间间隔
    '''

    frames = []
    pngFiles = os.listdir(path)
    image_list = [os.path.join(path, f) for f in pngFiles]
    for image_index in list(np.arange(1017)):
        image_name = path +'//' +str(image_index) + '.png'
    #for image_name in image_list:
        # 读取 png 图像文件
        frames.append(imageio.imread(image_name))
    # 保存为 gif 
    imageio.mimsave(gif_name, frames, 'GIF', duration = duration)
    return

def get_start_point(point_dict):
    mid_dict = []
    for line in point_dict:
        img = line[0]
        point1 = line[1][0:2]
        point2 = line[1][2:4]
        point1 = np.array(point1)
        point2 = np.array(point2)
        mid_point_x = np.mean([point1[0],point2[0]]) 
        mid_point_y = np.mean([point1[1],point2[1]])
        mid_point = [mid_point_x, mid_point_y]
        mid_dict.append([img, mid_point])
    return mid_dict

def degree_to_rad(angle):
    return (angle/360)*2*np.pi

def line_construct_truth(start_point, rate, index):
    x = start_point[0]
    y = start_point[1]
    list_x = []
    list_y = []
    
    if rate>0:
        if 0<=label_dict[index][1]<=90:
            print(label_dict[index][1])
            print('0-90',x,y,rate)
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y - np.abs(rate)
                list_x.append(x)
                list_y.append(y)
        else:
            print(label_dict[index][1])
            print('180-270',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    else:
        if 90<=label_dict[index][1]<=180:
            print(label_dict[index][1])
            print('90-180',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y - np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
        else:
            print(label_dict[index][1])
            print('270-360',x,y,rate)   
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    return list_x, list_y

def line_construct_pred(start_point, rate, index):
    x = start_point[0]
    y = start_point[1]
    list_x = []
    list_y = []
    if rate>0:
        if 0<=pred_angle[index]<=90:
            print(pred_angle[index])
            print('0-90',x,y,rate)
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y - np.abs(rate)
                list_x.append(x)
                list_y.append(y)
        else:
            print(pred_angle[index])
            print('180-270',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    else:
        if 90<=pred_angle[index]<=180:
            print(pred_angle[index])
            print('90-180',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y - np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
        else:
            print(pred_angle[index])
            print('270-360',x,y,rate)
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    return list_x, list_y

def draw_line_img(img, list_x, list_y, mark=0, size = [2,1]):
    for index in range(len(list_x)):
        y = int(list_x[index])
        x = int(list_y[index])
        # b g r
        if mark ==0:
        # mark green
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],0] = 0 
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],1] = 255
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],2] = 0
        else:
        # red green
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],0] = 0 
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],1] = 0
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],2] = 255
    return img
            
def img_construct(path, mid_dict, pred_angle, label_dict, display = 0, save = 0):
    count = 0
    save_path = r'C:\Users\xut\Desktop\test'
    for index in range(len(label_dict)):
        
        # name
        img = label_dict[index] 
        img_name = img[0]
        print('name: ',img[0])
        print('path: ',path+ '//'+img_name)
        image = cv2.imread(path+ '//'+img_name)
        #get mid_point
        mid_point = mid_dict[index][1]
        #get radian
        truth_rad = degree_to_rad(label_dict[index][1])
        pred_rad = degree_to_rad(pred_angle[index])
        #get changing rating
        truth_rate = np.tan(truth_rad)
        pred_rate = np.tan(pred_rad)
        #get the tracks of line
        truth_line_x,truth_line_y= line_construct_truth(mid_point, truth_rate, index)
        pred_line_x,pred_line_y = line_construct_pred(mid_point, pred_rate, index)
        # start to draw line on img
        new_img = draw_line_img(image, truth_line_x, truth_line_y, 0)
        new_img = draw_line_img(new_img, pred_line_x, pred_line_y, 1)

        if display:
            cv2.imshow("Image", new_img) 
            cv2.waitKey (0)
            cv2.destroyAllWindows()
        if save:
            cv2.imwrite(save_path+'//'+str(count)+'.png', new_img)
        count = count + 1

# construct the separate imgs
data_path = './Ant01_West_2018-03-29'
label_file = '/Ant01_West_Full.pos'
label_dict = construct_label_dict(data_path, label_file)    
point_dict = construct_label_dict(data_path, label_file, True)
mid_dict = get_start_point(point_dict)
path = r'C:\Users\xut\Desktop\testing_vgg\Ant01_West_2018-03-29'
img_list = img_construct(path, mid_dict, pred_angle, label_dict, save=1)

#construct the gif
source_path = r'D:\test'
#g_path = r'C:\Users\xut\Desktop\testing_vgg'
create_gif(r'test.gif', source_path, duration=0.3)
