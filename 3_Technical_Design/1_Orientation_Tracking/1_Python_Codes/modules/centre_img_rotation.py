# -*- coding: utf-8 -*-
"""
Created on Fri Sep 21 19:27:49 2018

@author: xut
"""
import imutils
import numpy as np
import cv2
import os


path = r'C:\Users\xut\Desktop\shu_liu\mixed_data\south_data'
path_content = os.listdir(path)

theta = 300
theta_neg = - theta



img = cv2.imread(r'C:\Users\xut\Desktop\shu_liu\mixed_data\west_data\0.png')

img1 =imutils.rotate(img, theta)

y_centre = int(720/2)
x_centre = int(1280/2)

#400,600,510,710
#240,440,550,750
#360,580,510,730
x_point = 900
#650
y_point = 470
#340
r = 5

xx = (x_point - x_centre)
yy = (y_point - y_centre)

rr = (xx**2+yy**2)**0.5
print(rr)

if xx > 0:
    if yy > 0:
        hori_angle = np.arctan(-yy/xx) - theta_neg*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
    if yy < 0:
        hori_angle = np.arctan(-yy/xx) + theta_neg*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
if xx < 0:
    if yy > 0:
        hori_angle = np.arctan(-yy/xx) + theta_neg*np.pi/180 + np.pi
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
    if yy < 0:
        hori_angle = np.pi - np.arctan(-yy/xx) + theta_neg*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
if xx == 0:
    if yy == 0:
        x_2 = x_point
        y_2 = y_point
    if yy > 0:
        hori_angle = -(90-theta_neg)*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
    if yy < 0:
        hori_angle = (90+theta_neg)*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
if yy == 0:
    if xx > 0:
        hori_angle = theta_neg*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
    if yy < 0:
        hori_angle = (180+theta_neg)*np.pi/180
        x_2 = int(np.cos(hori_angle)*rr+x_centre)
        y_2 = int(y_centre-np.sin(hori_angle)*rr)
        
        

img_0 = img1


img_0[y_point-r:y_point+r,x_point-r:x_point+r, 2] = 0
img_0[y_point-r:y_point+r,x_point-r:x_point+r, 0] = 0
img_0[y_point-r:y_point+r,x_point-r:x_point+r, 1] = 255

img_0[y_centre-r:y_centre+r,x_centre-r:x_centre+r, 2] = 0
img_0[y_centre-r:y_centre+r,x_centre-r:x_centre+r, 0] = 255
img_0[y_centre-r:y_centre+r,x_centre-r:x_centre+r, 1] = 0

img_0[y_2-r:y_2+r,x_2-r:x_2+r, 2] = 255
img_0[y_2-r:y_2+r,x_2-r:x_2+r, 0] = 0
img_0[y_2-r:y_2+r,x_2-r:x_2+r, 1] = 0
cv2.imshow('image',img_0)
cv2.waitKey(0)
cv2.destroyAllWindows()

'''
r = 100
img_0 = img_0[y_2-r:y_2+r,x_2-r:x_2+r, :]
cv2.imshow('image',img_0)
cv2.waitKey(0)
cv2.destroyAllWindows()



img_0 = img

img_0[y-r:y+r,x-r:x+r, 2] = 255
img_0[y-r:y+r,x-r:x+r, 0] = 0
img_0[y-r:y+r,x-r:x+r, 1] = 0
cv2.imshow('image',img_0)
cv2.waitKey(0)
cv2.destroyAllWindows()
'''