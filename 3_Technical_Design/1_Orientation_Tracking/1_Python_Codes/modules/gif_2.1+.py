# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 04:02:08 2018

@author: xut
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Aug 25 22:52:53 2018

@author: xut
"""

import cv2 
import numpy as np
import imageio
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import os

def get_direction_1(x1,y1,x2,y2):
    # 1 is left eye, 2 is right eye
    diff_x = (x2-x1)
    diff_y = -(y2-y1)
    
    if diff_y >=0:
        if diff_x>0:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
        elif diff_x==0:
            eye_angle = 90
        else:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 180 - eye_angle
    else:
        if diff_x>0:
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 360 - eye_angle
        elif diff_x==0:
            eye_angle = 270
        else:   
            eyes_vector = np.abs((y2-y1)/(x2-x1))
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = eye_angle + 180
    eye_angle =  eye_angle+90
    if eye_angle>360:
        eye_angle = eye_angle-360
    return eye_angle









def get_direction_2(x1,y1,x2,y2,x3,y3, r = 12):
    # 1 is left eye, 2 is right eye, 3 is abdomen
    # r stands for the length of head
    diff_x = x2-x1
    diff_y = y2-y1          
        
    mid_x = np.mean([x1,x2])
    mid_y = np.mean([y1,y2])
    
    factor = np.sqrt((r**2)/(diff_x**2+diff_y**2))
    
    x_move = diff_y*factor
    y_move = diff_x*factor
    #get the position of neck
    
    if diff_x > 0:
        if diff_y > 0:
            new_x = int(mid_x - x_move)
            new_y = int(mid_y + y_move)
        if diff_y < 0:
            new_x = int(mid_x - x_move)
            new_y = int(mid_y + y_move)
        if diff_y == 0:
            new_x = int(mid_x)
            new_y = int(mid_y + y_move)
    if diff_x < 0:
        if diff_y > 0:
            new_x = int(mid_x - x_move)
            new_y = int(mid_y + y_move)
        if diff_y < 0:
            new_x = int(mid_x - x_move)
            new_y = int(mid_y + y_move)
        if diff_y == 0:
            new_x = int(mid_x)
            new_y = int(mid_y + y_move)
    if diff_x == 0:
        if diff_y > 0:
            new_x = int(mid_x - x_move)
            new_y = int(mid_y)
        if diff_y < 0:
            new_x = int(mid_x - x_move)
            new_y = int(mid_y)

    #get the orientation of body
    body_diff_x = new_x-x3
    body_diff_y = -(new_y-y3)
    #turn the direction into angle
    if body_diff_y >=0:
        if body_diff_x>0:
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
        elif body_diff_x==0:
            eye_angle = 90
        else:
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 180 - eye_angle
    else:
        if body_diff_x>0:
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = 360 - eye_angle
        elif body_diff_x==0:
            eye_angle = 270
        else:   
            eyes_vector = np.abs(body_diff_y/body_diff_x)
            eye_angle = np.arctan(eyes_vector)/(2*np.pi)*360
            eye_angle = eye_angle + 180 
    if eye_angle>360:
        eye_angle = eye_angle-360
    return eye_angle



def construct_label_dict(dir_path, label_file_list, testing = False, target = 'head', r =12):
    print('get target:',target)
    # target gives the predict target of this dict, 0 represents head, 1 represents body

	#get the elems from differen dirs
    content = []	
    for file_index, file_path in enumerate(label_file_list):
        n = 0
        current_path = dir_path + file_path
        current_files =  os.listdir(current_path)
        for file in current_files:            
            if file.endswith('pos'):
                pos_path = file
        f = open(dir_path + file_path +'//'+ pos_path)
        for line in f.readlines():
    			
            name = line[0:5].replace(' ','')
            labels = line[16:-1]
            labels = labels.split('    ')
            n +=1
            if n >3:
                content.append([file_path+ '//' + name+'.png', labels, file_index])

	#process the elems and put all elems from different dirs into one 'new_content'
    new_content = []	
    for elems in content:
        if target == 'head':
            error_data = 0
            new_elems = []
            new_elems.append(dir_path+ elems[0])
            new_labels = []
            num_var = 4
            for i in elems[1]:
                new_labels.append(int(i))
                num_var = num_var-1
                if num_var <= 0:
                    break
    		 #process the labels 
            if testing:
                new_elems.append(new_labels)
            else:
                #left eye,0:x, 1:y
                #right : 2：x, 3:y
                x1 = new_labels[0]
                y1 = new_labels[1]
                x2 = new_labels[2]
                y2 = new_labels[3]
                if (x1!=0)&(x2 !=0):
                    new_elems.append(get_direction_1(x1,y1,x2,y2))
                else:
                    error_data = 1
    		 #add the dir index in elem 
            new_elems.append(elems[2])
    		 #avoid error data be in dataset
            if error_data!=1:
                new_content.append(new_elems)
        elif target == 'body':
            error_data = 0
            new_elems = []
            new_elems.append(dir_path+ elems[0])
            new_labels = []
            num_var = 6
            for i in elems[1]:
                new_labels.append(int(i))
                num_var = num_var-1
                if num_var <= 0:
                    break
    		 #process the labels 
            if testing:
                new_elems.append(new_labels)
            else:
                #left eye,0:x, 1:y
                #right : 2：x, 3:y
                x1 = new_labels[0]
                y1 = new_labels[1]
                x2 = new_labels[2]
                y2 = new_labels[3]
                x3 = new_labels[4]
                y3 = new_labels[5]
                if (x1!=0)&(x2!=0)&(x3!=0):
                    new_elems.append(get_direction_2(x1,y1,x2,y2,x3,y3,r))
                else:
                    error_data = 1
    		 #add the dir index in elem 
            new_elems.append(elems[2])
    		 #avoid error data be in dataset
            if error_data!=1:
                new_content.append(new_elems)
        else:
            print('receive wrong target ')
            print(1/0)
    return new_content

def create_gif(gif_name, path, duration = 0.3):
    '''
    生成gif文件，原始图片仅支持png格式
    gif_name ： 字符串，所生成的 gif 文件名，带 .gif 后缀
    path :      需要合成为 gif 的图片所在路径
    duration :  gif 图像时间间隔
    '''

    frames = []
    pngFiles = os.listdir(path)
    image_list = [os.path.join(path, f) for f in pngFiles]
    for image_index in list(np.arange(1017)):
        image_name = path +'//' +str(image_index) + '.png'
    #for image_name in image_list:
        # 读取 png 图像文件
        frames.append(imageio.imread(image_name))
    # 保存为 gif 
    imageio.mimsave(gif_name, frames, 'GIF', duration = duration)
    return

def get_start_point(point_dict):
    mid_dict = []
    for line in point_dict:
        img = line[0]
        print(line[1])
        point1 = line[1][4:6]

        mid_point = [point1[0], point1[1]]
        mid_dict.append([img, mid_point])
    return mid_dict





def degree_to_rad(angle):
    return (angle/360)*2*np.pi







def line_construct_truth(start_point, rate, index):
    x = start_point[0]
    y = start_point[1]
    list_x = []
    list_y = []
    
    if rate>0:
        if 0<=label_dict[index][1]<=90:
            print(label_dict[index][1])
            print('0-90',x,y,rate)
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y - np.abs(rate)
                list_x.append(x)
                list_y.append(y)
        else:
            print(label_dict[index][1])
            print('180-270',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    else:
        if 90<=label_dict[index][1]<=180:
            print(label_dict[index][1])
            print('90-180',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y - np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
        else:
            print(label_dict[index][1])
            print('270-360',x,y,rate)   
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    return list_x, list_y

def line_construct_pred(start_point, rate, index):
    x = start_point[0]
    y = start_point[1]
    list_x = []
    list_y = []
    if rate>0:
        if 0<=pred_angle[index]<=90:
            print(pred_angle[index])
            print('0-90',x,y,rate)
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y - np.abs(rate)
                list_x.append(x)
                list_y.append(y)
        else:
            print(pred_angle[index])
            print('180-270',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    else:
        if 90<=pred_angle[index]<=180:
            print(pred_angle[index])
            print('90-180',x,y,rate)
            while (y >= 0+np.abs(rate))&(x>0):
                x = x - 1
                y = y - np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
        else:
            print(pred_angle[index])
            print('270-360',x,y,rate)
            while (y >= 0+np.abs(rate))&(x<1280):
                x = x + 1
                y = y + np.abs(rate)
                #
                list_x.append(x)
                list_y.append(y)
    return list_x, list_y

def draw_line_img(img, list_x, list_y, mark=0, size = [2,1]):
    for index in range(len(list_x)):
        y = int(list_x[index])
        x = int(list_y[index])
        # b g r
        if mark ==0:
        # mark green
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],0] = 0 
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],1] = 255
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],2] = 0
        else:
        # red green
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],0] = 0 
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],1] = 0
            img[x-size[0]:x+size[0],y-size[1]:y+size[1],2] = 255
    return img
            
def img_construct(path, mid_dict, pred_angle, label_dict, display = 0, save = 0):
    count = 0
    save_path = r'C:\Users\xut\Desktop\test'
    for index in range(len(label_dict)):
        
        # name
        img = label_dict[index] 
        img_name = img[0]
        print('name: ',img[0])
        print('path: ',path+ '//'+img_name)
        image = cv2.imread(path+ '//'+img_name)
        #get mid_point
        mid_point = mid_dict[index][1]
        #get radian
        truth_rad = degree_to_rad(label_dict[index][1])
        pred_rad = degree_to_rad(pred_angle[index])
        #get changing rating
        truth_rate = np.tan(truth_rad)
        pred_rate = np.tan(pred_rad)
        #get the tracks of line
        truth_line_x,truth_line_y= line_construct_truth(mid_point, truth_rate, index)
        pred_line_x,pred_line_y = line_construct_pred(mid_point, pred_rate, index)
        # start to draw line on img
        new_img = draw_line_img(image, truth_line_x, truth_line_y, 0)
        new_img = draw_line_img(new_img, pred_line_x, pred_line_y, 1)

        if display:
            cv2.imshow("Image", new_img) 
            cv2.waitKey (0)
            cv2.destroyAllWindows()
        if save:
            cv2.imwrite(save_path+'//'+str(count)+'.png', new_img)
        count = count + 1

# construct the separate imgs
target = 'body'
r=12
data_path = r'C:\Users\xut\Desktop\shu_liu\mixed_data'
label_file_1 = r'\west_data'
label_file_2 = r'\south_data'
label_file_3 = r'\vr_version'
label_file_list = [label_file_1,label_file_2,label_file_3]

label_dict = construct_label_dict(data_path, label_file_list, False, target, r)
point_dict = construct_label_dict(data_path, label_file_list, True, target, r)


mid_dict = get_start_point(point_dict)
path = r'C:\Users\xut\Desktop\shu_liu\mixed_data'
img_list = img_construct(path, mid_dict, pred_angle, label_dict, save=1)

#construct the gif
source_path = r'D:\test'
#g_path = r'C:\Users\xut\Desktop\testing_vgg'
create_gif(r'test.gif', source_path, duration=0.3)
